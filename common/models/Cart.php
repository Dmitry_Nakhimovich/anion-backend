<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property int $id
 * @property int $id_stock
 * @property string $name
 * @property string $company
 * @property string $tel
 * @property string $email
 * @property string $text
 * @property int $priority
 * @property int $isActive
 * @property string $date
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_stock', 'priority', 'isActive'], 'integer'],
            [['text'], 'string'],
            [['date'], 'safe'],
            [['name', 'company', 'tel', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_stock' => 'Id Stock',
            'name' => 'Name',
            'company' => 'Company',
            'tel' => 'Tel',
            'email' => 'Email',
            'text' => 'Text',
            'priority' => 'Priority',
            'isActive' => 'Is Active',
            'date' => 'Date',
        ];
    }
}
