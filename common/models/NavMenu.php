<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "nav_menu".
 *
 * @property int $id
 * @property int $menu_type
 * @property int $parent_id
 * @property int $external_id
 * @property int $menu_level
 * @property string $name
 * @property string $link
 * @property string $tag
 * @property int $tag_isActive
 * @property int $isComplect
 * @property int $priority
 * @property int $isActive
 */
class NavMenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nav_menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_type', 'parent_id', 'external_id', 'menu_level', 'tag_isActive', 'isComplect', 'priority', 'isActive'], 'integer'],
            [['name'], 'required'],
            [['name', 'link', 'tag'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_type' => 'Menu Type',
            'parent_id' => 'Parent ID',
            'external_id' => 'External ID',
            'menu_level' => 'Menu Level',
            'name' => 'Name',
            'link' => 'Link',
            'tag' => 'Tag',
            'tag_isActive' => 'Tag Is Active',
            'isComplect' => 'Is Complect',
            'priority' => 'Priority',
            'isActive' => 'Is Active',
        ];
    }
}
