<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "materials".
 *
 * @property int $id
 * @property string $title
 * @property string $link
 * @property string $img
 * @property string $text
 * @property int $isActive
 * @property string $date
 */
class Materials extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'materials';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'date'], 'required'],
            [['img', 'text'], 'string'],
            [['isActive'], 'integer'],
            [['date'], 'safe'],
            [['title', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'link' => 'Link',
            'img' => 'Img',
            'text' => 'Text',
            'isActive' => 'Is Active',
            'date' => 'Date',
        ];
    }
}
