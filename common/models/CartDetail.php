<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cart_detail".
 *
 * @property int $id
 * @property int $id_cart
 * @property int $id_product
 * @property int $count
 * @property double $price
 * @property string $name
 * @property int $priority
 * @property int $isActive
 */
class CartDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cart_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cart', 'id_product'], 'required'],
            [['id_cart', 'id_product', 'count', 'priority', 'isActive'], 'integer'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_cart' => 'Id Cart',
            'id_product' => 'Id Product',
            'count' => 'Count',
            'price' => 'Price',
            'name' => 'Name',
            'priority' => 'Priority',
            'isActive' => 'Is Active',
        ];
    }
}
