<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "products_attr".
 *
 * @property int $id
 * @property int $sections_id
 * @property string $name
 * @property string $type
 * @property int $priority
 * @property int $isActive
 * @property int $isFilter
 */
class ProductsAttr extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products_attr';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sections_id', 'name', 'priority'], 'required'],
            [['sections_id', 'priority', 'isActive', 'isFilter'], 'integer'],
            [['name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sections_id' => 'Sections ID',
            'name' => 'Name',
            'type' => 'Type',
            'priority' => 'Priority',
            'isActive' => 'Is Active',
            'isFilter' => 'Is Filter',
        ];
    }
}
