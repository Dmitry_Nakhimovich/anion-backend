<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "filters_content".
 *
 * @property int $id
 * @property int $attr_id
 * @property string $name
 * @property string $text
 * @property double $num_before
 * @property double $num_after
 */
class FiltersContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'filters_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attr_id'], 'required'],
            [['attr_id'], 'integer'],
            [['text'], 'string'],
            [['num_before', 'num_after'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attr_id' => 'Attr ID',
            'name' => 'Name',
            'text' => 'Text',
            'num_before' => 'Num Before',
            'num_after' => 'Num After',
        ];
    }
}
