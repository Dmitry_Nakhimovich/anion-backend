<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sections_info".
 *
 * @property int $id
 * @property int $id_sections
 * @property int $id_info
 * @property int $priority
 * @property int $isActive
 */
class SectionsInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sections_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sections', 'id_info', 'priority', 'isActive'], 'required'],
            [['id_sections', 'id_info', 'priority', 'isActive'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sections' => 'Id Sections',
            'id_info' => 'Id Info',
            'priority' => 'Priority',
            'isActive' => 'Is Active',
        ];
    }
}
