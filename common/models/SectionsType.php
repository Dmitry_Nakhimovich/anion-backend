<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sections_type".
 *
 * @property int $id
 * @property int $id_sections
 * @property string $link
 * @property string $name
 * @property int $priority
 * @property int $isActive
 * @property string $datePublic
 */
class SectionsType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sections_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sections', 'name', 'priority', 'datePublic'], 'required'],
            [['id_sections', 'priority', 'isActive'], 'integer'],
            [['datePublic'], 'safe'],
            [['link', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sections' => 'Id Sections',
            'link' => 'Link',
            'name' => 'Name',
            'priority' => 'Priority',
            'isActive' => 'Is Active',
            'datePublic' => 'Date Public',
        ];
    }
}
