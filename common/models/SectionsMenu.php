<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sections_menu".
 *
 * @property int $id
 * @property int $id_sections
 * @property int $id_menu
 * @property int $priority
 * @property int $isActive
 * @property string $date
 */
class SectionsMenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sections_menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sections', 'id_menu', 'priority', 'isActive', 'date'], 'required'],
            [['id_sections', 'id_menu', 'priority', 'isActive'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sections' => 'Id Sections',
            'id_menu' => 'Id Menu',
            'priority' => 'Priority',
            'isActive' => 'Is Active',
            'date' => 'Date',
        ];
    }
}
