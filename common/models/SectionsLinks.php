<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sections_links".
 *
 * @property int $id
 * @property int $id_sections
 * @property int $id_link
 * @property int $priority
 * @property int $isActive
 */
class SectionsLinks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sections_links';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sections', 'id_link', 'priority', 'isActive'], 'required'],
            [['id_sections', 'id_link', 'priority', 'isActive'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sections' => 'Id Sections',
            'id_link' => 'Id Link',
            'priority' => 'Priority',
            'isActive' => 'Is Active',
        ];
    }
}
