<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $nav_menu_id
 * @property string $name
 * @property string $title
 * @property string $text
 * @property string $img
 * @property string $article
 * @property double $price
 * @property string $link
 * @property string $date
 * @property int $isActive
 * @property int $isComplect
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nav_menu_id', 'price', 'date'], 'required'],
            [['nav_menu_id', 'isActive', 'isComplect'], 'integer'],
            [['text', 'img'], 'string'],
            [['price'], 'number'],
            [['date'], 'safe'],
            [['name', 'title', 'article', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nav_menu_id' => 'Nav Menu ID',
            'name' => 'Name',
            'title' => 'Title',
            'text' => 'Text',
            'img' => 'Img',
            'article' => 'Article',
            'price' => 'Price',
            'link' => 'Link',
            'date' => 'Date',
            'isActive' => 'Is Active',
            'isComplect' => 'Is Complect',
        ];
    }
}
