<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "descont".
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $text
 * @property string $img
 * @property string $link
 */
class Descont extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'descont';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'img'], 'required'],
            [['img'], 'string'],
            [['name', 'title', 'text', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'title' => 'Title',
            'text' => 'Text',
            'img' => 'Img',
            'link' => 'Link',
        ];
    }
}
