<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "dop_menu_content".
 *
 * @property int $id
 * @property int $id_dop_menu
 * @property string $name
 * @property string $link
 * @property string $tag
 * @property int $tag_isActive
 * @property int $priority
 * @property int $isActive
 */
class DopMenuContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dop_menu_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_dop_menu', 'name'], 'required'],
            [['id_dop_menu', 'tag_isActive', 'priority', 'isActive'], 'integer'],
            [['name', 'link', 'tag'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_dop_menu' => 'Id Dop Menu',
            'name' => 'Name',
            'link' => 'Link',
            'tag' => 'Tag',
            'tag_isActive' => 'Tag Is Active',
            'priority' => 'Priority',
            'isActive' => 'Is Active',
        ];
    }
}
