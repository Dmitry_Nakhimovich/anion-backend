<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sections_files".
 *
 * @property int $id
 * @property int $id_sections
 * @property int $id_files
 * @property int $priority
 * @property int $isActive
 */
class SectionsFiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sections_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sections', 'id_files', 'priority', 'isActive'], 'required'],
            [['id_sections', 'id_files', 'priority', 'isActive'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sections' => 'Id Sections',
            'id_files' => 'Id Files',
            'priority' => 'Priority',
            'isActive' => 'Is Active',
        ];
    }
}
