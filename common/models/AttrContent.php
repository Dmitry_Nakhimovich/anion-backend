<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attr_content".
 *
 * @property int $id
 * @property int $attr_id
 * @property int $sections_id
 * @property string $name
 * @property string $text
 * @property double $num
 * @property double $num_before
 * @property double $num_after
 */
class AttrContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attr_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attr_id', 'sections_id'], 'required'],
            [['attr_id', 'sections_id'], 'integer'],
            [['text'], 'string'],
            [['num', 'num_before', 'num_after'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attr_id' => 'Attr ID',
            'sections_id' => 'Sections ID',
            'name' => 'Name',
            'text' => 'Text',
            'num' => 'Num',
            'num_before' => 'Num Before',
            'num_after' => 'Num After',
        ];
    }
}
