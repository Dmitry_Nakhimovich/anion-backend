<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "products_img".
 *
 * @property int $id
 * @property string $name
 * @property int $product_id
 * @property string $img
 * @property int $priority
 */
class ProductsImg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products_img';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
            [['product_id', 'priority'], 'integer'],
            [['img'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'product_id' => 'Product ID',
            'img' => 'Img',
            'priority' => 'Priority',
        ];
    }
}
