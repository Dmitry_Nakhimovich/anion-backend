<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sections_descont".
 *
 * @property int $id
 * @property int $id_sections
 * @property int $id_descont
 * @property int $priority
 * @property int $isActive
 */
class SectionsDescont extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sections_descont';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_sections', 'id_descont', 'priority', 'isActive'], 'required'],
            [['id_sections', 'id_descont', 'priority', 'isActive'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sections' => 'Id Sections',
            'id_descont' => 'Id Descont',
            'priority' => 'Priority',
            'isActive' => 'Is Active',
        ];
    }
}
