<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stock_content".
 *
 * @property int $id
 * @property int $stock_id
 * @property int $product_id
 * @property double $price
 */
class StockContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stock_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stock_id', 'product_id'], 'required'],
            [['stock_id', 'product_id'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stock_id' => 'Stock ID',
            'product_id' => 'Product ID',
            'price' => 'Price',
        ];
    }
}
