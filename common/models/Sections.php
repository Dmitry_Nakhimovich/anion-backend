<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sections".
 *
 * @property int $id
 * @property int $id_type
 * @property int $id_category
 * @property int $id_podcategory
 * @property int $id_item
 * @property int $isActive
 * @property string $link
 * @property int $id_menu
 * @property int $menu_isActive
 * @property int $priority_links
 * @property int $links_isActive
 * @property int $priority_files
 * @property int $files_isActive
 * @property int $priority_info
 * @property int $info_isActive
 * @property int $priority_descont
 * @property int $descont_isActive
 */
class Sections extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sections';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_type'], 'required'],
            [['id_type', 'id_category', 'id_podcategory', 'id_item', 'isActive', 'id_menu', 'menu_isActive', 'priority_links', 'links_isActive', 'priority_files', 'files_isActive', 'priority_info', 'info_isActive', 'priority_descont', 'descont_isActive'], 'integer'],
            [['link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_type' => 'Id Type',
            'id_category' => 'Id Category',
            'id_podcategory' => 'Id Podcategory',
            'id_item' => 'Id Item',
            'isActive' => 'Is Active',
            'link' => 'Link',
            'id_menu' => 'Id Menu',
            'menu_isActive' => 'Menu Is Active',
            'priority_links' => 'Priority Links',
            'links_isActive' => 'Links Is Active',
            'priority_files' => 'Priority Files',
            'files_isActive' => 'Files Is Active',
            'priority_info' => 'Priority Info',
            'info_isActive' => 'Info Is Active',
            'priority_descont' => 'Priority Descont',
            'descont_isActive' => 'Descont Is Active',
        ];
    }
}
