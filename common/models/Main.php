<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "main".
 *
 * @property int $id
 * @property int $type
 * @property string $name
 * @property string $title
 * @property string $subtitle
 * @property string $text
 * @property string $img
 * @property string $tag
 * @property int $tag_isActive
 * @property string $link
 * @property int $delay
 * @property int $priority
 * @property int $isActive
 * @property int $external_id
 */
class Main extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'main';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type', 'tag_isActive', 'delay', 'priority', 'isActive', 'external_id'], 'integer'],
            [['text', 'img'], 'string'],
            [['name', 'title', 'subtitle', 'tag', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'title' => 'Title',
            'subtitle' => 'Subtitle',
            'text' => 'Text',
            'img' => 'Img',
            'tag' => 'Tag',
            'tag_isActive' => 'Tag Is Active',
            'link' => 'Link',
            'delay' => 'Delay',
            'priority' => 'Priority',
            'isActive' => 'Is Active',
            'external_id' => 'External ID',
        ];
    }
}
