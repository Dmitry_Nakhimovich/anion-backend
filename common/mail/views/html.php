<?php

/** @var $this \yii\web\View */
/** @var $link string */
/** @var $paramExample string */

?>

<table width="100%" border="0" cellpadding="0" cellspacing="0"
       style="border-collapse: collapse; border-spacing: 0;font-family:'Open Sans', sans-serif;">
  <tr>
    <td width="100%" align="center">
      <table border="0" cellpadding="0" cellspacing="0" width="600"
             style="border-collapse: collapse; border-spacing: 0;">
        <!-- title start-->
        <tr>
          <td width="100%" style="background-color: #0097ff;padding-left: 50px; padding-right: 50px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; border-spacing: 0;">
              <tr>
                <td>
                  <img src="anion_email_1_03.png" width="90" height="60">
                </td>
              </tr>
              <tr>
                <td align="center" width="100%">
                  <span style="vertical-align: middle; display:inline-block;">
                    <img src="anion_email_1_05.png" width="80" height="70">
                  </span>
                  <span style="vertical-align: middle; display:inline-block; font-size: 18px; color: #ffffff;">
                    Заявка на заказ отправлена
                  </span>
                </td>
              </tr>
              <tr>
                <td width="100%" height="60"></td>
              </tr>
            </table>
          </td>
        </tr>
        <!-- title end-->

        <!-- head start -->
        <tr>
          <td width="100%" style="border-bottom: 1px solid #c9c9c9;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; border-spacing: 0;">
              <tr>
                <td width="100%" style="background-color: #ffffff;padding-left: 50px; padding-right: 50px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%"
                         style="border-collapse: collapse; border-spacing: 0;">
                    <tr>
                      <td width="100%" height="25">

                      </td>
                    </tr>
                    <tr>
                      <td>
                        <span style="color: #003068; font-weight: bold;font-size: 16px;">Здравствуйте,Игорь!</span>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="25"></td>
                    </tr>
                    <tr>
                      <td>
                        <span style="color: #003068;font-weight: 400;font-size: 14px;">
                          Мы приняли ваш заказ, скоро с Вами свяжется <br>
                          наш менеджер для подтверждения деталей заказа
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="50"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <!-- head end-->

        <!-- main start-->
        <tr>
          <td width="100%" style="background-color: #ffffff;padding-left: 50px; padding-right: 50px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; border-spacing: 0;">
              <tr>
                <td width="100%" height="50"></td>
              </tr>
              <tr>
                <td>
                  <span style="color: #003068; font-weight: bold;">Позиции заказа:</span>
                </td>
              </tr>
              <tr>
                <td width="100%" height="35"></td>
              </tr>
              <tr>
                <td width="100%">
                  <table width="100%" border="0" cellpadding="0" cellspacing="0"
                         style="border-collapse: collapse; border-spacing: 0;">
                    <tr>
                      <td>
                        <img src="anion_email_1_09.png" width="80" height="50">
                      </td>
                      <td>
                        <span style="display: block;color: #003068; font-size: 14px;font-weight: 600;">Крышка для ванной К 900</span>
                        <span style="display: block;color: #9e9e9e; font-size: 12px;">2 шт.</span>
                      </td>
                      <td style="padding-bottom: 20px;">
                         <span style="color: #003068;font-weight: 600;">
                           1 296, 00 ₽
                         </span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <img src="anion_email_1_12.png" width="80" height="50">
                      </td>
                      <td>
                        <span
                          style="display: block; color: #003068; font-size: 14px;font-weight:600;">
                        Кран шаровой 1/2
                        </span>
                        <span style="display: block;color: #9e9e9e; font-size: 12px;">
                          1 шт.
                        </span>
                      </td>
                      <td style="padding-bottom: 20px;">
                        <span style="color: #003068;font-weight: 600;">
                          1 578, 00 ₽
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <img src="anion_email_1_15.png" width="80" height="50">
                      </td>
                      <td>
                       <span
                         style="display: block; color: #003068; font-size: 14px;font-weight: 600;">
                        Пластиковый бак 200л <br>
                        для дозирующих слайдов
                       </span>
                        <span style="display: block;color: #9e9e9e; font-size: 12px;">
                         2 шт.
                       </span>
                      </td>
                      <td style="padding-bottom: 20px;">
                                               <span style="color: #003068;font-weight: 600;">
                                                 7 460, 00 ₽
                                               </span>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td width="100%" height="45">

                </td>
              </tr>
              <tr>
                <td width="100%">
                  <table>
                    <tr>
                      <td style="color: #949494;">
                                                  <span>
                                                    Склад:
                                                  </span>
                      </td>
                      <td style="text-decoration: underline;color: #003068;">
                                                <span style="padding-left: 55px">
                                                  Нижний Новгород
                                                </span>
                      </td>
                      <td style="color: #949494;">
                                                <span style="padding-left: 30px">
                                                    Итого:
                                                </span>
                      </td>
                      <td style="color: #66c1ff;font-size: 18px;">
                                                <span style="padding-left: 20px">
                                                 10 334, 00  ₽
                                                </span>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td width="100%" height="45">

                </td>
              </tr>
            </table>
          </td>
        </tr>
        <!-- main end-->

        <!-- footer start -->
        <tr>
          <td width="100%" style="background-color: #f5f7fa;padding-left: 50px; padding-right: 50px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; border-spacing: 0;">
              <tr>
                <td width="100%" height="30">

                </td>
              </tr>
              <tr>
                <td width="100%">
                  <table border="0" cellpadding="0" cellspacing="0"
                         style="width: 500px;border-collapse: collapse; border-spacing: 0;">
                    <tr>
                      <td>
                                                   <span style="display:block;">
                                                      <img src="anion_email_1_19.png" width="90" height="60">
                                                   </span>
                        <span style="font-size: 10px;display: block;color: #b2b5c0;">
                                                       © ООО «Анион», пластиковые емкости, промышленная <br>
                                                       тара, листы, панели, стержни из пластика, дорожные <br>
                                                       блоки, конусы, ограждения, садовая мебель. 1992—2017
                                                   </span>
                      </td>
                      <td>
                                                  <span style="display:block;height: 40px;">
                                                     <span width="12" height="12" style="display:inline-block;">
                                                        <img src="1.png" width="12" height="12">
                                                     </span>
                                                     <span
                                                       style="display:inline-block; font-size: 10px;font-weight: bold;color: #162b62;">
                                                         +7 (831) 123-45-67
                                                     </span>
                                                  </span>
                        <span style="display:block;height: 40px;">
                                                     <span width="10" height="15" style="display:inline-block;">
                                                        <img src="2.png" width="10" height="15">
                                                     </span>
                                                     <span
                                                       style="display:inline-block; font-size: 10px;font-weight: bold;color: #162b62;">
                                                        г. Нижний Новгород, <br>
                                                        ул. Лермонтова, 65, к.1
                                                     </span>
                                                  </span>
                        <span style="display:block;height: 40px;">
                                                       <span width="12" height="12" style="display:inline-block;">
                                                          <img src="3.png" width="12" height="12">
                                                       </span>
                                                       <span
                                                         style="display:inline-block; font-size: 10px;font-weight: bold;color: #162b62;">
                                                          пн—пт 9:00—18:00
                                                       </span>
                                                   </span>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td width="100%" height="25">

                </td>
              </tr>
            </table>
          </td>
        </tr>
        <!-- footer end -->
      </table>
    </td>
  </tr>
</table>