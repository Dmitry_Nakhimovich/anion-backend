<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=Anion_test',
            'username' => 'root',
            'password' => '',
            //'dsn' => 'mysql:host=localhost;dbname=nefabrika0_pl',
            //'username' => 'nefabrika0_pl',
            //'password' => 'NYx3eR9R',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.nic.ru',
                'username' => 'test@anionplast.ru',
                'password' => 'testotesto123',
                'port' => '465',
                'encryption' => 'ssl',
            ],
            'useFileTransport' => false,
        ],
    ],
];
