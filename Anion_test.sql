-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 03 2018 г., 14:47
-- Версия сервера: 5.6.38
-- Версия PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `Anion_test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `attr_content`
--

CREATE TABLE `attr_content` (
  `id` int(11) NOT NULL,
  `attr_id` int(11) NOT NULL,
  `sections_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `text` text,
  `num` float DEFAULT '0',
  `num_before` float DEFAULT NULL,
  `num_after` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `id_stock` int(11) DEFAULT '1',
  `name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `text` text,
  `priority` int(11) NOT NULL DEFAULT '0',
  `isActive` int(11) NOT NULL DEFAULT '1',
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `cart_detail`
--

CREATE TABLE `cart_detail` (
  `id` int(11) NOT NULL,
  `id_cart` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '1',
  `price` float NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `isActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `descont`
--

CREATE TABLE `descont` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `img` text NOT NULL,
  `link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `descont`
--

INSERT INTO `descont` (`id`, `name`, `title`, `text`, `img`, `link`) VALUES
(2, 'Акция 2', 'Акция 2', '123 12 1 вы фыв 12 3увы фы122', '/admin/upload/global/imgs/news_pict_4.png', 'test12');

-- --------------------------------------------------------

--
-- Структура таблицы `dop_menu`
--

CREATE TABLE `dop_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dop_menu`
--

INSERT INTO `dop_menu` (`id`, `name`, `link`) VALUES
(1, 'Dop menu', ''),
(2, 'menu 2', NULL),
(5, 'Мое меня 3', 'test12');

-- --------------------------------------------------------

--
-- Структура таблицы `dop_menu_content`
--

CREATE TABLE `dop_menu_content` (
  `id` int(11) NOT NULL,
  `id_dop_menu` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `tag_isActive` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  `isActive` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dop_menu_content`
--

INSERT INTO `dop_menu_content` (`id`, `id_dop_menu`, `name`, `link`, `tag`, `tag_isActive`, `priority`, `isActive`) VALUES
(2, 1, 'eeee', '123', '', 0, 2, 1),
(3, 2, '1', '123', 'new ', 0, 2, 1),
(4, 2, '222', '222', 'МОЙ ЛЕЙБЛ', 1, 3, 1),
(5, 5, 'текст 1', '123', 'new ', 1, 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `id_stock` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `text` text,
  `priority` int(11) NOT NULL DEFAULT '0',
  `isActive` int(11) NOT NULL DEFAULT '1',
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `files`
--

INSERT INTO `files` (`id`, `name`, `file`) VALUES
(1, '123', '/admin/upload/global/imgs/action.png'),
(2, '4343', '/admin/upload/global/imgs/complect_1.png');

-- --------------------------------------------------------

--
-- Структура таблицы `filters_content`
--

CREATE TABLE `filters_content` (
  `id` int(11) NOT NULL,
  `attr_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `text` text,
  `num_before` float DEFAULT NULL,
  `num_after` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `info`
--

INSERT INTO `info` (`id`, `name`, `text`, `date`) VALUES
(1, '123', '3wsdsdasd asd qwe \r\nqwe qwe123', '2018-07-20'),
(2, '131123', '3131', '2017-07-20');

-- --------------------------------------------------------

--
-- Структура таблицы `links`
--

CREATE TABLE `links` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `links`
--

INSERT INTO `links` (`id`, `name`, `link`) VALUES
(2, 'link 2322', ''),
(3, '3232', 'test12');

-- --------------------------------------------------------

--
-- Структура таблицы `main`
--

CREATE TABLE `main` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `text` text,
  `img` text,
  `tag` varchar(255) DEFAULT NULL,
  `tag_isActive` int(11) NOT NULL DEFAULT '0',
  `link` varchar(255) DEFAULT NULL,
  `delay` int(11) NOT NULL DEFAULT '5',
  `priority` int(11) NOT NULL DEFAULT '0',
  `isActive` int(11) NOT NULL DEFAULT '0',
  `external_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `main`
--

INSERT INTO `main` (`id`, `type`, `name`, `title`, `subtitle`, `text`, `img`, `tag`, `tag_isActive`, `link`, `delay`, `priority`, `isActive`, `external_id`) VALUES
(2, 1, 'Заказать', 'Моя первая новость', NULL, 'lorem lorem 321', '/admin/upload/global/imgs/main-bg.jpg', 'МОЙ ЛЕЙБЛ', 1, 'test132', 3000, 4, 1, NULL),
(3, 2, NULL, 'Акция 2', NULL, '12312312 123 ', '/admin/upload/global/imgs/main-bg.jpg', NULL, 0, 'test12', 2000, 1, 1, NULL),
(4, 2, NULL, 'Моя первая новость1', NULL, '123123 ', '/admin/upload/global/imgs/action.png', NULL, 0, '1', 3, 1, 1, NULL),
(5, 4, 'main', NULL, NULL, '<p>Главный текст описания 123</p>\r\n', NULL, NULL, 0, NULL, 5, 0, 1, NULL),
(6, 4, 'sub', NULL, NULL, '<pre>\r\n<span style=\"font-size:14px\">С 1992 года на рынке</span></pre>\r\n\r\n<pre>\r\n<span style=\"font-size:14px\">Собственное производство</span></pre>\r\n\r\n<pre>\r\n<span style=\"font-size:14px\">Член международной ассоциации\r\nпроизводителей ARM\r\n(Association of rotational molders)</span></pre>\r\n', NULL, NULL, 0, NULL, 5, 0, 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `materials`
--

CREATE TABLE `materials` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `img` text,
  `text` longtext,
  `isActive` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1531221733),
('m130524_201442_init', 1531221739);

-- --------------------------------------------------------

--
-- Структура таблицы `nav_menu`
--

CREATE TABLE `nav_menu` (
  `id` int(11) NOT NULL,
  `menu_type` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `external_id` int(11) DEFAULT NULL,
  `menu_level` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `tag_isActive` int(11) NOT NULL DEFAULT '0',
  `isComplect` int(11) DEFAULT '0',
  `priority` int(11) DEFAULT NULL,
  `isActive` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `nav_menu_img`
--

CREATE TABLE `nav_menu_img` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `img` text,
  `priority` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `img` text,
  `text` longtext,
  `isActive` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `nav_menu_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `img` text,
  `article` varchar(255) DEFAULT NULL,
  `price` float NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `date` date NOT NULL,
  `isActive` int(11) NOT NULL DEFAULT '1',
  `isComplect` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `products_attr`
--

CREATE TABLE `products_attr` (
  `id` int(11) NOT NULL,
  `sections_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'text',
  `priority` int(11) NOT NULL,
  `isActive` int(11) NOT NULL DEFAULT '1',
  `isFilter` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `products_img`
--

CREATE TABLE `products_img` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `img` text,
  `priority` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `id_category` int(11) DEFAULT NULL,
  `id_podcategory` int(11) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  `isActive` int(11) NOT NULL DEFAULT '0',
  `link` varchar(255) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `menu_isActive` int(11) NOT NULL DEFAULT '0',
  `priority_links` int(11) DEFAULT NULL,
  `links_isActive` int(11) NOT NULL DEFAULT '0',
  `priority_files` int(11) DEFAULT NULL,
  `files_isActive` int(11) NOT NULL DEFAULT '0',
  `priority_info` int(11) DEFAULT NULL,
  `info_isActive` int(11) NOT NULL DEFAULT '0',
  `priority_descont` int(11) DEFAULT NULL,
  `descont_isActive` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sections`
--

INSERT INTO `sections` (`id`, `id_type`, `id_category`, `id_podcategory`, `id_item`, `isActive`, `link`, `id_menu`, `menu_isActive`, `priority_links`, `links_isActive`, `priority_files`, `files_isActive`, `priority_info`, `info_isActive`, `priority_descont`, `descont_isActive`) VALUES
(1, 0, NULL, NULL, NULL, 1, 'index', NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0),
(2, 1, NULL, NULL, NULL, 1, 'products', NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0),
(3, 2, NULL, NULL, 0, 1, 'materials', 14, 1, NULL, 0, NULL, 0, NULL, 0, NULL, 0),
(4, 3, NULL, NULL, -1, 1, 'news', 11, 1, 1, 1, NULL, 0, 3, 0, 2, 0),
(5, 4, NULL, NULL, 2, 1, 'about', NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0),
(6, 5, NULL, NULL, 1, 1, 'contacts', NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `sections_descont`
--

CREATE TABLE `sections_descont` (
  `id` int(11) NOT NULL,
  `id_sections` int(11) NOT NULL,
  `id_descont` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sections_descont`
--

INSERT INTO `sections_descont` (`id`, `id_sections`, `id_descont`, `priority`, `isActive`) VALUES
(1, 26, 2, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `sections_files`
--

CREATE TABLE `sections_files` (
  `id` int(11) NOT NULL,
  `id_sections` int(11) NOT NULL,
  `id_files` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sections_files`
--

INSERT INTO `sections_files` (`id`, `id_sections`, `id_files`, `priority`, `isActive`) VALUES
(1, 26, 1, 3, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `sections_info`
--

CREATE TABLE `sections_info` (
  `id` int(11) NOT NULL,
  `id_sections` int(11) NOT NULL,
  `id_info` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sections_links`
--

CREATE TABLE `sections_links` (
  `id` int(11) NOT NULL,
  `id_sections` int(11) NOT NULL,
  `id_link` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `isActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sections_links`
--

INSERT INTO `sections_links` (`id`, `id_sections`, `id_link`, `priority`, `isActive`) VALUES
(2, 4, 3, 1, 1),
(3, 32, 2, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `sections_menu`
--

CREATE TABLE `sections_menu` (
  `id` int(11) NOT NULL,
  `id_sections` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `isActive` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sections_menu`
--

INSERT INTO `sections_menu` (`id`, `id_sections`, `id_menu`, `priority`, `isActive`, `date`) VALUES
(11, 4, 1, 1, 1, '2018-07-30'),
(13, 7, 1, 1, 1, '2018-07-30'),
(14, 3, 5, 1, 1, '2018-07-31'),
(16, 11, 1, 1, 1, '2018-08-03'),
(17, 26, 1, 2, 1, '2018-08-08');

-- --------------------------------------------------------

--
-- Структура таблицы `sections_type`
--

CREATE TABLE `sections_type` (
  `id` int(11) NOT NULL,
  `id_sections` int(11) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  `isActive` int(11) NOT NULL DEFAULT '1',
  `datePublic` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sections_type`
--

INSERT INTO `sections_type` (`id`, `id_sections`, `link`, `name`, `priority`, `isActive`, `datePublic`) VALUES
(1, 1, 'index', 'Главная', 1, 0, '2018-07-17'),
(2, 2, 'products', 'Продукция', 2, 1, '2018-07-17'),
(3, 3, 'materials', 'Материалы', 3, 1, '2018-07-17'),
(4, 4, 'news', 'Новости', 4, 1, '2018-07-31'),
(5, 5, 'about', 'О компании', 5, 1, '2018-07-17'),
(6, 6, 'contacts', 'Контакты', 6, 1, '2018-08-02');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `link` varchar(255) DEFAULT NULL,
  `isActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `name`, `title`, `text`, `link`, `isActive`) VALUES
(1, 'tel', NULL, '+7 (999) 123-45-67', NULL, 1),
(2, 'adr', NULL, 'Нижний Новгород, ул. Пушкина, д.666', NULL, 1),
(3, 'work', NULL, 'пн-пт 9:00-18:00', NULL, 1),
(4, 'mail', NULL, 'info@site.ru', NULL, 1),
(5, 'rules', NULL, 'Правила обработки персональных данных ', '/admin/upload/global/imgs/news_pict_4.png', 1),
(6, 'logo', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `isActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stock`
--

INSERT INTO `stock` (`id`, `name`, `code`, `priority`, `isActive`) VALUES
(6, 'Москва', 'msk', 2, 1),
(7, 'Нижний Новгород', 'nnov', 1, 1),
(8, 'Клин', 'klin', 3, 1),
(9, 'Дзержинск', 'dzr', 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `stock_content`
--

CREATE TABLE `stock_content` (
  `id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'BIK3xhdLroFShSnvjS5MoiWJWLfnbS0Q', '$2y$13$4xUuZ1b1gb7.J435BCAa4uA8U3qZwvsFm4vNS9CYl/m5HS5/mSN2u', NULL, 'admin@admin.ru', 10, 1531221840, 1531221840),
(2, 'user', 'I16FrgoQSvxXzPyUDtCS-ekiWgQh4LJn', '$2y$13$py5Nz942RS98A5MDyvn.FOGh7JTlZbpZYSUeSTmbSIBYQQTngqtoW', NULL, 'user@gmail.com', 10, 1532678717, 1532678717),
(3, 'user2', '87QLa_DQCmcCNUelhCxCo8DtSko70pXv', '$2y$13$t1LSX6LQ.7MMoZh5C7uML.YOyqeVo6st82l./peqTAhSdhLIG1xVW', NULL, 'user2@gmail.com', 10, 1532678754, 1532678754);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `attr_content`
--
ALTER TABLE `attr_content`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cart_detail`
--
ALTER TABLE `cart_detail`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `descont`
--
ALTER TABLE `descont`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `dop_menu`
--
ALTER TABLE `dop_menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `dop_menu_content`
--
ALTER TABLE `dop_menu_content`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `filters_content`
--
ALTER TABLE `filters_content`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `main`
--
ALTER TABLE `main`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `nav_menu`
--
ALTER TABLE `nav_menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nav_menu_img`
--
ALTER TABLE `nav_menu_img`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products_attr`
--
ALTER TABLE `products_attr`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products_img`
--
ALTER TABLE `products_img`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sections_descont`
--
ALTER TABLE `sections_descont`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sections_files`
--
ALTER TABLE `sections_files`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sections_info`
--
ALTER TABLE `sections_info`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sections_links`
--
ALTER TABLE `sections_links`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sections_menu`
--
ALTER TABLE `sections_menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sections_type`
--
ALTER TABLE `sections_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Индексы таблицы `stock_content`
--
ALTER TABLE `stock_content`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `attr_content`
--
ALTER TABLE `attr_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cart_detail`
--
ALTER TABLE `cart_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `descont`
--
ALTER TABLE `descont`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `dop_menu`
--
ALTER TABLE `dop_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `dop_menu_content`
--
ALTER TABLE `dop_menu_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `filters_content`
--
ALTER TABLE `filters_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `links`
--
ALTER TABLE `links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `main`
--
ALTER TABLE `main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `materials`
--
ALTER TABLE `materials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `nav_menu`
--
ALTER TABLE `nav_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `nav_menu_img`
--
ALTER TABLE `nav_menu_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `products_attr`
--
ALTER TABLE `products_attr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `products_img`
--
ALTER TABLE `products_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `sections_descont`
--
ALTER TABLE `sections_descont`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `sections_files`
--
ALTER TABLE `sections_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `sections_info`
--
ALTER TABLE `sections_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `sections_links`
--
ALTER TABLE `sections_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `sections_menu`
--
ALTER TABLE `sections_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `sections_type`
--
ALTER TABLE `sections_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `stock_content`
--
ALTER TABLE `stock_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
