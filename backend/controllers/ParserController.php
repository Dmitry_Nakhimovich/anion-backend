<?php

namespace backend\controllers;

use common\models\AttrContent;
use common\models\FiltersContent;
use common\models\NavMenu;
use common\models\Products;
use common\models\ProductsAttr;
use common\models\Sections;
use common\models\Stock;
use common\models\StockContent;
use phpQuery;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use GuzzleHttp\Client;
use yii\helpers\Url;


class ParserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'secondary'
            ],
        ];
    }

    public static function link2url($s)
    {
        $s = (string)$s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
        return $s; // возвращаем результат
    }

    public static function createParser($base, $data)
    {
        // выход из рекурсии
        if (empty($data[0]) && empty($data['category']) && empty($data['product']))
            return;
        // основной цикл перебора
        foreach ($data as $i => $item) {
            if ($item['title'])
                ParserController::createCategory($base, $item['title'], $i + 1);
            if (!empty($item['data']['category']))
                foreach ($item['data']['category'] as $attr) {
                    ParserController::createAttr($base, $item['title'], $i + 1, $attr);
                }
            if (!empty($item['data']['product']))
                foreach ($item['data']['product'] as $product) {
                    ParserController::getProduct($base, $item['title'], $product);
                }
            if (!empty($item))
                ParserController::createParser($item['title'], $item['data']);
        }
    }

    public static function createCategory($parent_name, $curr_name, $priority = 0)
    {
        $parent_name = trim($parent_name);
        $curr_name_string = htmlentities($curr_name, null, 'utf-8');
        $curr_name = mb_eregi_replace("&nbsp;", "", $curr_name_string);
        $curr_name = html_entity_decode($curr_name);
        $parent_navMenu = NavMenu::find()->where(['menu_type' => 1])
            ->andWhere(['name' => $parent_name])
            ->one();
        $cmodel = NavMenu::find()->where(['menu_type' => 1])
            ->andWhere(['parent_id' => $parent_navMenu->id])
            ->andWhere(['name' => $curr_name])->one();
        if (isset($cmodel))
            return;
        $csection = new Sections();
        $csection->id_type = 1;
        $csection->id_item = 0;
        $csection->isActive = 1;
        $csection->save();
        $cmodel = new NavMenu();
        $cmodel->menu_type = 1;
        $cmodel->parent_id = isset($parent_navMenu) ? $parent_navMenu->id : 0;
        $cmodel->external_id = $csection->id;
        $cmodel->menu_level = ($parent_navMenu->menu_level ?: 0) + 1;
        $cmodel->name = trim($curr_name);
        $cmodel->priority = $priority;
        $cmodel->isActive = 1;
        $cmodel->save();
        $clink = ParserController::link2url($curr_name) ?: '';
        $clinkAll = $clink != '' ? Sections::find()->where(['id_type' => 1])->andWhere(['link' => $clink])->one() : '';
        $cmodel->link = isset($clinkAll) ? $clink . '-' . (string)$cmodel->id : $clink;
        $cmodel->save();
        $csection->link = $cmodel->link;
        $csection->save();
        return $cmodel;
    }

    public static function createAttr($parent_name, $curr_name, $priority = 0, $data)
    {
        $parent_name = trim($parent_name);
        $curr_name = trim(html_entity_decode($curr_name));
        $parent_navMenu = NavMenu::find()->where(['menu_type' => 1])
            ->andWhere(['name' => $parent_name])
            ->one();
        $navMenu = NavMenu::find()->where(['menu_type' => 1])
            ->andWhere(['parent_id' => $parent_navMenu->id])
            ->andWhere(['name' => $curr_name])->one();
        if (isset($cmodel))
            return;
        $section = Sections::findOne($navMenu->external_id);
        $cmodel = new ProductsAttr();
        $cmodel->sections_id = $section->id;
        $cmodel->name = trim(str_replace('&nbsp;', '', htmlentities($data['filter_name'])));
        $cmodel->type = $data['filter_type'];
        $cmodel->priority = $priority;
        $cmodel->isActive = 1;
        $cmodel->isFilter = 1;
        $cmodel->save();
        foreach ($data['filter_attr'] as $attr) {
            $c = new FiltersContent();
            $c->attr_id = $cmodel->id;
            $c->text = trim($attr);
            $c->save();
        }
        $cproducts = Products::find()->where(['nav_menu_id' => $navMenu->id])->all();
        foreach ($cproducts as $p) {
            $cattrContent = new AttrContent();
            $cattrContent->attr_id = $cmodel->id;
            $cattrContent->sections_id = Sections::find()->where(['id_type' => 1])
                ->andWhere(['id_item' => $p->id])->one()->id;
            $cattrContent->save();
        }
        return;
    }

    public static function getTitle($link)
    {
        // подключаем Guzzle
        $client = new Client();
        // передаем параметры в запрос
        $res = $client->request(
            'GET',
            $link
        );
        // получаем страницу
        $body = $res->getBody();
        // подключаем phpQuery для обработки страницы
        $document = phpQuery::newDocumentHTML($body);
        // осуществляем поиск
        $title = trim($document->find(".article h1")->text());
        if ($title) {
            // не учитывается дубликат в других категориях
            return ParserController::createCategory('', $title, 1);
        }
        return false;
    }

    public static function getProduct($parent_name, $curr_name, $link)
    {
        $parent_name = trim($parent_name);
        $curr_name = trim($curr_name);
        $parent_navMenu = NavMenu::find()->where(['menu_type' => 1])
            ->andWhere(['name' => $parent_name])
            ->one();
        $navMenu = NavMenu::find()->where(['menu_type' => 1])
            ->andWhere(['parent_id' => $parent_navMenu->id])
            ->andWhere(['name' => $curr_name])
            ->one();
        if (empty($navMenu))
            return;

        // подключаем Guzzle
        $client = new Client();
        // передаем параметры в запрос
        $res = $client->request(
            'GET',
            $link
        );
        // получаем страницу
        $body = $res->getBody();
        // подключаем phpQuery для обработки страницы
        $document = phpQuery::newDocumentHTML($body);
        // осуществляем поиск
        $cmodel = new Products();
        $cmodel->nav_menu_id = $navMenu->id;
        $title = trim($document->find(".article h1")->text());
        $cmodel->title = $title ?: '';
        $cmodel->isComplect = 0;
        $article = trim($document->find(".catdescr .article")->text());
        $articleText = str_replace('Артикул: ', '', $article);
        $article = $document->find(".catdescr .article")->remove();
        $img = $document->find(".catdescr .catdescr_images")->remove();
        $cmodel->article = $articleText ?: '';
        $cmodel->price = 0;
        $cmodel->img = '/admin/upload/no-image.png';
        $cmodel->date = date('Y-m-d');
        $cmodel->isActive = 1;
        $cmodel->save();

        $prices = $document->find(".catdescr .catdescr_prices table tbody tr");
        foreach ($prices as $ps) {
            $ps_name = trim(pq($ps)->find("td:eq(0) span:eq(0)")->text());
            $ps_price = trim(pq($ps)->find("td:eq(1)")->text());
            $stockId = Stock::find()->where(['name' => $ps_name])->one()->id;
            if ($stockId) {
                $pc = new StockContent();
                $pc->stock_id = $stockId;
                $pc->product_id = $cmodel->id;
                $pc->price = (float)$ps_price ? (float)$ps_price : 0;
                $pc->save();
            }
        }
        $prices = $document->find(".catdescr .catdescr_prices")->remove();
        $text = $document->find(".catdescr")->html();
        $cmodel->text = $text;
        $cmodel->save();

        $csection = new Sections();
        $csection->id_type = 1;
        $csection->id_item = $cmodel->id;
        $csection->link = (string)$cmodel->id;
        $csection->isActive = 1;
        $csection->save();

        $cattrs = $document->find("#xar table tr");
        $attrs = ProductsAttr::find()->where(['sections_id' => $navMenu->external_id])->all();
        foreach ($cattrs as $ca) {
            foreach ($attrs as $a) {
                $caTitle = trim(pq($ca)->find("td:eq(0)")->text());
                $caVal = trim(pq($ca)->find("td:eq(1)")->text());
                if (trim($a->name) == $caTitle) {
                    $attrContent = new AttrContent();
                    $attrContent->attr_id = $a->id;
                    $attrContent->sections_id = $csection->id;
                    $attrContent->text = (string)$caVal ? (string)$caVal : '';
                    $attrContent->num = (float)$caVal ? (float)$caVal : 0;
                    $attrContent->save();
                }
            }
        }

        return false;
    }

    public static function getPage($link)
    {
        // подключаем Guzzle
        $client = new Client();
        // передаем параметры в запрос
        $res = $client->request(
            'GET',
            $link
        );
        // получаем страницу
        $body = $res->getBody();
        // подключаем phpQuery для обработки страницы
        $document = phpQuery::newDocumentHTML($body);
        // осуществляем поиск
        $data_links = $document->find(".cat-list li");
        $data_products = $document->find("#list_table_product");
        $title = trim($document->find(".article h1")->text());
        // проверка выхода из рекурсии
        if (empty($data_links) && empty($data_products))
            return;

        if ($title) {
            // не учитывается дубликат в других категориях
            //ParserController::createCategory('', $title, 1);
        }
        // обработка данных из поиска
        $data = [];
        // ссылки на подкатегории
        if (!empty($data_links)) {
            foreach ($data_links as $i => $elem) {
                $pq = pq($elem);

                $ntitle = $pq->find("a:last-child")->text();
                $nlink = 'http://anion-msk.com' . $pq->find("a:last-child")->attr('href');
                //ParserController::createCategory($title, $ntitle, $i + 1);

                sleep(1);
                $data[] = [
                    'title' => $ntitle,
                    'link' => $nlink,
                    'data' => ParserController::getPage($nlink)
                ];
            }
        }
        // характеристики товаров подкатегории
        if (!empty($data_products)) {
            $data['category'] = [];
            //$filter = $document->find("#list_table_product tr:eq(0) th");
            $table_arr = $document->find("#list_table_product tr:eq(1) th");
            foreach ($table_arr as $i => $elem) {
                $tooltip = pq($elem)->find(".tooltip");
                $tooltip->find('img, span')->remove();
                $filter_name = trim(pq($elem)->text());
                $filter_data = $document->find("#list_table_product tr:eq(0) th:eq(" . $i . ") select");
                $filter_type = $filter_data != "" ? "select" : "text";
                $filter_attr = [];
                foreach (pq($filter_data)->find("option") as $attr)
                    $filter_attr[] = pq($attr)->text();
                if ($filter_name != "" && $i > 2 && $i < $table_arr->length - 2) {
                    $cdata = [
                        'filter_name' => $filter_name,
                        'filter_type' => $filter_type,
                        'filter_attr' => $filter_attr,
                    ];
                    $data['category'][] = $cdata;
                    //ParserController::createAttr($title, $i + 1, $cdata);
                }
            }
            $data['product'] = [];
            $product_arr = $document->find("#list_table_product tr.linkCard");
            foreach ($product_arr as $item) {
                $data['product'][] = 'http://anion-msk.com' . pq($item)->attr('data-link-card');
            }
        }

        return $data;
    }

    public function actionIndex()
    {
        ini_set('max_execution_time', '6000');
        ini_set('mysql.connect_timeout', '6000');
        ini_set('mysql.wait_timeout', '6000');

        // ссылка для парсинга
        $clink = 'http://anion-msk.com/catalog/izdeliya-rotatsionnogo-formovaniya-1/';
        // рекурсивный парсинг
        $data = ParserController::getPage($clink);
        // создать базовую категорию для парсинга
        $baseCat = ParserController::getTitle($clink);
        // создать записи по результатам парсинга
        ParserController::createParser($baseCat->name, $data);
        // передача данных в представление
        return $this->render('index', [
            'data' => $data,
        ]);
    }

}
