<?php

namespace backend\controllers;

use common\models\Materials;
use common\models\Sections;
use common\models\SectionsDescont;
use common\models\SectionsFiles;
use common\models\SectionsInfo;
use common\models\SectionsLinks;
use common\models\SectionsMenu;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class MaterialsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'detail', 'create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'secondary'
            ],
        ];
    }


    public static function link2url($s)
    {
        $s = (string)$s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
        return $s; // возвращаем результат
    }

    public function actionIndex()
    {
        $model = Sections::find()->where(['id_type' => 2])->andWhere(['>', 'id_item', 0])->all();
        $materials = Materials::find()->all();
        $section = Sections::findOne(3);// 3 - Materials id in sections table
        $dopMenu = SectionsMenu::find()->where(['id_sections' => $section->id])->all();
        $links = SectionsLinks::find()->where(['id_sections' => $section->id])->all();
        $files = SectionsFiles::find()->where(['id_sections' => $section->id])->all();
        $info = SectionsInfo::find()->where(['id_sections' => $section->id])->all();
        $descont = SectionsDescont::find()->where(['id_sections' => $section->id])->all();

        if (Yii::$app->request->post('materials')) {
            if (Yii::$app->request->post('materials_id')) {
                $cmodel = Sections::findOne($_POST['materials_id']);
                $cmaterials = Materials::findOne($cmodel->id_item);
                $clink = MaterialsController::link2url($_POST['link']) ?: '';
                $clinkAll = $clink != '' ? Sections::find()->where(['id_type' => 2])->andWhere(['link' => $clink])->one() : '';
                $cmodel->link = isset($clinkAll) ? (string)$cmaterials->id : $clink;
                $cmaterials->date = (!$cmaterials->isActive && $_POST['isActive']) ? date('Y-m-d') : $cmaterials->date;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmaterials->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
                $cmaterials->save();
            }
            if (Yii::$app->request->post('section')) {
                $csection = Sections::findOne($section->id);
                $csection->id_item = $_POST['sections_sidebar_all'] ? -1 : 0;
                $csection->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('materials_id')) {
                $cmodel = Sections::findOne($_POST['materials_id']);
                $cmaterials = Materials::findOne($cmodel->id_item);
                $dopMenu = SectionsMenu::find()->where(['id_sections' => $cmodel->id])->all();
                $links = SectionsLinks::find()->where(['id_sections' => $cmodel->id])->all();
                $files = SectionsFiles::find()->where(['id_sections' => $cmodel->id])->all();
                $info = SectionsInfo::find()->where(['id_sections' => $cmodel->id])->all();
                $descont = SectionsDescont::find()->where(['id_sections' => $cmodel->id])->all();
                $del = array_merge($dopMenu, $links, $files, $info, $descont);
                foreach ($del as $d)
                    $d->delete();
                $cmodel->delete();
                $cmaterials->delete();
            }

            if (Yii::$app->request->post('dopMenu')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::findOne($section->id);
                    $csection->id_menu = $_POST['sections_dopMenu_id'];
                    $csection->menu_isActive = $_POST['sections_dopMenu_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('dopMenu_id')) {
                    $cmodel = SectionsMenu::findOne($_POST['dopMenu_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_menu = $_POST['dopMenu_idContent'];
                    $cmodel->priority = $_POST['dopMenu_priority'];
                    $cmodel->date = date("y-m-d", strtotime($_POST['dopMenu_date']));
                    $cmodel->date = (!$cmodel->isActive && $_POST['dopMenu_isActive']) ? date('Y-m-d') : $cmodel->date;
                    $cmodel->isActive = $_POST['dopMenu_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsMenu();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_menu = $_POST['dopMenu_idContent'];
                    $cmodel->priority = $_POST['dopMenu_priority'];
                    $cmodel->date = date("y-m-d", strtotime($_POST['dopMenu_date']));
                    $cmodel->date = (!$cmodel->isActive && $_POST['dopMenu_isActive']) ? date('Y-m-d') : $cmodel->date;
                    $cmodel->isActive = $_POST['dopMenu_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('dopMenu_id')) {
                    $cmodel = SectionsMenu::findOne($_POST['dopMenu_id']);
                    $csection = Sections::findOne($cmodel->id_sections);
                    if ($csection->id_menu == $cmodel->id) {
                        $cdopMenu = SectionsMenu::find()->where(['id_sections' => $section->id])->one();
                        $csection->id_menu = $cdopMenu->id;
                    }
                    $csection->save();
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('links')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::findOne($section->id);
                    $csection->priority_links = $_POST['sections_links_priority'];
                    $csection->links_isActive = $_POST['sections_links_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('links_id')) {
                    $cmodel = SectionsLinks::findOne($_POST['links_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_link = $_POST['links_idContent'];
                    $cmodel->priority = $_POST['links_priority'];
                    $cmodel->isActive = $_POST['links_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsLinks();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_link = $_POST['links_idContent'];
                    $cmodel->priority = $_POST['links_priority'];
                    $cmodel->isActive = $_POST['links_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('links_id')) {
                    $cmodel = SectionsLinks::findOne($_POST['links_id']);
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('files')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::findOne($section->id);
                    $csection->priority_files = $_POST['sections_files_priority'];
                    $csection->files_isActive = $_POST['sections_files_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('files_id')) {
                    $cmodel = SectionsFiles::findOne($_POST['files_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_files = $_POST['files_idContent'];
                    $cmodel->priority = $_POST['files_priority'];
                    $cmodel->isActive = $_POST['files_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsFiles();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_files = $_POST['files_idContent'];
                    $cmodel->priority = $_POST['files_priority'];
                    $cmodel->isActive = $_POST['files_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('files_id')) {
                    $cmodel = SectionsFiles::findOne($_POST['files_id']);
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('info')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::findOne($section->id);
                    $csection->priority_info = $_POST['sections_info_priority'];
                    $csection->info_isActive = $_POST['sections_info_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('info_id')) {
                    $cmodel = SectionsInfo::findOne($_POST['info_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_info = $_POST['info_idContent'];
                    $cmodel->priority = $_POST['info_priority'];
                    $cmodel->isActive = $_POST['info_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsInfo();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_info = $_POST['info_idContent'];
                    $cmodel->priority = $_POST['info_priority'];
                    $cmodel->isActive = $_POST['info_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('info_id')) {
                    $cmodel = SectionsInfo::findOne($_POST['info_id']);
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('descont')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::findOne($section->id);
                    $csection->priority_descont = $_POST['sections_descont_priority'];
                    $csection->descont_isActive = $_POST['sections_descont_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('descont_id')) {
                    $cmodel = SectionsDescont::findOne($_POST['descont_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_descont = $_POST['descont_idContent'];
                    $cmodel->priority = $_POST['descont_priority'];
                    $cmodel->isActive = $_POST['descont_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsDescont();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_descont = $_POST['descont_idContent'];
                    $cmodel->priority = $_POST['descont_priority'];
                    $cmodel->isActive = $_POST['descont_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('descont_id')) {
                    $cmodel = SectionsDescont::findOne($_POST['descont_id']);
                    $cmodel->delete();
                }
            }

            return $this->redirect(['/materials']);
        }

        return $this->render('index', [
            'model' => $model,
            'materials' => $materials,
            'section' => $section,
            'dopMenu' => $dopMenu,
            'links' => $links,
            'files' => $files,
            'info' => $info,
            'descont' => $descont,
        ]);
    }

    public function actionCreate()
    {
        if (Yii::$app->request->post('detail')) {
            if (Yii::$app->request->post('create')) {
                $cmodel = new Materials();
                $cmodel->title = $_POST['title'];
                $cmodel->text = $_POST['text'];
                $cmodel->img = $_POST['img'];
                $cmodel->date = date("y-m-d", strtotime($_POST['date']));
                $cmodel->date = (!$cmodel->isActive && $_POST['isActive']) ? date('Y-m-d') : $cmodel->date;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
                $csection = new Sections();
                $csection->id_type = 2;
                $csection->id_item = $cmodel->id;
                $clink = MaterialsController::link2url($cmodel->title) ?: '';
                $clinkAll = $clink != '' ? Sections::find()->where(['id_type' => 2])->andWhere(['link' => $clink])->one() : '';
                $csection->link = isset($clinkAll) ? $clink . '-' . (string)$cmodel->id : $clink;
                $csection->isActive = $_POST['isActive'] ? 1 : 0;
                $csection->save();
            }

            return $this->redirect(['/materials']);
        }

        return $this->render('create', []);
    }

    public function actionDetail($id)
    {
        $model = Materials::findOne($id);
        $section = Sections::find()->where(['id_type' => 2])->andWhere(['id_item' => $id])->one();
        $dopMenu = SectionsMenu::find()->where(['id_sections' => $section->id])->all();
        $links = SectionsLinks::find()->where(['id_sections' => $section->id])->all();
        $files = SectionsFiles::find()->where(['id_sections' => $section->id])->all();
        $info = SectionsInfo::find()->where(['id_sections' => $section->id])->all();
        $descont = SectionsDescont::find()->where(['id_sections' => $section->id])->all();

        if (Yii::$app->request->post('detail')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = Materials::findOne($_POST['id']);
                $csection = Sections::find()->where(['id_type' => 2])->andWhere(['id_item' => $cmodel->id])->one();
                $cmodel->title = $_POST['title'];
                $cmodel->text = $_POST['text'];
                $cmodel->img = $_POST['img'];
                $cmodel->date = date("y-m-d", strtotime($_POST['date']));
                $cmodel->date = (!$cmodel->isActive && $_POST['isActive']) ? date('Y-m-d') : $cmodel->date;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $csection->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
                $csection->save();
            }

            if (Yii::$app->request->post('dopMenu')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::find()->where(['id_type' => 2])->andWhere(['id_item' => $_POST['id']])->one();
                    $csection->id_menu = $_POST['sections_dopMenu_id'];
                    $csection->menu_isActive = $_POST['sections_dopMenu_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('dopMenu_id')) {
                    $cmodel = SectionsMenu::findOne($_POST['dopMenu_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_menu = $_POST['dopMenu_idContent'];
                    $cmodel->priority = $_POST['dopMenu_priority'];
                    $cmodel->date = date("y-m-d", strtotime($_POST['dopMenu_date']));
                    $cmodel->date = (!$cmodel->isActive && $_POST['dopMenu_isActive']) ? date('Y-m-d') : $cmodel->date;
                    $cmodel->isActive = $_POST['dopMenu_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsMenu();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_menu = $_POST['dopMenu_idContent'];
                    $cmodel->priority = $_POST['dopMenu_priority'];
                    $cmodel->date = date("y-m-d", strtotime($_POST['dopMenu_date']));
                    $cmodel->date = (!$cmodel->isActive && $_POST['dopMenu_isActive']) ? date('Y-m-d') : $cmodel->date;
                    $cmodel->isActive = $_POST['dopMenu_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('dopMenu_id')) {
                    $cmodel = SectionsMenu::findOne($_POST['dopMenu_id']);
                    $csection = Sections::findOne($cmodel->id_sections);
                    if ($csection->id_menu == $cmodel->id) {
                        $cdopMenu = SectionsMenu::find()->where(['id_sections' => $section->id])->one();
                        $csection->id_menu = $cdopMenu->id;
                    }
                    $csection->save();
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('links')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::find()->where(['id_type' => 2])->andWhere(['id_item' => $_POST['id']])->one();
                    $csection->priority_links = $_POST['sections_links_priority'];
                    $csection->links_isActive = $_POST['sections_links_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('links_id')) {
                    $cmodel = SectionsLinks::findOne($_POST['links_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_link = $_POST['links_idContent'];
                    $cmodel->priority = $_POST['links_priority'];
                    $cmodel->isActive = $_POST['links_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsLinks();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_link = $_POST['links_idContent'];
                    $cmodel->priority = $_POST['links_priority'];
                    $cmodel->isActive = $_POST['links_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('links_id')) {
                    $cmodel = SectionsLinks::findOne($_POST['links_id']);
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('files')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::find()->where(['id_type' => 2])->andWhere(['id_item' => $_POST['id']])->one();
                    $csection->priority_files = $_POST['sections_files_priority'];
                    $csection->files_isActive = $_POST['sections_files_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('files_id')) {
                    $cmodel = SectionsFiles::findOne($_POST['files_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_files = $_POST['files_idContent'];
                    $cmodel->priority = $_POST['files_priority'];
                    $cmodel->isActive = $_POST['files_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsFiles();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_files = $_POST['files_idContent'];
                    $cmodel->priority = $_POST['files_priority'];
                    $cmodel->isActive = $_POST['files_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('files_id')) {
                    $cmodel = SectionsFiles::findOne($_POST['files_id']);
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('info')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::find()->where(['id_type' => 2])->andWhere(['id_item' => $_POST['id']])->one();
                    $csection->priority_info = $_POST['sections_info_priority'];
                    $csection->info_isActive = $_POST['sections_info_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('info_id')) {
                    $cmodel = SectionsInfo::findOne($_POST['info_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_info = $_POST['info_idContent'];
                    $cmodel->priority = $_POST['info_priority'];
                    $cmodel->isActive = $_POST['info_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsInfo();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_info = $_POST['info_idContent'];
                    $cmodel->priority = $_POST['info_priority'];
                    $cmodel->isActive = $_POST['info_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('info_id')) {
                    $cmodel = SectionsInfo::findOne($_POST['info_id']);
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('descont')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::find()->where(['id_type' => 2])->andWhere(['id_item' => $_POST['id']])->one();
                    $csection->priority_descont = $_POST['sections_descont_priority'];
                    $csection->descont_isActive = $_POST['sections_descont_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('descont_id')) {
                    $cmodel = SectionsDescont::findOne($_POST['descont_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_descont = $_POST['descont_idContent'];
                    $cmodel->priority = $_POST['descont_priority'];
                    $cmodel->isActive = $_POST['descont_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsDescont();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_descont = $_POST['descont_idContent'];
                    $cmodel->priority = $_POST['descont_priority'];
                    $cmodel->isActive = $_POST['descont_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('descont_id')) {
                    $cmodel = SectionsDescont::findOne($_POST['descont_id']);
                    $cmodel->delete();
                }
            }

            return $this->redirect(['/materials/detail', 'id' => $id]);
        }

        return $this->render('detail', [
            'model' => $model,
            'section' => $section,
            'dopMenu' => $dopMenu,
            'links' => $links,
            'files' => $files,
            'info' => $info,
            'descont' => $descont,
        ]);
    }

}
