<?php

namespace backend\controllers;

use common\models\Cart;
use common\models\CartDetail;
use common\models\Feedback;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class MailController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index',
                            'mail', 'cart', 'detail-cart'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'secondary'
            ],
        ];
    }


    public function actionMail()
    {
        $model = Feedback::find()->orderBy(['date' => SORT_DESC])->all();

        if (Yii::$app->request->post('mail')) {
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = Feedback::findOne($_POST['id']);
                $cmodel->delete();
            }
            return $this->redirect(['/mail/mail']);
        }

        return $this->render('mail', [
            'model' => $model,
        ]);
    }

    public function actionCart()
    {
        $model = Cart::find()->orderBy(['date' => SORT_DESC])->all();

        if (Yii::$app->request->post('mail')) {
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = Cart::findOne($_POST['id']);
                $cmodelContent = CartDetail::find()->where(['id_cart' => $cmodel->id])->all();
                foreach ($cmodelContent as $cm)
                    $cm->delete();
                $cmodel->delete();
            }
            return $this->redirect(['/mail/cart']);
        }

        return $this->render('cart', [
            'model' => $model,
        ]);
    }

    public function actionDetailCart($id)
    {
        $model = CartDetail::find()->where(['id_cart' => $id])->all();
        $stock_id = Cart::findOne($id)->id_stock;

        return $this->render('detail-cart', [
            'model' => $model,
            'stock_id' => $stock_id,
        ]);
    }

}
