<?php

namespace backend\controllers;

use common\models\Products;
use common\models\Stock;
use common\models\StockContent;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class StockController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'secondary'
            ],
        ];
    }


    public function actionIndex()
    {
        $model = Stock::find()->all();

        if (Yii::$app->request->post('stock')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = Stock::findOne($_POST['id']);
                $cmodel->name = $_POST['name'];
                $cmodel->code = $_POST['code'];
                $cmodel->priority = $_POST['priority'];
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new Stock();
                $cmodel->name = $_POST['name'];
                $cmodel->code = $_POST['code'];
                $cmodel->priority = $_POST['priority'];
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
                $products = Products::find()->all();
                foreach ($products as $p) {
                    $sc = new StockContent();
                    $sc->stock_id = $cmodel->id;
                    $sc->product_id = $p->id;
                    $sc->price = StockContent::find()->where(['product_id' => $p->id])->one()->price ?: 0;
                    $sc->save();
                }
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = Stock::findOne($_POST['id']);
                $productsContent = StockContent::find()->where(['stock_id' => $cmodel->id])->all();
                foreach ($productsContent as $pc)
                    $pc->delete();
                $cmodel->delete();
            }
            return $this->redirect(['/stock']);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

}
