<?php

namespace backend\controllers;

use common\models\AttrContent;
use common\models\NavMenu;
use common\models\Products;
use common\models\ProductsAttr;
use common\models\ProductsImg;
use common\models\Sections;
use common\models\Stock;
use common\models\StockContent;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Writer_Excel2007;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

class DataController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'secondary'
            ],
        ];
    }


    public function actionIndex()
    {
        /*ini_set('max_execution_time', '6000');
        ini_set('mysql.connect_timeout', '6000');
        ini_set('mysql.wait_timeout', '6000');*/

        if (Yii::$app->request->post('export')) {
            require_once(Yii::getAlias('@vendor/phpexcel/PHPExcel.php'));
            $pExcel = new PHPExcel();
            $pExcel->setActiveSheetIndex(0);
            $aSheet = $pExcel->getActiveSheet();
            $aSheet->setTitle("Продукция");

            $aSheet->getColumnDimensionByColumn(0)->setWidth(5);
            $aSheet->setCellValueByColumnAndRow(0, 1, 'ID Продукта');
            $aSheet->getColumnDimensionByColumn(1)->setWidth(30);
            $aSheet->setCellValueByColumnAndRow(1, 1, 'Наименование');
            $aSheet->getColumnDimensionByColumn(2)->setWidth(5);
            $aSheet->setCellValueByColumnAndRow(2, 1, 'ID Категории');
            $aSheet->getColumnDimensionByColumn(3)->setWidth(30);
            $aSheet->setCellValueByColumnAndRow(3, 1, 'Категория');
            $aSheet->getColumnDimensionByColumn(4)->setWidth(15);
            $aSheet->setCellValueByColumnAndRow(4, 1, 'Артикул');
            $aSheet->getColumnDimensionByColumn(5)->setWidth(30);
            $aSheet->setCellValueByColumnAndRow(5, 1, 'Логотип товара');
            $aSheet->getColumnDimensionByColumn(6)->setWidth(30);
            $aSheet->setCellValueByColumnAndRow(6, 1, 'Изображения слайдера');
            $offset_main = 7;
            for ($i = 0; $i < $offset_main; $i++)
                $aSheet->mergeCellsByColumnAndRow($i, 1, $i, 2);

            // Склады
            $arr_stock = Stock::find()->orderBy('priority')->all();
            $aSheet->mergeCellsByColumnAndRow($offset_main, 1, $offset_main + count($arr_stock) - 1, 1);
            $aSheet->setCellValueByColumnAndRow($offset_main, 1, 'Склад');
            foreach ($arr_stock as $i => $item) {
                $col = $i + $offset_main;
                $aSheet->getColumnDimensionByColumn($col)->setWidth(15);
                $aSheet->setCellValueByColumnAndRow($col, 2, $item->name);
            }

            // Характеристики
            $offset_stock = $offset_main + count($arr_stock);
            $arr_attr = ProductsAttr::find()
                ->orderBy('sections_id')
                ->select(['name'])->distinct()
                ->all();
            $aSheet->mergeCellsByColumnAndRow($offset_stock, 1, $offset_stock +
                (count($arr_attr) ?: 1) - 1, 1);
            $aSheet->setCellValueByColumnAndRow($offset_stock, 1, 'Характеристика');
            foreach ($arr_attr as $i => $item) {
                $col = $i + $offset_stock;
                $aSheet->getColumnDimensionByColumn($col)->setWidth(15);
                $aSheet->setCellValueByColumnAndRow($col, 2, $item->name);
            }

            // Заполнение данными
            $arr_products = Products::find()->orderBy('nav_menu_id')->all();
            foreach ($arr_products as $i => $product) {
                $row = $i + 3;
                $section = Sections::find()->where(['id_type' => 1])
                    ->andWhere(['id_item' => $product->id])->one();
                $aSheet->setCellValueByColumnAndRow(0, $row, $product->id);
                $aSheet->setCellValueByColumnAndRow(1, $row, $product->title);
                $navMenu = NavMenu::findOne($product->nav_menu_id);
                $aSheet->setCellValueByColumnAndRow(2, $row, $navMenu->id);
                $aSheet->setCellValueByColumnAndRow(3, $row, $navMenu->name);
                $aSheet->setCellValueByColumnAndRow(4, $row, $product->article);
                $aSheet->setCellValueByColumnAndRow(5, $row,
                    array_pop(explode("/admin/upload/", $product->img)));

                $arr_imgs = ProductsImg::find()
                    ->where(['product_id' => $product->id])
                    ->orderBy('priority')
                    ->all();
                $product_img = '';
                foreach ($arr_imgs as $j => $item) {
                    if ($j > 0)
                        $product_img = $product_img . ',';
                    $product_img = $product_img .
                        array_pop(explode("/admin/upload/", $item->img));
                }
                $aSheet->setCellValueByColumnAndRow(6, $row, $product_img);
                foreach ($arr_stock as $j => $item) {
                    $col = $j + $offset_main;
                    $price = StockContent::find()->where(['stock_id' => $item->id])
                        ->andWhere(['product_id' => $product->id])->one();
                    $aSheet->setCellValueByColumnAndRow($col, $row, $price->price);
                }
                foreach ($arr_attr as $j => $item) {
                    $col = $j + $offset_stock;
                    $attr_id = ProductsAttr::find()->where(['sections_id' => $navMenu->external_id])
                        ->andWhere(['name' => $item->name])->one();
                    $attr = AttrContent::find()->where(['attr_id' => $attr_id->id])
                        ->andWhere(['sections_id' => $section->id])->one();
                    if (isset($attr))
                        if ($item->type == 'num')
                            $aSheet->setCellValueByColumnAndRow($col, $row, $attr->num);
                        else
                            $aSheet->setCellValueByColumnAndRow($col, $row, $attr->text);
                    else
                        $aSheet->setCellValueByColumnAndRow($col, $row, '-');
                }
            }

            header('Content-Type:xlsx:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition:attachment;filename="Продукция_' . Yii::$app->formatter->asDate(date('Y-m-d')) . '.xlsx"');
            $objWriter = new PHPExcel_Writer_Excel2007($pExcel);
            $objWriter->save('php://output');

            exit();
        }

        if (Yii::$app->request->post('import')) {
            if ($_FILES['spreadsheet']['name'] != '' && $_FILES['spreadsheet']['tmp_name'] != '') {
                $extension = strtoupper(array_pop(explode(".", $_FILES['spreadsheet']['name'])));
                if ($extension == 'XLSX' || $extension == 'ODS') {
                    $destination = Yii::getAlias('@webroot/import/');
                    $random = rand(1, 1000);
                    $uploadfile = md5(basename($_FILES['spreadsheet']['name']) . $random) . '.xlsx';
                    if (move_uploaded_file($_FILES['spreadsheet']['tmp_name'], $destination . $uploadfile)) {
                        chmod($destination . $uploadfile, 0777);
                        exec('php ' .
                            $_SERVER['DOCUMENT_ROOT'] . '/yii ' .
                            'data/import ' .
                            $destination . $uploadfile .
                            ' &'
                        );
                        /*var_dump('php ' .
                            $_SERVER['DOCUMENT_ROOT'] . '/yii ' .
                            'yii ' .
                            'data/import ' .
                            $destination . $uploadfile);die;
                        */
                    }
                }
            }
        }

        /*if (Yii::$app->request->post('import') && $_FILES['spreadsheet']['tmp_name']) {
            require_once(Yii::getAlias('@vendor/phpexcel/PHPExcel.php'));

            $inputFile = $_FILES['spreadsheet']['tmp_name'];
            $extension = strtoupper(array_pop(explode(".", $_FILES['spreadsheet']['name'])));
            if ($extension == 'XLSX' || $extension == 'ODS') {
                $inputFileType = PHPExcel_IOFactory::identify($inputFile);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFile);

                //Get worksheet dimensions
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //Loop through each row of the worksheet in turn
                $offset_main = 7;
                $rowData = [];
                for ($row = 3; $row <= $highestRow; $row++) {
                    //  Read a row of data into an array
                    $rowData[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                }
                $offset_attr = $offset_main + count(Stock::find()->orderBy('priority')->all());
                $offset_all = $offset_attr + count(ProductsAttr::find()->orderBy('sections_id')->all());
                foreach ($rowData as $item) {
                    $product = Products::findOne($item[0][0]);
                    if (!empty($product)) {
                        $section = Sections::find()->where(['id_type' => 1])
                            ->andWhere(['id_item' => $product->id])->one();
                        $navMenu = NavMenu::findOne($product->nav_menu_id);
                        $product->title = (string)$item[0][1];
                        $product->article = (string)$item[0][4];
                        $product->price = (float)$item[0][7] ?: 0;
                        $product->img = (string)$item[0][5] ? "/admin/upload/" . (string)$item[0][5] : '';
                        $arr_imgs = ProductsImg::find()->where(['product_id' => $product->id])->all();
                        foreach ($arr_imgs as $pic) $pic->delete();
                        $arr_imgs = explode(",", $item[0][6]);
                        foreach ($arr_imgs as $j => $pic) {
                            $cimg = new ProductsImg();
                            $cimg->product_id = $product->id;
                            $cimg->img = "/admin/upload/" . (string)$pic;
                            $cimg->priority = $j;
                            $cimg->save();
                        }
                        for ($i = $offset_attr; $i < $offset_all; $i++) {
                            $attr = ProductsAttr::find()
                                ->where(['name' => $sheet->getCellByColumnAndRow($i, 2)])
                                ->andWhere(['sections_id' => $navMenu->external_id])->one();
                            $attrCnt = AttrContent::find()->where(['attr_id' => $attr->id])
                                ->andWhere(['sections_id' => $section->id])->one();
                            if (isset($attrCnt) && isset($attr)) {
                                if ($attr->type == 'num') {
                                    $attrCnt->text = (string)$item[0][$i];
                                    $attrCnt->num = (float)$item[0][$i];
                                } else {
                                    $attrCnt->text = $item[0][$i];
                                    $attrCnt->num = 0;
                                }
                                $attrCnt->save();
                            } else {
                                $attrCnt = new AttrContent();
                                $attrCnt->attr_id = $attr->id;
                                $attrCnt->sections_id = $section->id;
                                if ($attr->type == 'num') {
                                    $attrCnt->text = (string)$item[0][$i];
                                    $attrCnt->num = (float)$item[0][$i];
                                } else {
                                    $attrCnt->text = $item[0][$i];
                                    $attrCnt->num = 0;
                                }
                                $attrCnt->save();
                            }
                        }
                        for ($i = $offset_main; $i < $offset_attr; $i++) {
                            $attr = Stock::find()
                                ->where(['name' => $sheet->getCellByColumnAndRow($i, 2)])
                                ->one();
                            $attrCnt = StockContent::find()->where(['stock_id' => $attr->id])
                                ->andWhere(['product_id' => $product->id])->one();
                            if (isset($attrCnt) && isset($attr)) {
                                $attrCnt->price = (float)$item[0][$i];
                                $attrCnt->save();
                            } else {
                                $cattrCnt = new StockContent();
                                $cattrCnt->stock_id = $attr->id;
                                $cattrCnt->product_id = $product->id;
                                $cattrCnt->price = (float)$item[0][$i] ? (float)$item[0][$i] : 0;
                                $cattrCnt->save();
                            }
                        }
                        $product->save();
                    } else {
                        $navMenu = NavMenu::findOne($item[0][2]);
                        $product = new Products();
                        $product->nav_menu_id = $navMenu->id;
                        $product->title = (string)$item[0][1];
                        $product->article = (string)$item[0][4];
                        $product->price = (float)$item[0][7] ?: 0;
                        $product->img = (string)$item[0][5] ? "/admin/upload/" . (string)$item[0][5] : '';
                        $product->date = date('Y-m-d');
                        $product->isActive = 1;
                        $product->isComplect = 0;
                        $product->save();
                        $arr_imgs = explode(",", $item[0][6]);
                        foreach ($arr_imgs as $j => $pic) {
                            $cimg = new ProductsImg();
                            $cimg->product_id = $product->id;
                            $cimg->img = "/admin/upload/" . (string)$pic;
                            $cimg->priority = $j;
                            $cimg->save();
                        }
                        $section = new Sections();
                        $section->id_type = 1;
                        $section->id_item = $product->id;
                        $section->isActive = 1;
                        $section->save();
                        for ($i = $offset_attr; $i < $offset_all; $i++) {
                            $attr = ProductsAttr::find()
                                ->where(['name' => $sheet->getCellByColumnAndRow($i, 2)])
                                ->andWhere(['sections_id' => $navMenu->external_id])->one();
                            $attrCnt = new AttrContent();
                            $attrCnt->attr_id = $attr->id;
                            $attrCnt->sections_id = $section->id;
                            if ($attr->type == 'num') {
                                $attrCnt->text = (string)$item[0][$i];
                                $attrCnt->num = (float)$item[0][$i];
                            } else {
                                $attrCnt->text = $item[0][$i];
                                $attrCnt->num = 0;
                            }
                            $attrCnt->save();
                        }
                        for ($i = $offset_main; $i < $offset_attr; $i++) {
                            $attr = Stock::find()
                                ->where(['name' => $sheet->getCellByColumnAndRow($i, 2)])
                                ->one();
                            $cattrCnt = new StockContent();
                            $cattrCnt->stock_id = $attr->id;
                            $cattrCnt->product_id = $product->id;
                            $cattrCnt->price = (float)$item[0][$i] ? (float)$item[0][$i] : 0;
                            $cattrCnt->save();
                        }
                    }
                }
                //var_dump('<pre>', $rowData);die;
            }
            return $this->redirect(['/data']);
        }*/

        return $this->render('index', []);
    }

}
