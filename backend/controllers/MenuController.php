<?php

namespace backend\controllers;

use common\models\AttrContent;
use common\models\FiltersContent;
use common\models\Materials;
use common\models\NavMenu;
use common\models\NavMenuImg;
use common\models\Products;
use common\models\ProductsAttr;
use common\models\Sections;
use common\models\SectionsDescont;
use common\models\SectionsFiles;
use common\models\SectionsInfo;
use common\models\SectionsLinks;
use common\models\SectionsMenu;
use common\models\SectionsType;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class MenuController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index',
                            'main', 'materials',
                            'about', 'contacts',
                            'products', 'detail-products',
                            'detail-attr'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'secondary'
            ],
        ];
    }


    public static function link2url($s)
    {
        $s = (string)$s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
        return $s; // возвращаем результат
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionMain()
    {
        $model = SectionsType::find()->all();

        if (Yii::$app->request->post('main')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = SectionsType::findOne($_POST['id']);
                $cmodel->name = $_POST['name'];
                $cmodel->priority = $_POST['priority'];
                if (!$cmodel->isActive && $_POST['isActive']) {
                    $cmodel->datePublic = date('Y-m-d');
                }
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            return $this->redirect(['/menu/main']);
        }

        return $this->render('main', [
            'model' => $model,
        ]);
    }

    public function actionMaterials()
    {
        $model = NavMenu::find()->where(['menu_type' => 2])
            ->orderBy(['priority' => SORT_ASC, 'menu_level' => SORT_ASC])
            ->all();
        $materials = Materials::find()->all();

        if (Yii::$app->request->post('materials')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = NavMenu::findOne($_POST['id']);
                $cmodel->parent_id = $_POST['parent_id'];
                if ($_POST['parent_id'] != 0) {
                    $childrens = NavMenu::find()->where(['parent_id' => $_POST['id']])->all();
                    foreach ($childrens as $children)
                        $children->delete();
                }
                $cmodel->external_id = $_POST['external_id'];
                $cmodel->menu_level = (NavMenu::findOne($_POST['parent_id'])->menu_level ?: 0) + 1;
                $cmodel->name = trim($_POST['name']) ?: '';
                $cmodel->priority = $_POST['priority'];
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new NavMenu();
                $cmodel->menu_type = 2;
                $cmodel->parent_id = $_POST['parent_id'];
                $cmodel->external_id = $_POST['external_id'];
                $cmodel->menu_level = (NavMenu::findOne($_POST['parent_id'])->menu_level ?: 0) + 1;
                $cmodel->name = trim($_POST['name']) ?: '';
                $cmodel->priority = $_POST['priority'];
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = NavMenu::findOne($_POST['id']);
                $childrens = NavMenu::find()->where(['parent_id' => $_POST['id']])->all();
                foreach ($childrens as $children)
                    $children->delete();
                $cmodel->delete();
            }

            return $this->redirect(['/menu/materials']);
        }

        return $this->render('materials', [
            'model' => $model,
            'materials' => $materials,
        ]);
    }

    public function actionProducts()
    {
        $model = NavMenu::find()->where(['menu_type' => 1])
            ->orderBy(['priority' => SORT_ASC, 'menu_level' => SORT_ASC])
            ->all();

        if (Yii::$app->request->post('products')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = NavMenu::findOne($_POST['id']);
                $cmodel->parent_id = $_POST['parent_id'];
                if ($_POST['parent_id'] != 0) {
                    $childrens = NavMenu::find()->where(['parent_id' => $_POST['id']])->all();
                    foreach ($childrens as $children) {
                        $children->parent_id = $_POST['parent_id'];
                        $children->save();
                    }
                }
                $cmodel->menu_level = (NavMenu::findOne($_POST['parent_id'])->menu_level ?: 0) + 1;
                $cmodel->name = trim($_POST['name']) ?: '';
                $tmpLink = MenuController::link2url($_POST['link']);
                $clink = $tmpLink != '' ? $tmpLink : MenuController::link2url($cmodel->name);
                $clinkAll = $clink != '' ? Sections::find()->where(['id_type' => 1])->andWhere(['link' => $clink])->one() : '';
                $cmodel->link = $clinkAll != '' ? $clink . '-' . (string)$cmodel->id : $clink;
                $cmodel->tag = $_POST['tag'];
                $cmodel->tag_isActive = $_POST['tag_isActive'] ? 1 : 0;
                $cmodel->isComplect = $_POST['isComplect'] ? 1 : 0;
                $cmodel->priority = $_POST['priority'];
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
                $csection = Sections::findOne($cmodel->external_id);
                $csection->link = $cmodel->link;
                $csection->isActive = $_POST['isActive'] ? 1 : 0;
                $csection->save();
                $cimg = NavMenuImg::find()->where(['product_id' => $cmodel->id])->one();
                if ($cimg) $cimg->delete();
                $cimg = new NavMenuImg();
                $cimg->product_id = $cmodel->id;
                $cimg->img = $_POST['img'] ?: '/admin/upload/no-image.png';
                $cimg->save();
            }
            if (Yii::$app->request->post('create')) {
                $csection = new Sections();
                $csection->id_type = 1;
                $csection->id_item = 0;
                $csection->isActive = $_POST['isActive'] ? 1 : 0;
                $csection->save();
                $cmodel = new NavMenu();
                $cmodel->menu_type = 1;
                $cmodel->parent_id = $_POST['parent_id'];
                $cmodel->external_id = $csection->id;
                $cmodel->menu_level = (NavMenu::findOne($_POST['parent_id'])->menu_level ?: 0) + 1;
                $cmodel->name = trim($_POST['name']) ?: '';
                $cmodel->tag = $_POST['tag'];
                $cmodel->tag_isActive = $_POST['tag_isActive'] ? 1 : 0;
                $cmodel->isComplect = $_POST['isComplect'] ? 1 : 0;
                $cmodel->priority = $_POST['priority'];
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
                $tmpLink = MenuController::link2url($_POST['link']);
                $clink = $tmpLink != '' ? $tmpLink : MenuController::link2url($cmodel->name);
                $clinkAll = $clink != '' ? Sections::find()->where(['id_type' => 1])->andWhere(['link' => $clink])->one() : '';
                $cmodel->link = $clinkAll != '' ? $clink . '-' . (string)$cmodel->id : $clink;
                $cmodel->save();
                $csection->link = $cmodel->link;
                $csection->save();
                $cimg = new NavMenuImg();
                $cimg->product_id = $cmodel->id;
                $cimg->img = $_POST['img'] ?: '/admin/upload/no-image.png';
                $cimg->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = NavMenu::findOne($_POST['id']);
                $csection = Sections::findOne($cmodel->external_id);
                $childrens = NavMenu::find()->where(['parent_id' => $_POST['id']])->all();
                foreach ($childrens as $children) {
                    $children->parent_id = NavMenu::findOne($cmodel->parent_id)->parent_id ?: 0;
                    $children->menu_level = NavMenu::findOne($cmodel->parent_id)->menu_level ?: 1;
                    $children->save();
                }
                $cimg = NavMenuImg::find()->where(['product_id' => $cmodel->id])->one();
                if ($cimg) $cimg->delete();
                // delete sidebar and filters with content
                $dopMenu = SectionsMenu::find()->where(['id_sections' => $csection->id])->all();
                $links = SectionsLinks::find()->where(['id_sections' => $csection->id])->all();
                $files = SectionsFiles::find()->where(['id_sections' => $csection->id])->all();
                $info = SectionsInfo::find()->where(['id_sections' => $csection->id])->all();
                $descont = SectionsDescont::find()->where(['id_sections' => $csection->id])->all();
                $del = array_merge($dopMenu, $links, $files, $info, $descont);
                foreach ($del as $d)
                    $d->delete();
                $cattrs = ProductsAttr::find()->where(['sections_id' => $csection->id])->all();
                foreach ($cattrs as $a) {
                    $cfilters = FiltersContent::find()->where(['attr_id' => $a->id])->all();
                    foreach ($cfilters as $d)
                        $d->delete();
                    $cattrContent = AttrContent::find()->where(['attr_id' => $a->id])->all();
                    foreach ($cattrContent as $d)
                        $d->delete();
                    $a->delete();
                }

                $csection->delete();
                $cmodel->delete();
            }

            return $this->redirect(['/menu/products']);
        }

        return $this->render('products', [
            'model' => $model,
        ]);
    }

    public function actionDetailProducts($id)
    {
        $model = NavMenu::find()->where(['menu_type' => 1])->andWhere(['external_id' => $id])->one();
        $attr = ProductsAttr::find()->where(['sections_id' => $id])->all();
        $section = Sections::findOne($id);
        $dopMenu = SectionsMenu::find()->where(['id_sections' => $id])->all();
        $links = SectionsLinks::find()->where(['id_sections' => $id])->all();
        $files = SectionsFiles::find()->where(['id_sections' => $id])->all();
        $info = SectionsInfo::find()->where(['id_sections' => $id])->all();
        $descont = SectionsDescont::find()->where(['id_sections' => $id])->all();

        if (Yii::$app->request->post('products')) {
            if (Yii::$app->request->post('products_id')) {
                $cmodel = ProductsAttr::findOne($_POST['products_id']);
                $cmodel->name = trim($_POST['name']) ?: '';
                //$cmodel->type = $_POST['type'];
                $cmodel->priority = $_POST['priority'];
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->isFilter = $_POST['isFilter'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new ProductsAttr();
                $cmodel->sections_id = $section->id;
                $cmodel->name = trim($_POST['name']) ?: '';
                $cmodel->type = $_POST['type'];
                $cmodel->priority = $_POST['priority'];
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->isFilter = $_POST['isFilter'] ? 1 : 0;
                $cmodel->save();
                $cmenu = NavMenu::find()->where(['external_id' => $section->id])
                    ->andWhere(['menu_type' => 1])->one();
                $cproducts = Products::find()->where(['nav_menu_id' => $cmenu->id])->all();
                foreach ($cproducts as $p) {
                    $cattrContent = new AttrContent();
                    $cattrContent->attr_id = $cmodel->id;
                    $cattrContent->sections_id = Sections::find()->where(['id_type' => 1])
                        ->andWhere(['id_item' => $p->id])->one()->id;
                    $cattrContent->save();
                }
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('products_id')) {
                $cmodel = ProductsAttr::findOne($_POST['products_id']);
                $cattrs = FiltersContent::find()->where(['attr_id' => $_POST['products_id']])->all();
                foreach ($cattrs as $d)
                    $d->delete();
                $cattrContent = AttrContent::find()->where(['attr_id' => $_POST['products_id']])->all();
                foreach ($cattrContent as $d)
                    $d->delete();
                $cmodel->delete();
            }

            if (Yii::$app->request->post('section')) {
                $csection = Sections::findOne($section->id);
                $csection->id_item = $_POST['sections_sidebar_all'] ? -1 : 0;
                $csection->save();
            }

            if (Yii::$app->request->post('dopMenu')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::findOne($section->id);
                    $csection->id_menu = $_POST['sections_dopMenu_id'];
                    $csection->menu_isActive = $_POST['sections_dopMenu_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('dopMenu_id')) {
                    $cmodel = SectionsMenu::findOne($_POST['dopMenu_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_menu = $_POST['dopMenu_idContent'];
                    $cmodel->priority = $_POST['dopMenu_priority'];
                    $cmodel->date = date("y-m-d", strtotime($_POST['dopMenu_date']));
                    $cmodel->date = (!$cmodel->isActive && $_POST['dopMenu_isActive']) ? date('Y-m-d') : $cmodel->date;
                    $cmodel->isActive = $_POST['dopMenu_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsMenu();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_menu = $_POST['dopMenu_idContent'];
                    $cmodel->priority = $_POST['dopMenu_priority'];
                    $cmodel->date = date("y-m-d", strtotime($_POST['dopMenu_date']));
                    $cmodel->date = (!$cmodel->isActive && $_POST['dopMenu_isActive']) ? date('Y-m-d') : $cmodel->date;
                    $cmodel->isActive = $_POST['dopMenu_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('dopMenu_id')) {
                    $cmodel = SectionsMenu::findOne($_POST['dopMenu_id']);
                    $csection = Sections::findOne($cmodel->id_sections);
                    if ($csection->id_menu == $cmodel->id) {
                        $cdopMenu = SectionsMenu::find()->where(['id_sections' => $section->id])->one();
                        $csection->id_menu = $cdopMenu->id;
                    }
                    $csection->save();
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('links')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::findOne($section->id);
                    $csection->priority_links = $_POST['sections_links_priority'];
                    $csection->links_isActive = $_POST['sections_links_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('links_id')) {
                    $cmodel = SectionsLinks::findOne($_POST['links_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_link = $_POST['links_idContent'];
                    $cmodel->priority = $_POST['links_priority'];
                    $cmodel->isActive = $_POST['links_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsLinks();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_link = $_POST['links_idContent'];
                    $cmodel->priority = $_POST['links_priority'];
                    $cmodel->isActive = $_POST['links_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('links_id')) {
                    $cmodel = SectionsLinks::findOne($_POST['links_id']);
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('files')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::findOne($section->id);
                    $csection->priority_files = $_POST['sections_files_priority'];
                    $csection->files_isActive = $_POST['sections_files_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('files_id')) {
                    $cmodel = SectionsFiles::findOne($_POST['files_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_files = $_POST['files_idContent'];
                    $cmodel->priority = $_POST['files_priority'];
                    $cmodel->isActive = $_POST['files_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsFiles();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_files = $_POST['files_idContent'];
                    $cmodel->priority = $_POST['files_priority'];
                    $cmodel->isActive = $_POST['files_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('files_id')) {
                    $cmodel = SectionsFiles::findOne($_POST['files_id']);
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('info')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::findOne($section->id);
                    $csection->priority_info = $_POST['sections_info_priority'];
                    $csection->info_isActive = $_POST['sections_info_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('info_id')) {
                    $cmodel = SectionsInfo::findOne($_POST['info_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_info = $_POST['info_idContent'];
                    $cmodel->priority = $_POST['info_priority'];
                    $cmodel->isActive = $_POST['info_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsInfo();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_info = $_POST['info_idContent'];
                    $cmodel->priority = $_POST['info_priority'];
                    $cmodel->isActive = $_POST['info_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('info_id')) {
                    $cmodel = SectionsInfo::findOne($_POST['info_id']);
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('descont')) {
                if (Yii::$app->request->post('section')) {
                    $csection = Sections::findOne($section->id);
                    $csection->priority_descont = $_POST['sections_descont_priority'];
                    $csection->descont_isActive = $_POST['sections_descont_isActive'] ? 1 : 0;
                    $csection->save();
                }
                if (Yii::$app->request->post('descont_id')) {
                    $cmodel = SectionsDescont::findOne($_POST['descont_id']);
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_descont = $_POST['descont_idContent'];
                    $cmodel->priority = $_POST['descont_priority'];
                    $cmodel->isActive = $_POST['descont_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new SectionsDescont();
                    $cmodel->id_sections = $section->id;
                    $cmodel->id_descont = $_POST['descont_idContent'];
                    $cmodel->priority = $_POST['descont_priority'];
                    $cmodel->isActive = $_POST['descont_isActive'] ? 1 : 0;
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('descont_id')) {
                    $cmodel = SectionsDescont::findOne($_POST['descont_id']);
                    $cmodel->delete();
                }
            }

            return $this->redirect(['/menu/detail-products', 'id' => $id]);
        }

        return $this->render('detail-products', [
            'model' => $model,
            'attr' => $attr,
            'section' => $section,
            'dopMenu' => $dopMenu,
            'links' => $links,
            'files' => $files,
            'info' => $info,
            'descont' => $descont,
        ]);
    }

    public function actionDetailAttr($id)
    {
        $model = ProductsAttr::findOne($id);
        $content = FiltersContent::find()->where(['attr_id' => $id])->all();

        if (Yii::$app->request->post('filters')) {
            if (Yii::$app->request->post('update')) {
                foreach ($content as $c) {
                    $c->text = $_POST['text_' . $c->id];
                    $c->save();
                }
            }
            if (Yii::$app->request->post('create')) {
                $c = new FiltersContent();
                $c->attr_id = $id;
                $c->text = $_POST['text_new'];
                $c->save();
                return $this->redirect(['/menu/detail-attr', 'id' => $model->id]);
            }
            if (Yii::$app->request->post('delete')) {
                $c = FiltersContent::findOne($_POST['delete']);
                $c->delete();
                return $this->redirect(['/menu/detail-attr', 'id' => $model->id]);
            }

            return $this->redirect(['/menu/detail-products', 'id' => $model->sections_id]);
        }

        return $this->render('detail-attr', [
            'model' => $model,
            'content' => $content,
        ]);
    }

    public function actionAbout()
    {
        $model = Sections::findOne(5);

        if (Yii::$app->request->post('menu')) {
            $model->id_item = $_POST['materials'];
            $model->save();

            return $this->redirect(['/menu/about']);
        }

        return $this->render('about', [
            'model' => $model,
        ]);
    }

    public function actionContacts()
    {
        $model = Sections::findOne(6);

        if (Yii::$app->request->post('menu')) {
            $model->id_item = $_POST['materials'];
            $model->save();

            return $this->redirect(['/menu/contacts']);
        }

        return $this->render('contacts', [
            'model' => $model,
        ]);
    }


}
