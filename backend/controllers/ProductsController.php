<?php

namespace backend\controllers;

use common\models\AttrContent;
use common\models\NavMenu;
use common\models\Products;
use common\models\ProductsAttr;
use common\models\ProductsImg;
use common\models\Sections;
use common\models\Stock;
use common\models\StockContent;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class ProductsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'detail', 'create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'secondary'
            ],
        ];
    }


    public function actionIndex()
    {
        $model = Products::find()->all();

        if (Yii::$app->request->post('products')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = Sections::find()->where(['id_type' => 1])
                    ->andWhere(['id_item' => $_POST['id']])
                    ->one();
                $cproducts = Products::findOne($_POST['id']);
                $cmodel->link = $_POST['link'];
                $cproducts->link = $_POST['link'];
                $cproducts->date = (!$cproducts->isActive && $_POST['isActive']) ? date('Y-m-d') : $cproducts->date;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cproducts->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
                $cproducts->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = Products::findOne($_POST['id']);
                $csection = Sections::find()->where(['id_type' => 1])
                    ->andWhere(['id_item' => $_POST['id']])
                    ->one();
                $cattr = AttrContent::find()->where(['sections_id' => $csection->id])->all();
                foreach ($cattr as $a)
                    $a->delete();
                $cslider = ProductsImg::find()->where(['product_id' => $cmodel->id])->all();
                foreach ($cslider as $s)
                    $s->delete();
                $cprice = StockContent::find()->where(['product_id' => $cmodel->id])->all();
                foreach ($cprice as $cp)
                    $cp->delete();
                $cmodel->delete();
                $csection->delete();
            }

            return $this->redirect(['/products/index']);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionCreate()
    {
        if (Yii::$app->request->post('detail')) {
            if (Yii::$app->request->post('create')) {
                $cmodel = new Products();
                $cmodel->nav_menu_id = $_POST['nav_menu'];
                $cmodel->title = $_POST['title'];
                $cmodel->isComplect = $_POST['isComplect'] ? 1 : 0;
                $cmodel->text = $_POST['text'];
                $cmodel->article = $_POST['article'];
                $cmodel->price = $_POST['price'];
                $cmodel->img = $_POST['img'] ?: '/admin/upload/no-image.png';
                $cmodel->link = $_POST['link'];
                $cmodel->date = date("y-m-d", strtotime($_POST['date']));
                $cmodel->date = (!$cmodel->isActive && $_POST['isActive']) ? date('Y-m-d') : $cmodel->date;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
                $csection = new Sections();
                $csection->id_type = 1;
                $csection->id_item = $cmodel->id;
                $csection->link = $_POST['link'];
                $csection->isActive = $_POST['isActive'] ? 1 : 0;
                $csection->save();
                $cmenu = NavMenu::findOne($_POST['nav_menu']);
                $cattr = ProductsAttr::find()->where(['sections_id' => $cmenu->external_id])->all();
                foreach ($cattr as $a) {
                    $attrContent = new AttrContent();
                    $attrContent->attr_id = $a->id;
                    $attrContent->sections_id = $csection->id;
                    $attrContent->save();
                }
                $priceStocks = Stock::find()->all();
                foreach ($priceStocks as $ps) {
                    $pc = new StockContent();
                    $pc->stock_id = $ps->id;
                    $pc->product_id = $cmodel->id;
                    $pc->price = $_POST['price'] ?: 0;
                    $pc->save();
                }
            }

            return $this->redirect(['/products']);
        }

        return $this->render('create', []);
    }

    public function actionDetail($id)
    {
        $model = Products::findOne($id);
        $section = Sections::find()->where(['id_type' => 1])
            ->andWhere(['id_item' => $id])
            ->one();
        $attr = AttrContent::find()->where(['sections_id' => $section->id])->all();
        $slider = ProductsImg::find()->where(['product_id' => $model->id])->all();
        $stocks = StockContent::find()->where(['product_id' => $model->id])->all();

        if (Yii::$app->request->post('detail')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = Products::findOne($_POST['id']);
                $csection = Sections::find()->where(['id_type' => 1])
                    ->andWhere(['id_item' => $_POST['id']])
                    ->one();
                if ($cmodel->nav_menu_id == $_POST['nav_menu'])
                    $cmodel->nav_menu_id = $_POST['nav_menu'];
                else {
                    $cmenu = NavMenu::findOne($cmodel->nav_menu_id);
                    $cattr = AttrContent::find()->where(['sections_id' => $cmenu->external_id])->all();
                    foreach ($cattr as $a)
                        $a->delete();
                    $cmodel->nav_menu_id = $_POST['nav_menu'];
                    $cmenu = NavMenu::findOne($_POST['nav_menu']);
                    $cattrNew = ProductsAttr::find()->where(['sections_id' => $cmenu->external_id])->all();
                    foreach ($cattrNew as $a) {
                        $an = new AttrContent();
                        $an->attr_id = $a->id;
                        $an->sections_id = $csection->id;
                        $an->save();
                    }
                }
                $cmodel->title = $_POST['title'];
                $cmodel->isComplect = $_POST['isComplect'] ? 1 : 0;
                $cmodel->text = $_POST['text'];
                $cmodel->article = $_POST['article'];
                //$cmodel->price = $_POST['price'];
                $cmodel->img = $_POST['img'];
                $cmodel->link = $_POST['link'];
                $cmodel->date = date("y-m-d", strtotime($_POST['date']));
                $cmodel->date = (!$cmodel->isActive && $_POST['isActive']) ? date('Y-m-d') : $cmodel->date;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
                $csection->link = $_POST['link'];
                $csection->isActive = $_POST['isActive'] ? 1 : 0;
                $csection->save();
            }

            if (Yii::$app->request->post('slider')) {
                if (Yii::$app->request->post('slider_id')) {
                    $cmodel = ProductsImg::findOne($_POST['slider_id']);
                    $cmodel->name = $_POST['slider_name'];
                    $cmodel->img = $_POST['slider_img'];
                    $cmodel->priority = $_POST['slider_priority'];
                    $cmodel->save();
                }
                if (Yii::$app->request->post('create')) {
                    $cmodel = new ProductsImg();
                    $cmodel->product_id = $model->id;
                    $cmodel->name = $_POST['slider_name'];
                    $cmodel->img = $_POST['slider_img'];
                    $cmodel->priority = $_POST['slider_priority'];
                    $cmodel->save();
                }
                if (Yii::$app->request->post('delete') && Yii::$app->request->post('slider_id')) {
                    $cmodel = ProductsImg::findOne($_POST['slider_id']);
                    $cmodel->delete();
                }
            }

            if (Yii::$app->request->post('attr')) {
                foreach ($attr as $c) {
                    $c->text = (string)$_POST['text_' . $c->id];
                    $c->num = (float)$_POST['text_' . $c->id];
                    $c->save();
                }
            }

            if (Yii::$app->request->post('stock')) {
                foreach ($stocks as $s) {
                    $s->price = (float)$_POST['price_' . $s->id];
                    $s->save();
                }
            }

            return $this->redirect(['/products/detail', 'id' => $id]);
        }

        return $this->render('detail', [
            'model' => $model,
            'attr' => $attr,
            'slider' => $slider,
            'stocks' => $stocks,
        ]);
    }

}
