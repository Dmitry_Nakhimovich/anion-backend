<?php

namespace backend\controllers;

use common\models\Descont;
use common\models\DopMenu;
use common\models\DopMenuContent;
use common\models\Files;
use common\models\Info;
use common\models\Links;
use common\models\Sections;
use common\models\SectionsDescont;
use common\models\SectionsFiles;
use common\models\SectionsInfo;
use common\models\SectionsLinks;
use common\models\SectionsMenu;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class SidebarController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index',
                            'menu', 'detail-menu',
                            'links', 'files', 'info', 'descont',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'secondary'
            ],
        ];
    }


    public function actionMenu()
    {
        $model = DopMenu::find()->all();

        if (Yii::$app->request->post('menu')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = DopMenu::findOne($_POST['id']);
                $cmodel->name = $_POST['name'];
                $cmodel->link = $_POST['link'];
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new DopMenu();
                $cmodel->name = $_POST['name'];
                $cmodel->link = $_POST['link'];
                $cmodel->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = DopMenu::findOne($_POST['id']);
                $content = DopMenuContent::find()->where(['id_dop_menu' => $_POST['id']])->all();
                $csection = SectionsMenu::find()->where(['id_menu' => $_POST['id']])->all();
                foreach ($csection as $s) {
                    $cs = Sections::findOne($s->id_sections);
                    if ($cs->id_menu == $s->id)
                        $cs->id_menu = 0;
                    $cs->save();
                    $s->delete();
                }
                foreach ($content as $c)
                    $c->delete();
                $cmodel->delete();
            }
            return $this->redirect(['/sidebar/menu']);
        }

        return $this->render('menu', [
            'model' => $model,
        ]);
    }

    public function actionDetailMenu($id)
    {
        $model = DopMenuContent::find()->where(['id_dop_menu' => $id])->all();

        if (Yii::$app->request->post('detail-menu')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = DopMenuContent::findOne($_POST['id']);
                $cmodel->name = $_POST['name'];
                $cmodel->link = $_POST['link'] ?: '';
                $cmodel->tag = $_POST['tag'] ?: '';
                $cmodel->tag_isActive = $_POST['tag_isActive'] ? 1 : 0;
                $cmodel->priority = $_POST['priority'] ?: 0;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new DopMenuContent();
                $cmodel->id_dop_menu = $id;
                $cmodel->name = $_POST['name'];
                $cmodel->link = $_POST['link'] ?: '';
                $cmodel->tag = $_POST['tag'] ?: '';
                $cmodel->tag_isActive = $_POST['tag_isActive'] ? 1 : 0;
                $cmodel->priority = $_POST['priority'] ?: 0;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = DopMenuContent::findOne($_POST['id']);
                $cmodel->delete();
            }
            return $this->redirect(['/sidebar/detail-menu', 'id' => $id]);
        }

        return $this->render('detail-menu', [
            'model' => $model,
            'model_id' => $id,
        ]);
    }

    public function actionLinks()
    {
        $model = Links::find()->all();

        if (Yii::$app->request->post('links')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = Links::findOne($_POST['id']);
                $cmodel->name = $_POST['name'];
                $cmodel->link = $_POST['link'];
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new Links();
                $cmodel->name = $_POST['name'];
                $cmodel->link = $_POST['link'];
                $cmodel->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = Links::findOne($_POST['id']);
                $csection = SectionsLinks::find()->where(['id_links' => $_POST['id']])->all();
                foreach ($csection as $s)
                    $s->delete();
                $cmodel->delete();
            }
            return $this->redirect(['/sidebar/links']);
        }

        return $this->render('links', [
            'model' => $model,
        ]);
    }

    public function actionFiles()
    {
        $model = Files::find()->all();

        if (Yii::$app->request->post('files')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = Files::findOne($_POST['id']);
                $cmodel->name = $_POST['name'];
                $cmodel->file = $_POST['file'];
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new Files();
                $cmodel->name = $_POST['name'];
                $cmodel->file = $_POST['file'];
                $cmodel->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = Files::findOne($_POST['id']);
                $csection = SectionsFiles::find()->where(['id_files' => $_POST['id']])->all();
                foreach ($csection as $s)
                    $s->delete();
                $cmodel->delete();
            }
            return $this->redirect(['/sidebar/files']);
        }

        return $this->render('files', [
            'model' => $model,
        ]);
    }

    public function actionInfo()
    {
        $model = Info::find()->all();

        if (Yii::$app->request->post('info')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = Info::findOne($_POST['id']);
                $cmodel->name = $_POST['name'];
                $cmodel->text = $_POST['text'];
                $cmodel->date = date("y-m-d", strtotime($_POST['date']));
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new Info();
                $cmodel->name = $_POST['name'];
                $cmodel->text = $_POST['text'];
                $cmodel->date = date("y-m-d", strtotime($_POST['date']));
                $cmodel->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = Info::findOne($_POST['id']);
                $csection = SectionsInfo::find()->where(['id_info' => $_POST['id']])->all();
                foreach ($csection as $s)
                    $s->delete();
                $cmodel->delete();
            }
            return $this->redirect(['/sidebar/info']);
        }

        return $this->render('info', [
            'model' => $model,
        ]);
    }

    public function actionDescont()
    {
        $model = Descont::find()->all();

        if (Yii::$app->request->post('descont')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = Descont::findOne($_POST['id']);
                $cmodel->name = $_POST['name'];
                $cmodel->title = $_POST['title'];
                $cmodel->link = $_POST['link'];
                $cmodel->text = $_POST['text'];
                $cmodel->img = $_POST['img'];
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new Descont();
                $cmodel->name = $_POST['name'];
                $cmodel->title = $_POST['title'];
                $cmodel->link = $_POST['link'];
                $cmodel->text = $_POST['text'];
                $cmodel->img = $_POST['img'];
                $cmodel->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = Descont::findOne($_POST['id']);
                $csection = SectionsDescont::find()->where(['id_descont' => $_POST['id']])->all();
                foreach ($csection as $s)
                    $s->delete();
                $cmodel->delete();
            }
            return $this->redirect(['/sidebar/descont']);
        }

        return $this->render('descont', [
            'model' => $model,
        ]);
    }

}
