<?php

namespace backend\controllers;

use common\models\Settings;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class SettingsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'secondary'
            ],
        ];
    }


    public function actionIndex()
    {
        $model = Settings::find()->all();

        if (Yii::$app->request->post('settings')) {
            $cmodel = Settings::find()->where(['name' => 'tel'])->one();
            $cmodel->text = $_POST['tel'] ?: '';
            $cmodel->save();
            $cmodel = Settings::find()->where(['name' => 'adr'])->one();
            $cmodel->text = $_POST['adr'] ?: '';
            $cmodel->save();
            $cmodel = Settings::find()->where(['name' => 'work'])->one();
            $cmodel->text = $_POST['work'] ?: '';
            $cmodel->save();
            $cmodel = Settings::find()->where(['name' => 'mail'])->one();
            $cmodel->text = $_POST['mail'] ?: '';
            $cmodel->save();
            $cmodel = Settings::find()->where(['name' => 'rules'])->one();
            $cmodel->text = $_POST['rules']['text'] ?: '';
            $cmodel->link = $_POST['rules']['link'] ?: '';
            $cmodel->save();

            return $this->redirect(['/settings/index']);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }


}
