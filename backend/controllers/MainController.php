<?php

namespace backend\controllers;

use common\models\Main;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class MainController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'slider', 'descont',
                            'products', 'about',
                            'news'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'secondary'
            ],
        ];
    }


    public function actionSlider()
    {
        $model = Main::find()->where(['type' => 1])->all();

        if (Yii::$app->request->post('main')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = Main::findOne($_POST['id']);
                $cmodel->title = $_POST['title'] ?: '';
                $cmodel->text = $_POST['text'] ?: '';
                $cmodel->img = $_POST['img'] ?: '';
                $cmodel->link = $_POST['link'] ?: '';
                $cmodel->name = $_POST['name'] ?: '';
                $cmodel->tag = $_POST['tag'] ?: '';
                $cmodel->tag_isActive = $_POST['tag_isActive'] ? 1 : 0;
                $cmodel->delay = $_POST['delay'] ?: 5;
                $cmodel->priority = $_POST['priority'] ?: 0;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new Main();
                $cmodel->type = $_POST['type'];
                $cmodel->title = $_POST['title'] ?: '';
                $cmodel->text = $_POST['text'] ?: '';
                $cmodel->img = $_POST['img'] ?: '';
                $cmodel->link = $_POST['link'] ?: '';
                $cmodel->name = $_POST['name'] ?: '';
                $cmodel->tag = $_POST['tag'] ?: '';
                $cmodel->tag_isActive = $_POST['tag_isActive'] ? 1 : 0;
                $cmodel->delay = $_POST['delay'] ?: 5;
                $cmodel->priority = $_POST['priority'] ?: 0;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = Main::findOne($_POST['id']);
                $cmodel->delete();
            }

            return $this->redirect(['/main/slider']);
        }

        return $this->render('slider', [
            'model' => $model,
        ]);
    }

    public function actionDescont()
    {
        $model = Main::find()->where(['type' => 2])->all();

        if (Yii::$app->request->post('main')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = Main::findOne($_POST['id']);
                $cmodel->title = $_POST['title'] ?: '';
                $cmodel->text = $_POST['text'] ?: '';
                $cmodel->img = $_POST['img'] ?: '';
                $cmodel->link = $_POST['link'] ?: '';
                $cmodel->delay = $_POST['delay'] ?: 5;
                $cmodel->priority = $_POST['priority'] ?: 0;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new Main();
                $cmodel->type = $_POST['type'];
                $cmodel->title = $_POST['title'] ?: '';
                $cmodel->text = $_POST['text'] ?: '';
                $cmodel->img = $_POST['img'] ?: '';
                $cmodel->link = $_POST['link'] ?: '';
                $cmodel->delay = $_POST['delay'] ?: 5;
                $cmodel->priority = $_POST['priority'] ?: 0;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = Main::findOne($_POST['id']);
                $cmodel->delete();
            }

            return $this->redirect(['/main/descont']);
        }

        return $this->render('descont', [
            'model' => $model,
        ]);
    }

    public function actionProducts()
    {
        $model = Main::find()->where(['type' => 3])->all();

        if (Yii::$app->request->post('main')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = Main::findOne($_POST['id']);
                $cmodel->external_id = $_POST['external_id'] ?: 0;
                $cmodel->img = $_POST['img'];
                $cmodel->priority = $_POST['priority'] ?: 0;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new Main();
                $cmodel->type = $_POST['type'];
                $cmodel->img = $_POST['img'];
                $cmodel->external_id = $_POST['external_id'] ?: 0;
                $cmodel->priority = $_POST['priority'] ?: 0;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = Main::findOne($_POST['id']);
                $cmodel->delete();
            }

            return $this->redirect(['/main/products']);
        }


        return $this->render('products', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        $text_main = Main::find()->where(['type' => 4])->andWhere(['name' => 'main'])->one();
        $text_sub = Main::find()->where(['type' => 4])->andWhere(['name' => 'sub'])->one();

        if (Yii::$app->request->post('main')) {
            $text_main->text = $_POST['text_main'] ?: '';
            $text_sub->text = $_POST['text_sub'] ?: '';
            $text_main->save();
            $text_sub->save();

            return $this->redirect(['/main/about']);
        }

        return $this->render('about', [
            'text_main' => $text_main,
            'text_sub' => $text_sub,
        ]);
    }

    public function actionNews()
    {
        $model = Main::find()->where(['type' => 5])->all();

        if (Yii::$app->request->post('main')) {
            if (Yii::$app->request->post('id')) {
                $cmodel = Main::findOne($_POST['id']);
                $cmodel->external_id = $_POST['external_id'] ?: 0;
                $cmodel->priority = $_POST['priority'] ?: 0;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('create')) {
                $cmodel = new Main();
                $cmodel->type = $_POST['type'];
                $cmodel->external_id = $_POST['external_id'] ?: 0;
                $cmodel->priority = $_POST['priority'] ?: 0;
                $cmodel->isActive = $_POST['isActive'] ? 1 : 0;
                $cmodel->save();
            }
            if (Yii::$app->request->post('delete') && Yii::$app->request->post('id')) {
                $cmodel = Main::findOne($_POST['id']);
                $cmodel->delete();
            }

            return $this->redirect(['/main/news']);
        }

        return $this->render('news', [
            'model' => $model,
        ]);
    }


}
