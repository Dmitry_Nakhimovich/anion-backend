(function ($) {
    'use strict';
    $(function () {
        var dataTable = $('#sorting-table, .sorting-table').DataTable({
            "lengthMenu": [[10, 15, 20, -1], [10, 15, 20, "Все"]],
            "order": [[0, "asc"]],
            "oLanguage": {
                "sProcessing": "Подождите...",
                "sSearch": "Поиск:",
                "sLengthMenu": "Показать _MENU_ записей",
                "sInfo": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "sInfoEmpty": "Записи с 0 до 0 из 0 записей",
                "sInfoFiltered": "(отфильтровано из _MAX_ записей)",
                "sInfoPostFix": "",
                "sLoadingRecords": "Загрузка записей...",
                "sZeroRecords": "Записи отсутствуют.",
                "sEmptyTable": "В таблице отсутствуют данные",
                "oPaginate": {
                    "sFirst": "Первая",
                    "sPrevious": "Предыдущая",
                    "sNext": "Следующая",
                    "sLast": "Последняя"
                },
                "oAria": {
                    "sSortAscending": ": активировать для сортировки столбца по возрастанию",
                    "sSortDescending": ": активировать для сортировки столбца по убыванию"
                }
            }
        });
        $('.table-responsive').on('keyup change', '#sorting-table_filter input', function () {
            //console.log(this.value);
            if ($('#sorting-table').hasClass('products-table')) {
                var searchTerm = this.value.toLowerCase();
                $.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
                    //search only in column 1 and 3
                    if (~data[0].toLowerCase().indexOf(searchTerm)) return true;
                    if (~data[2].toLowerCase().indexOf(searchTerm)) return true;
                    return false;
                })
                dataTable.draw();
                $.fn.dataTable.ext.search.pop();
            }
        });
    });

})(jQuery);