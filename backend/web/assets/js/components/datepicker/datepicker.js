(function ($) {
    'use strict';
    $(function () {
        $('#daterange').daterangepicker({autoApply: true});
        $('#datetime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {format: 'MM/DD/YYYY h:mm A'}
        });
        $('#datesingle, .datesingle').daterangepicker({
            singleDatePicker: true,
            "locale": {
                "format": "DD.MM.YYYY",
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Отмена",
                "fromLabel": "От",
                "toLabel": "До",
                "customRangeLabel": "Свой",
                "weekLabel": "Нед",
                "daysOfWeek": ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                "monthNames": ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                    'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                "firstDay": 1
            },
        });
    });
})(jQuery);
