<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        /*'css/site.css',*/
        'assets/vendors/css/base/bootstrap.min.css',
        'assets/vendors/css/base/elisyam-1.2.min.css',
        'assets/css/animate/animate.min.css',
        'assets/css/datatables/datatables.min.css',
        'assets/css/bootstrap-select/bootstrap-select.min.css',
        'assets/vendors/css/base/custom.css',
    ];
    public $js = [
        /*'assets/vendors/js/base/jquery.min.js',*/
        /*'assets/vendors/js/base/jquery.ui.min.js',*/
        'assets/vendors/js/base/core.min.js',
        /*all plugins*/
        'assets/vendors/js/bootstrap-select/bootstrap-select.min.js',
        'assets/vendors/js/bootstrap-select/bootstrap-select_ru.js',
        'assets/vendors/js/datatables/datatables.min.js',
        'assets/vendors/js/datatables/dataTables.buttons.min.js',
        'assets/vendors/js/datepicker/moment.min.js',
        'assets/vendors/js/datepicker/daterangepicker.js',
        /*init plugins*/
        'assets/vendors/js/nicescroll/nicescroll.min.js',
        'assets/vendors/js/app/app.js',
        'assets/js/components/tables/tables.js',
        'assets/js/components/datepicker/datepicker.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        /*'yii\bootstrap\BootstrapAsset',*/
    ];
}
