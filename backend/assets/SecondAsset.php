<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class SecondAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        /*'css/site.css',*/
        'assets/vendors/css/base/bootstrap.min.css',
        'assets/vendors/css/base/elisyam-1.2.min.css',
        'assets/css/animate/animate.min.css',
        'assets/vendors/css/base/custom.css',
    ];
    public $js = [
        /*'assets/vendors/js/base/jquery.min.js',*/
        /*'assets/vendors/js/base/jquery.ui.min.js',*/
        'assets/vendors/js/base/core.min.js',
        /*all plugins*/

        /*init plugins*/
        'assets/vendors/js/nicescroll/nicescroll.min.js',
        'assets/vendors/js/app/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        /*'yii\bootstrap\BootstrapAsset',*/
    ];
}
