<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="widget has-shadow">
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <div class="row">
      <div class="col-12">
        <h3 class="mb-4">Склады</h3>
      </div>
      <div class="col-12">
        <!--Добавить-->
        <a href="#" data-toggle="modal" data-target="#modal-create"
           class="btn btn-success ripple mr-1 mb-2">
          <i class="la la-plus edit"></i>Добавить
        </a>
        <div id="modal-create" class="modal fade">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <?= Html::beginForm(
                    ['stock/index'],
                    'post',
                    ['enctype' => 'multipart/form-data']
                ) ?>
                <?= Html::hiddenInput('stock', 'true'); ?>
                <?= Html::hiddenInput('create', 'true'); ?>
              <div class="modal-header">
                <h3 class="modal-title">Создание записи</h3>
                <button type="button" class="close" data-dismiss="modal">
                  <span aria-hidden="true">×</span>
                  <span class="sr-only">close</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label>Наименование</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="name"
                               value=''
                               class="form-control" required/>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Код</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="code"
                               value=''
                               class="form-control" required/>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Приоритет</label>
                      <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                        <input type="text" name="priority"
                               value=''
                               class="form-control" required/>
                      </div>
                    </div>
                    <div class="form-group mb-3">
                      <label>Признак публикации</label>
                      <div class="styled-checkbox">
                        <input type="checkbox" name="isActive"
                               id="check-create">
                        <label for="check-create">Активировать</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary ripple"
                        data-dismiss="modal">Закрыть
                </button>
                <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                </button>
              </div>
                <?= Html::endForm() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="widget-body">
    <div class="table-responsive">
      <table id="sorting-table" class="table mb-0">
        <thead>
        <tr>
          <th>Наименование</th>
          <th>Код</th>
          <th>Приоритет</th>
          <th>Признак публикации</th>
          <th>Действия</th>
        </tr>
        </thead>
        <tbody>

        <? foreach ($model as $item): ?>
          <tr>
            <td>
              <span class="text-primary">
                  <?= $item->name ?>
              </span>
            </td>
            <td>
              <span class="text-primary">
                  <?= $item->code ?: '' ?>
              </span>
            </td>
            <td><?= $item->priority ?></td>
            <td>
              <span style="width:100px;">
                  <? $status = $item->isActive ?>
                  <? if ($status): ?>
                    <span class="badge-text badge-text-small success">Активно</span>
                  <? else: ?>
                    <span class="badge-text badge-text-small danger">Отключено</span>
                  <? endif; ?>
              </span>
            </td>
            <td class="td-actions">
              <!--Редактировать-->
              <a href="#" data-toggle="modal" data-target="#modal-<?= $item->id ?>">
                <i class="la la-edit edit"></i>
              </a>
              <div id="modal-<?= $item->id ?>" class="modal fade">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                      <?= Html::beginForm(
                          ['stock/index'],
                          'post',
                          ['enctype' => 'multipart/form-data']
                      ) ?>
                      <?= Html::hiddenInput('stock', 'true'); ?>
                      <?= Html::hiddenInput('id', $item->id); ?>
                    <div class="modal-header">
                      <h3 class="modal-title">Редактирование записи: <?= $item->name ?></h3>
                      <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-12">
                          <div class="form-group">
                            <label>Наименование</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="name"
                                     value='<?= $item->name ?: ''; ?>'
                                     class="form-control" required/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Код</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="code"
                                     value='<?= $item->code ?: ''; ?>'
                                     class="form-control" required/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Приоритет</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="priority"
                                     value='<?= $item->priority ?: ''; ?>'
                                     class="form-control" required/>
                            </div>
                          </div>
                          <div class="form-group mb-3">
                            <label>Признак публикации</label>
                            <div class="styled-checkbox">
                              <input type="checkbox" name="isActive"
                                     id="check-<?= $item->id ?>"
                                  <?= $item->isActive ? 'checked' : ''; ?>>
                              <label for="check-<?= $item->id ?>">Активировать</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-secondary ripple"
                              data-dismiss="modal">Закрыть
                      </button>
                      <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                      </button>
                    </div>
                      <?= Html::endForm() ?>
                  </div>
                </div>
              </div>
              <!--Удалить-->
              <a href="#" data-toggle="modal" data-target="#modal-delete-<?= $item->id ?>">
                <i class="la la-close delete"></i>
              </a>
              <div id="modal-delete-<?= $item->id ?>" class="modal fade">
                <div class="modal-dialog modal-dialog-centered modal-sm">
                  <div class="modal-content">
                      <?= Html::beginForm(
                          ['stock/index'],
                          'post',
                          ['enctype' => 'multipart/form-data']
                      ) ?>
                      <?= Html::hiddenInput('stock', 'true'); ?>
                      <?= Html::hiddenInput('delete', 'true'); ?>
                      <?= Html::hiddenInput('id', $item->id); ?>
                    <div class="modal-header">
                      <h3 class="modal-title">Удаление записи: <?= $item->name ?></h3>
                      <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-12">
                          <p class="text-dark mb-3">Вы уверены что хотите удалить запись?</p>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-secondary ripple"
                              data-dismiss="modal">Отмена
                      </button>
                      <button type="submit" class="btn btn-outline-danger ripple">Удалить
                      </button>
                    </div>
                      <?= Html::endForm() ?>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        <? endforeach; ?>

        </tbody>
      </table>
    </div>
  </div>
</div>