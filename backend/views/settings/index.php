<?php

use common\models\Settings;
use yii\helpers\Html;
use yii\helpers\Url;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;

?>

<div class="widget has-shadow">
    <?= Html::beginForm(
        ['settings/index'],
        'post',
        ['enctype' => 'multipart/form-data']
    ) ?>
    <?= Html::hiddenInput('settings', 'true'); ?>
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <h3>Настройки</h3>
  </div>
  <div class="widget-body">
    <div class="row">
      <div class="col-12">
        <div class="form-group pb-3 pt-3 m-0">
          <label>Номер телефона</label>
          <div class="input-group">
            <span class="input-group-addon addon-primary">
              <i class="la la-pencil"></i>
            </span>
            <input type="text" name="tel"
                   value='<?= Settings::find()->where(['name' => 'tel'])->one()->text ?>'
                   class="form-control"/>
          </div>
        </div>
        <div class="form-group pb-3 pt-3 m-0">
          <label>Адрес</label>
          <div class="input-group">
            <span class="input-group-addon addon-primary">
              <i class="la la-pencil"></i>
            </span>
            <input type="text" name="adr"
                   value='<?= Settings::find()->where(['name' => 'adr'])->one()->text ?>'
                   class="form-control"/>
          </div>
        </div>
        <div class="form-group pb-3 pt-3 m-0">
          <label>Время работы</label>
          <div class="input-group">
            <span class="input-group-addon addon-primary">
              <i class="la la-pencil"></i>
            </span>
            <input type="text" name="work"
                   value='<?= Settings::find()->where(['name' => 'work'])->one()->text ?>'
                   class="form-control"/>
          </div>
        </div>
        <div class="form-group pb-3 pt-3 m-0">
          <label>Почта</label>
          <div class="input-group">
            <span class="input-group-addon addon-primary">
              <i class="la la-pencil"></i>
            </span>
            <input type="text" name="mail"
                   value='<?= Settings::find()->where(['name' => 'mail'])->one()->text ?>'
                   class="form-control"/>
          </div>
        </div>
        <div class="form-group pb-3 pt-3 m-0">
          <label>Текст документа</label>
          <div class="input-group">
            <span class="input-group-addon addon-primary">
              <i class="la la-pencil"></i>
            </span>
            <input type="text" name="rules[text]"
                   value='<?= Settings::find()->where(['name' => 'rules'])->one()->text ?>'
                   class="form-control"/>
          </div>
        </div>
        <div class="form-group pb-3 pt-3 m-0">
          <label>Ссылка на файл</label>
          <div class="input-group">
              <span class="input-group-addon addon-primary">
                  <i class="la la-file"></i>
              </span>
              <?= InputFile::widget([
                  'language' => 'ru',
                  //'filter' => 'image',
                  'name' => 'rules[link]',
                  'options' => [
                      'class' => 'form-control',
                      'required' => 'required',
                  ],
                  'buttonOptions' => [
                      'class' => 'btn btn-primary btn-square ripple'
                  ],
                  'buttonName' => 'Выбрать',
                  'value' => Settings::find()->where(['name' => 'rules'])->one()->link ?: '',
              ]); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="widget-footer bordered">
    <a href="<?= Url::toRoute(['/settings/index']) ?>"
       class="btn btn-outline-secondary ripple">Отменить
    </a>
    <button type="submit"
            class="btn btn-outline-primary ripple">Сохранить
    </button>
  </div>
    <?= Html::endForm() ?>
</div>