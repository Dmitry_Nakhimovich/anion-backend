<?php

use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use mihaildev\elfinder\InputFile;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="widget has-shadow">
    <?= Html::beginForm(
        ['materials/create'],
        'post',
        ['enctype' => 'multipart/form-data']
    ) ?>
    <?= Html::hiddenInput('detail', 'true'); ?>
    <?= Html::hiddenInput('create', 'true'); ?>
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <h3>Создание материала</h3>
  </div>
  <div class="widget-body">
    <div class="row">
      <div class="col-lg-4 col-12">
        <div class="form-group pb-3 pt-3 m-0">
          <label>Заголовок</label>
          <div class="input-group">
            <span class="input-group-addon addon-primary">
                <i class="la la-pencil"></i>
            </span>
            <textarea name="title" rows="4"
                      class="form-control" required></textarea>
          </div>
        </div>
        <div class="form-group pb-3 pt-3 m-0">
          <label>Дата публикации</label>
          <div class="input-group">
            <span class="input-group-addon addon-primary">
                <i class="la la-calendar"></i>
            </span>
            <input type="text" name="date" id="datesingle"
                   value=''
                   class="form-control" required/>
          </div>
        </div>
        <div class="form-group pb-3 pt-3 m-0">
          <label>Признак публикации</label>
          <div class="styled-checkbox">
            <input type="checkbox" name="isActive"
                   id="check-create">
            <label for="check-create">Активировать</label>
          </div>
        </div>
        <div class="form-group pb-3 pt-3 m-0">
          <label>Ссылка на изображение</label>
          <div class="input-group">
            <span class="input-group-addon addon-primary">
              <i class="la la-file"></i>
            </span>
              <?= InputFile::widget([
                  'language' => 'ru',
                  'filter' => ['image/jpeg', 'image/jpeg', 'image/png'],
                  'name' => 'img',
                  'options' => [
                      'id' => 'img-create-src',
                      'class' => 'form-control',
                      //'required' => 'required',
                  ],
                  'buttonOptions' => [
                      'class' => 'btn btn-primary btn-square ripple'
                  ],
                  'buttonName' => 'Выбрать',
                  'value' => '',
              ]); ?>
          </div>
        </div>
        <script>
            $(function () {
                $('#img-create-src').on('change', function () {
                    $('#img-create-prev').prop('src', $(this).val());
                });
            });
        </script>
        <div class="form-group pb-3 pt-3 m-0">
          <div class="img-pic">
            <img id="img-create-prev" src="" alt="">
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-12">
        <div class="form-group pb-3 pt-3 m-0">
          <label>Текст</label>
          <div class="cke-group">
              <?= CKEditor::widget([
                  'name' => 'text',
                  //'model' => $model,
                  'value' => '',
                  'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                      'preset' => 'full',
                      'inline' => false,
                  ]),
              ]); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="widget-footer bordered">
    <a href="<?= Url::toRoute(['/materials']) ?>"
       class="btn btn-outline-secondary ripple">Отменить
    </a>
    <button type="submit"
            class="btn btn-outline-primary ripple">Сохранить
    </button>
  </div>
    <?= Html::endForm() ?>
</div>