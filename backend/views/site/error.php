<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<div class="bg-error-01 h-100 w-100">
    <div class="container-fluid h-100 error-01">
        <div class="row justify-content-center align-items-center h-100">
            <div class="col-12">

                <div class="error-container mx-auto text-center">

                    <h1><?= Html::encode($this->title) ?></h1>

                    <div class="alert alert-danger" style="display: inline-block;">
                        <?= nl2br(Html::encode($message)) ?>
                    </div>

                    <p> Если ошибка повторяется, пожалуйста сообщите администратору сайта. </p>

                    <a href="<?= Yii::$app->request->referrer ?: Yii::$app->homeUrl ?>" class="btn btn-shadow">Вернуться
                        назад</a>

                </div>

            </div>
        </div>
    </div>
</div>
