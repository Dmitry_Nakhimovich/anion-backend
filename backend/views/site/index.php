<?php

/* @var $this yii\web\View */

$this->title = 'Главная';
?>

<div class="widget has-shadow">
    <div class="widget-body">

        <div class="site-index">
            <div class="jumbotron">
                <h1>Добро пожаловать!</h1>

                <p class="lead">Вы успешно авторизировались в панели управления сайтом.</p>
            </div>

            <div class="body-content">

                <div class="row">
                    <div class="col-12">
                        <h2>Для редактирования контента, выберите один из пунктов бокового меню.</h2>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
