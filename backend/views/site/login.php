<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid no-padding h-100">
    <div class="row flex-row h-100 bg-white">

        <div class="col-xl-3 col-lg-5 col-md-5 col-sm-12 col-12 no-padding">
            <div class="elisyam-bg background-03">
                <div class="elisyam-overlay overlay-08"></div>
                <div class="authentication-col-content-2 mx-auto text-center">
                    <div class="logo-centered">
                        <a href="<?= Yii::$app->urlManagerFrontend->createUrl('site/index') ?>">
                            <img src="/admin/assets/img/custom/logo.png" alt="logo">
                        </a>
                    </div>
                    <h1>Панель управления</h1>
                </div>
            </div>
        </div>

        <div class="col-xl-9 col-lg-7 col-md-7 col-sm-12 col-12 my-auto no-padding">
            <div class="authentication-form-2 mx-auto">

                <h3>Вход в панель управления сайтом</h3>
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username', [
                    'template' => '{input}
                                   <span class="highlight"></span>
                                   <span class="bar"></span>
                                   {label}
                                   {hint}
                                   {error}',
                    'inputOptions' => ['class' => ' ', 'required' => ''],
                    'labelOptions' => ['class' => ' '],
                    'options' => ['class' => 'group material-input']
                ])->textInput([])->label('Имя пользователя') ?>

                <?= $form->field($model, 'password', [
                    'template' => '{input}
                                   <span class="highlight"></span>
                                   <span class="bar"></span>
                                   {label}
                                   {hint}
                                   {error}',
                    'inputOptions' => ['class' => ' ', 'required' => ''],
                    'labelOptions' => ['class' => ' '],
                    'options' => ['class' => 'group material-input']
                ])->passwordInput()->label('Пароль') ?>

                <div class="sign-btn text-center">
                    <?= Html::submitButton('Войти', [
                        'class' => 'btn btn-primary btn-lg ripple',
                        'name' => 'login-button',
                    ]) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    </div>
</div>
