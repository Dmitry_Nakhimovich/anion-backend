<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="widget has-shadow">
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <div class="row">
      <div class="col-12">
        <h3 class="mb-4">Дополнительное меню содержание</h3>
      </div>
      <div class="col-12">
        <!--Добавить-->
        <a href="#" data-toggle="modal" data-target="#modal-create"
           class="btn btn-success ripple mr-1 mb-2">
          <i class="la la-plus edit"></i>Добавить
        </a>
        <div id="modal-create" class="modal fade">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <?= Html::beginForm(
                    ['sidebar/detail-menu', 'id' => $model_id],
                    'post',
                    ['enctype' => 'multipart/form-data']
                ) ?>
                <?= Html::hiddenInput('detail-menu', 'true'); ?>
                <?= Html::hiddenInput('create', 'true'); ?>
              <div class="modal-header">
                <h3 class="modal-title">Создание записи</h3>
                <button type="button" class="close" data-dismiss="modal">
                  <span aria-hidden="true">×</span>
                  <span class="sr-only">close</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label>Наименование</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="name"
                               value=''
                               class="form-control" required/>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Ссылка</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="link"
                               value=''
                               class="form-control"/>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-lg-6">
                        <label>Маркер</label>
                        <div class="input-group">
                          <span class="input-group-addon addon-primary">
                              <i class="la la-pencil"></i>
                          </span>
                          <input type="text" name="tag"
                                 value=''
                                 class="form-control"/>
                        </div>
                      </div>
                      <div class="form-group mb-3 col-lg-6">
                        <label>Показать маркер</label>
                        <div class="styled-checkbox">
                          <input type="checkbox" name="tag_isActive"
                                 id="tag_check">
                          <label for="tag_check">Активировать</label>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-lg-6">
                        <label>Приоритет</label>
                        <div class="input-group">
                          <span class="input-group-addon addon-primary">
                              <i class="la la-pencil"></i>
                          </span>
                          <input type="text" name="priority"
                                 value='0'
                                 class="form-control" required/>
                        </div>
                      </div>
                      <div class="form-group col-lg-6">
                        <label>Признак публикации</label>
                        <div class="styled-checkbox">
                          <input type="checkbox" name="isActive"
                                 id="check">
                          <label for="check">Активировать</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary ripple"
                        data-dismiss="modal">Закрыть
                </button>
                <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                </button>
              </div>
                <?= Html::endForm() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="widget-body">
    <div class="table-responsive">
      <table id="sorting-table" class="table mb-0">
        <thead>
        <tr>
          <th>Наименование</th>
          <th>Ссылка</th>
          <th>Маркер</th>
          <th>Показать маркер</th>
          <th>Приоритет</th>
          <th>Признак публикации</th>
          <th>Действия</th>
        </tr>
        </thead>
        <tbody>

        <? foreach ($model as $item): ?>
          <tr>
            <td>
              <span class="text-primary">
                  <?= $item->name ?>
              </span>
            </td>
            <td> <?= $item->link ?: ''; ?> </td>
            <td> <?= $item->tag ?: '' ?></td>
            <td>
              <span style="width:100px;">
                  <? $tag_status = $item->tag_isActive ?>
                  <? if ($tag_status): ?>
                    <span class="badge-text badge-text-small success">Активно</span>
                  <? else: ?>
                    <span class="badge-text badge-text-small danger">Отключено</span>
                  <? endif; ?>
              </span>
            </td>
            <td> <?= $item->priority ?: '' ?></td>
            <td>
              <span style="width:100px;">
                  <? $status = $item->isActive ?>
                  <? if ($status): ?>
                    <span class="badge-text badge-text-small success">Активно</span>
                  <? else: ?>
                    <span class="badge-text badge-text-small danger">Отключено</span>
                  <? endif; ?>
              </span>
            </td>
            <td class="td-actions">
              <!--Редактирование-->
              <a href="#" data-toggle="modal" data-target="#modal-<?= $item->id ?>">
                <i class="la la-edit edit"></i>
              </a>
              <div id="modal-<?= $item->id ?>" class="modal fade">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                      <?= Html::beginForm(
                          ['sidebar/detail-menu', 'id' => $model_id],
                          'post',
                          ['enctype' => 'multipart/form-data']
                      ) ?>
                      <?= Html::hiddenInput('detail-menu', 'true'); ?>
                      <?= Html::hiddenInput('id', $item->id); ?>
                    <div class="modal-header">
                      <h3 class="modal-title">Редактирование записи: <?= $item->name ?></h3>
                      <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-12">
                          <div class="form-group">
                            <label>Наименование</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="name"
                                     value='<?= $item->name ?: ''; ?>'
                                     class="form-control" required/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Ссылка</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="link"
                                     value='<?= $item->link ?: ''; ?>'
                                     class="form-control"/>
                            </div>
                          </div>
                          <div class="row">
                            <div class="form-group col-lg-6">
                              <label>Маркер</label>
                              <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                                <input type="text" name="tag"
                                       value='<?= $item->tag ?: ''; ?>'
                                       class="form-control"/>
                              </div>
                            </div>
                            <div class="form-group mb-3 col-lg-6">
                              <label>Показать маркер</label>
                              <div class="styled-checkbox">
                                <input type="checkbox" name="tag_isActive"
                                       id="tag_check-<?= $item->id ?>"
                                    <?= $item->tag_isActive ? 'checked' : ''; ?>>
                                <label for="tag_check-<?= $item->id ?>">Активировать</label>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="form-group col-lg-6">
                              <label>Приоритет</label>
                              <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                                <input type="text" name="priority"
                                       value='<?= $item->priority ?: ''; ?>'
                                       class="form-control" required/>
                              </div>
                            </div>
                            <div class="form-group col-lg-6">
                              <label>Признак публикации</label>
                              <div class="styled-checkbox">
                                <input type="checkbox" name="isActive"
                                       id="check-<?= $item->id ?>"
                                    <?= $item->isActive ? 'checked' : ''; ?>>
                                <label for="check-<?= $item->id ?>">Активировать</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-secondary ripple"
                              data-dismiss="modal">Закрыть
                      </button>
                      <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                      </button>
                    </div>
                      <?= Html::endForm() ?>
                  </div>
                </div>
              </div>
              <!--Удалить-->
              <a href="#" data-toggle="modal" data-target="#modal-delete-<?= $item->id ?>">
                <i class="la la-close delete"></i>
              </a>
              <div id="modal-delete-<?= $item->id ?>" class="modal fade">
                <div class="modal-dialog modal-dialog-centered modal-sm">
                  <div class="modal-content">
                      <?= Html::beginForm(
                          ['sidebar/detail-menu', 'id' => $model_id],
                          'post',
                          ['enctype' => 'multipart/form-data']
                      ) ?>
                      <?= Html::hiddenInput('detail-menu', 'true'); ?>
                      <?= Html::hiddenInput('delete', 'true'); ?>
                      <?= Html::hiddenInput('id', $item->id); ?>
                    <div class="modal-header">
                      <h3 class="modal-title">Удаление записи: <?= $item->name ?></h3>
                      <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-12">
                          <p class="text-dark mb-3">Вы уверены что хотите удалить запись?</p>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-secondary ripple"
                              data-dismiss="modal">Отмена
                      </button>
                      <button type="submit" class="btn btn-outline-danger ripple">Удалить
                      </button>
                    </div>
                      <?= Html::endForm() ?>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        <? endforeach; ?>

        </tbody>
      </table>
    </div>
  </div>
</div>