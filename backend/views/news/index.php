<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Descont;
use common\models\DopMenu;
use common\models\Files;
use common\models\Info;
use common\models\Links;

?>

<div class="widget has-shadow">
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <h3>Новости</h3>
  </div>
  <div class="widget-body">
    <ul class="nav nav-tabs" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="i-base-tab-1"
           data-toggle="tab" href="#i-tab-1" role="tab"
           aria-controls="i-tab-1" aria-selected="true">
          <i class="la la-align-left mr-2"></i>
          Основной контент
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="i-base-tab-2"
           data-toggle="tab" href="#i-tab-2" role="tab"
           aria-controls="i-tab-2" aria-selected="false">
          <i class="la la-list-ol mr-2"></i>
          Доп. меню
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="i-base-tab-3"
           data-toggle="tab" href="#i-tab-3" role="tab"
           aria-controls="i-tab-3" aria-selected="false">
          <i class="la la-info mr-2"></i>
          Ссылки
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="i-base-tab-4"
           data-toggle="tab" href="#i-tab-4" role="tab"
           aria-controls="i-tab-4" aria-selected="false">
          <i class="la la-info mr-2"></i>
          Файлы
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="i-base-tab-5"
           data-toggle="tab" href="#i-tab-5" role="tab"
           aria-controls="i-tab-5" aria-selected="false">
          <i class="la la-info mr-2"></i>
          Информация
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="i-base-tab-6"
           data-toggle="tab" href="#i-tab-6" role="tab"
           aria-controls="i-tab-6" aria-selected="false">
          <i class="la la-info mr-2"></i>
          Акции
        </a>
      </li>
    </ul>
    <div class="tab-content pt-3">
      <div class="tab-pane fade show active" id="i-tab-1" role="tabpanel" aria-labelledby="i-base-tab-1">
        <div class="row">
          <div class="col-12">
              <?= Html::beginForm(
                  ['news/index'],
                  'post',
                  ['enctype' => 'multipart/form-data']
              ) ?>
              <?= Html::hiddenInput('news', 'true'); ?>
              <?= Html::hiddenInput('section', 'true'); ?>
            <h4 class="mt-4 mb-3">Общий сайдбар:</h4>
            <div class="form-group row align-items-lg-end">
              <div class="col-12 col-lg-auto">
                <label>Сделать общим текущий сайдбар</label>
                <div class="styled-checkbox">
                  <input type="checkbox" name="sections_sidebar_all"
                         id="sections-check-sidebar"
                      <?= ($section->id_item == -1) ? 'checked' : ''; ?>>
                  <label for="sections-check-sidebar">Активировать</label>
                </div>
              </div>
              <div class="col-12 col-lg-auto">
                <button type="submit"
                        class="btn btn-outline-primary ripple">Изменить
                </button>
              </div>
            </div>
              <?= Html::endForm() ?>
          </div>
          <div class="col-12">
            <h4 class="mt-4 mb-3">Все новости:</h4>
            <!--Добавить-->
            <a href="<?= Url::toRoute(['/news/create']) ?>"
               class="btn btn-success ripple mr-1 mb-2">
              <i class="la la-plus edit"></i>Добавить
            </a>
          </div>
          <div class="col-12">
            <div class="table-responsive">
              <table id="sorting-table" class="table mb-0">
                <thead>
                <tr>
                  <th>Наименование</th>
                  <th>Ссылка</th>
                  <th>Признак публикации</th>
                  <th>Дата публикации</th>
                  <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                <? foreach ($model as $item): ?>
                  <tr>
                    <td>
                      <span class="text-primary">
                        <? foreach ($news as $n)
                            if ($n->id == $item->id_item) $cnews = $n;
                        ?>
                        <?= $cnews->title ?>
                      </span>
                    </td>
                    <td><?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/news/' . $item->link]) ?></td>
                    <td>
                      <span style="width:100px;">
                          <? $status = $cnews->isActive ?>
                          <? if ($status): ?>
                            <span class="badge-text badge-text-small success">Активно</span>
                          <? else: ?>
                            <span class="badge-text badge-text-small danger">Отключено</span>
                          <? endif; ?>
                      </span>
                    </td>
                    <td><?= Yii::$app->formatter->asDate($cnews->date) ?></td>
                    <td class="td-actions">
                      <!--Редактировать-->
                      <a href="#" data-toggle="modal" data-target="#modal-<?= $item->id ?>">
                        <i class="la la-edit edit"></i>
                      </a>
                      <div id="modal-<?= $item->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['news/index'],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('news', 'true'); ?>
                              <?= Html::hiddenInput('news_id', $item->id); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Редактирование записи: <?= $cnews->title ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <div class="form-group">
                                    <label>Ссылка</label>
                                    <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                                      <input type="text" name="link"
                                             value='<?= $item->link ? $item->link : ''; ?>'
                                             class="form-control"/>
                                    </div>
                                  </div>
                                  <div class="form-group mb-3">
                                    <label>Признак публикации</label>
                                    <div class="styled-checkbox">
                                      <input type="checkbox" name="isActive"
                                             id="check-<?= $item->id ?>"
                                          <?= $cnews->isActive ? 'checked' : ''; ?>>
                                      <label for="check-<?= $item->id ?>">Активировать</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Закрыть
                              </button>
                              <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                      <!--Детальная-->
                      <a href="<?= Url::toRoute(['/news/detail', 'id' => $item->id_item]) ?>">
                        <i class="la la-outdent edit"></i>
                      </a>
                      <!--Удалить-->
                      <a href="#" data-toggle="modal" data-target="#modal-delete-<?= $item->id ?>">
                        <i class="la la-close delete"></i>
                      </a>
                      <div id="modal-delete-<?= $item->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered modal-sm">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['news/index'],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('news', 'true'); ?>
                              <?= Html::hiddenInput('news_id', $item->id); ?>
                              <?= Html::hiddenInput('delete', 'true'); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Удаление записи: <?= $cnews->title ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <p class="text-dark mb-3">Вы уверены что хотите удалить запись?</p>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Отмена
                              </button>
                              <button type="submit" class="btn btn-outline-danger ripple">Удалить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                <? endforeach; ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="i-tab-2" role="tabpanel" aria-labelledby="i-base-tab-2">
        <div class="row">
          <div class="col-12">
              <?= Html::beginForm(
                  ['news/index'],
                  'post',
                  ['enctype' => 'multipart/form-data']
              ) ?>
              <?= Html::hiddenInput('news', 'true'); ?>
              <?= Html::hiddenInput('dopMenu', 'true'); ?>
              <?= Html::hiddenInput('section', 'true'); ?>
            <h4 class="mt-4 mb-3">Текущее дополнительное меню:</h4>
            <div class="form-group row align-items-lg-end">
              <div class="col-12 col-lg-auto">
                <label>ID дополнительного меню</label>
                <div class="input-group">
                  <span class="input-group-addon addon-primary">
                      <i class="la la-pencil"></i>
                  </span>
                  <select class="selectpicker show-menu-arrow" name="sections_dopMenu_id"
                          required data-style="btn btn-square ripple">
                      <? foreach ($dopMenu as $m): ?>
                          <? if ($m->id == $section->id_menu): ?>
                          <option value="<?= $m->id ?>" selected><?= $m->id ?></option>
                          <? else: ?>
                          <option value="<?= $m->id ?>"><?= $m->id ?></option>
                          <? endif; ?>
                      <? endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-12 col-lg-auto">
                <label>Признак публикации</label>
                <div class="styled-checkbox">
                  <input type="checkbox" name="sections_dopMenu_isActive"
                         id="sections-check-dopMenu-<?= $section->id ?>"
                      <?= $section->menu_isActive ? 'checked' : ''; ?>>
                  <label for="sections-check-dopMenu-<?= $section->id ?>">Активировать</label>
                </div>
              </div>
              <div class="col-12 col-lg-auto">
                <button type="submit"
                        class="btn btn-outline-primary ripple">Изменить
                </button>
              </div>
            </div>
              <?= Html::endForm() ?>
          </div>
          <div class="col-12">
            <h4 class="mt-4 mb-3">Дополнительные меню:</h4>
            <!--Добавить-->
            <a href="#" data-toggle="modal" data-target="#modal-dopMenu-create"
               class="btn btn-success ripple mr-1 mb-2">
              <i class="la la-plus edit"></i>Добавить
            </a>
            <div id="modal-dopMenu-create" class="modal fade">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <?= Html::beginForm(
                        ['news/index'],
                        'post',
                        ['enctype' => 'multipart/form-data']
                    ) ?>
                    <?= Html::hiddenInput('news', 'true'); ?>
                    <?= Html::hiddenInput('dopMenu', 'true'); ?>
                    <?= Html::hiddenInput('create', 'true'); ?>
                  <div class="modal-header">
                    <h3 class="modal-title">Создание записи</h3>
                    <button type="button" class="close" data-dismiss="modal">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">close</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-12">
                        <div class="form-group">
                          <label>Наименование</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                            <select class="selectpicker show-menu-arrow" name="dopMenu_idContent"
                                    required data-live-search="true" data-style="btn btn-square ripple">
                                <? foreach (DopMenu::find()->all() as $m): ?>
                                  <option value="<?= $m->id ?>"><?= $m->name ?></option>
                                <? endforeach; ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Приоритет</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                            <input type="text" name="dopMenu_priority"
                                   value=''
                                   class="form-control" required/>
                          </div>
                        </div>
                        <div class="form-group mb-3">
                          <label>Признак публикации</label>
                          <div class="styled-checkbox">
                            <input type="checkbox" name="dopMenu_isActive"
                                   id="check-dopMenu">
                            <label for="check-dopMenu">Активировать</label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Дата публикации</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                              <i class="la la-calendar"></i>
                            </span>
                            <input type="text" name="dopMenu_date"
                                   value=''
                                   class="form-control datesingle" required/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary ripple"
                            data-dismiss="modal">Закрыть
                    </button>
                    <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                    </button>
                  </div>
                    <?= Html::endForm() ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="table-responsive">
              <table class="sorting-table table mb-0">
                <thead>
                <tr>
                  <th style="width: 25px;">ID</th>
                  <th>Наименование</th>
                  <th>Приоритет</th>
                  <th>Признак публикации</th>
                  <th>Дата публикации</th>
                  <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                <? foreach ($dopMenu as $cDopMenu): ?>
                  <tr>
                    <td><?= $cDopMenu->id ?></td>
                    <td>
                      <span class="text-primary">
                        <? $cDopMenuContent = DopMenu::findOne($cDopMenu->id_menu) ?>
                        <?= $cDopMenuContent->name ?>
                      </span>
                    </td>
                    <td><?= $cDopMenu->priority ?></td>
                    <td>
                      <span style="width:100px;">
                          <? $status = $cDopMenu->isActive ?>
                          <? if ($status): ?>
                            <span class="badge-text badge-text-small success">Активно</span>
                          <? else: ?>
                            <span class="badge-text badge-text-small danger">Отключено</span>
                          <? endif; ?>
                      </span>
                    </td>
                    <td><?= Yii::$app->formatter->asDate($cDopMenu->date) ?></td>
                    <td class="td-actions">
                      <!--Редактировать-->
                      <a href="#" data-toggle="modal" data-target="#modal-dopMenu-<?= $cDopMenu->id ?>">
                        <i class="la la-edit edit"></i>
                      </a>
                      <div id="modal-dopMenu-<?= $cDopMenu->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['news/index'],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('news', 'true'); ?>
                              <?= Html::hiddenInput('dopMenu', 'true'); ?>
                              <?= Html::hiddenInput('dopMenu_id', $cDopMenu->id); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Редактирование записи: <?= $cDopMenuContent->name ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <div class="form-group">
                                    <label>Наименование</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                          <i class="la la-pencil"></i>
                                      </span>
                                      <select class="selectpicker show-menu-arrow" name="dopMenu_idContent"
                                              required data-live-search="true" data-style="btn btn-square ripple">
                                          <? foreach (DopMenu::find()->all() as $m): ?>
                                              <? if ($m->id == $cDopMenuContent->id): ?>
                                              <option value="<?= $m->id ?>" selected><?= $m->name ?></option>
                                              <? else: ?>
                                              <option value="<?= $m->id ?>"><?= $m->name ?></option>
                                              <? endif; ?>
                                          <? endforeach; ?>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label>Приоритет</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                          <i class="la la-pencil"></i>
                                      </span>
                                      <input type="text" name="dopMenu_priority"
                                             value='<?= $cDopMenu->priority ?: ''; ?>'
                                             class="form-control" required/>
                                    </div>
                                  </div>
                                  <div class="form-group mb-3">
                                    <label>Признак публикации</label>
                                    <div class="styled-checkbox">
                                      <input type="checkbox" name="dopMenu_isActive"
                                             id="check-dopMenu-<?= $cDopMenu->id ?>"
                                          <?= $cDopMenu->isActive ? 'checked' : ''; ?>>
                                      <label for="check-dopMenu-<?= $cDopMenu->id ?>">Активировать</label>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label>Дата публикации</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                        <i class="la la-calendar"></i>
                                      </span>
                                      <input type="text" name="dopMenu_date"
                                             value='<?= Yii::$app->formatter->asDate($cDopMenu->date) ?: ''; ?>'
                                             class="form-control datesingle" required/>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Закрыть
                              </button>
                              <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                      <!--Удалить-->
                      <a href="#" data-toggle="modal" data-target="#modal-dopMenu-delete-<?= $cDopMenu->id ?>">
                        <i class="la la-close delete"></i>
                      </a>
                      <div id="modal-dopMenu-delete-<?= $cDopMenu->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered modal-sm">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['news/index'],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('news', 'true'); ?>
                              <?= Html::hiddenInput('dopMenu', 'true'); ?>
                              <?= Html::hiddenInput('dopMenu_id', $cDopMenu->id); ?>
                              <?= Html::hiddenInput('delete', 'true'); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Удаление записи: <?= $cDopMenuContent->name ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <p class="text-dark mb-3">Вы уверены что хотите удалить запись?</p>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Отмена
                              </button>
                              <button type="submit" class="btn btn-outline-danger ripple">Удалить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                <? endforeach; ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="i-tab-3" role="tabpanel" aria-labelledby="i-base-tab-3">
        <div class="row">
          <div class="col-12">
              <?= Html::beginForm(
                  ['news/index'],
                  'post',
                  ['enctype' => 'multipart/form-data']
              ) ?>
              <?= Html::hiddenInput('news', 'true'); ?>
              <?= Html::hiddenInput('links', 'true'); ?>
              <?= Html::hiddenInput('section', 'true'); ?>
            <h4 class="mt-4 mb-3">Текущие полезные ссылки:</h4>
            <div class="form-group row align-items-lg-end">
              <div class="col-12 col-lg-auto">
                <label>Приоритет бокового меню</label>
                <div class="input-group">
                  <span class="input-group-addon addon-primary">
                      <i class="la la-pencil"></i>
                  </span>
                  <input type="text" name="sections_links_priority"
                         value='<?= $section->priority_links ?: ''; ?>'
                         class="form-control" required/>
                </div>
              </div>
              <div class="col-12 col-lg-auto">
                <label>Признак публикации</label>
                <div class="styled-checkbox">
                  <input type="checkbox" name="sections_links_isActive"
                         id="sections-check-links-<?= $section->id ?>"
                      <?= $section->links_isActive ? 'checked' : ''; ?>>
                  <label for="sections-check-links-<?= $section->id ?>">Активировать</label>
                </div>
              </div>
              <div class="col-12 col-lg-auto">
                <button type="submit"
                        class="btn btn-outline-primary ripple">Изменить
                </button>
              </div>
            </div>
              <?= Html::endForm() ?>
          </div>
          <div class="col-12">
            <h4 class="mt-4 mb-3">Полезные ссылки:</h4>
            <!--Добавить-->
            <a href="#" data-toggle="modal" data-target="#modal-links-create"
               class="btn btn-success ripple mr-1 mb-2">
              <i class="la la-plus edit"></i>Добавить
            </a>
            <div id="modal-links-create" class="modal fade">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <?= Html::beginForm(
                        ['news/index'],
                        'post',
                        ['enctype' => 'multipart/form-data']
                    ) ?>
                    <?= Html::hiddenInput('news', 'true'); ?>
                    <?= Html::hiddenInput('links', 'true'); ?>
                    <?= Html::hiddenInput('create', 'true'); ?>
                  <div class="modal-header">
                    <h3 class="modal-title">Создание записи</h3>
                    <button type="button" class="close" data-dismiss="modal">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">close</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-12">
                        <div class="form-group">
                          <label>Наименование</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                            <select class="selectpicker show-menu-arrow" name="links_idContent"
                                    required data-live-search="true" data-style="btn btn-square ripple">
                                <? foreach (Links::find()->all() as $m): ?>
                                  <option value="<?= $m->id ?>"><?= $m->name ?></option>
                                <? endforeach; ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Приоритет</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                            <input type="text" name="links_priority"
                                   value=''
                                   class="form-control" required/>
                          </div>
                        </div>
                        <div class="form-group mb-3">
                          <label>Признак публикации</label>
                          <div class="styled-checkbox">
                            <input type="checkbox" name="links_isActive"
                                   id="check-links">
                            <label for="check-links">Активировать</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary ripple"
                            data-dismiss="modal">Закрыть
                    </button>
                    <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                    </button>
                  </div>
                    <?= Html::endForm() ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="table-responsive">
              <table class="sorting-table table mb-0">
                <thead>
                <tr>
                  <th>Наименование</th>
                  <th>Приоритет</th>
                  <th>Признак публикации</th>
                  <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                <? foreach ($links as $cLink): ?>
                  <tr>
                    <td>
                      <span class="text-primary">
                        <? $cLinkContent = Links::findOne($cLink->id_link) ?>
                        <?= $cLinkContent->name ?>
                      </span>
                    </td>
                    <td><?= $cLink->priority ?></td>
                    <td>
                      <span style="width:100px;">
                          <? $status = $cLink->isActive ?>
                          <? if ($status): ?>
                            <span class="badge-text badge-text-small success">Активно</span>
                          <? else: ?>
                            <span class="badge-text badge-text-small danger">Отключено</span>
                          <? endif; ?>
                      </span>
                    </td>
                    <td class="td-actions">
                      <!--Редактировать-->
                      <a href="#" data-toggle="modal" data-target="#modal-links-<?= $cLink->id ?>">
                        <i class="la la-edit edit"></i>
                      </a>
                      <div id="modal-links-<?= $cLink->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['news/index'],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('news', 'true'); ?>
                              <?= Html::hiddenInput('links', 'true'); ?>
                              <?= Html::hiddenInput('links_id', $cLink->id); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Редактирование записи: <?= $cLinkContent->name ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <div class="form-group">
                                    <label>Наименование</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                          <i class="la la-pencil"></i>
                                      </span>
                                      <select class="selectpicker show-menu-arrow" name="links_idContent"
                                              required data-live-search="true" data-style="btn btn-square ripple">
                                          <? foreach (Links::find()->all() as $m): ?>
                                              <? if ($m->id == $cLinkContent->id): ?>
                                              <option value="<?= $m->id ?>" selected><?= $m->name ?></option>
                                              <? else: ?>
                                              <option value="<?= $m->id ?>"><?= $m->name ?></option>
                                              <? endif; ?>
                                          <? endforeach; ?>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label>Приоритет</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                          <i class="la la-pencil"></i>
                                      </span>
                                      <input type="text" name="links_priority"
                                             value='<?= $cLink->priority ?: ''; ?>'
                                             class="form-control" required/>
                                    </div>
                                  </div>
                                  <div class="form-group mb-3">
                                    <label>Признак публикации</label>
                                    <div class="styled-checkbox">
                                      <input type="checkbox" name="links_isActive"
                                             id="check-links-<?= $cLink->id ?>"
                                          <?= $cLink->isActive ? 'checked' : ''; ?>>
                                      <label for="check-links-<?= $cLink->id ?>">Активировать</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Закрыть
                              </button>
                              <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                      <!--Удалить-->
                      <a href="#" data-toggle="modal" data-target="#modal-links-delete-<?= $cLink->id ?>">
                        <i class="la la-close delete"></i>
                      </a>
                      <div id="modal-links-delete-<?= $cLink->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered modal-sm">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['news/index'],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('news', 'true'); ?>
                              <?= Html::hiddenInput('links', 'true'); ?>
                              <?= Html::hiddenInput('links_id', $cLink->id); ?>
                              <?= Html::hiddenInput('delete', 'true'); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Удаление записи: <?= $cLinkContent->name ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <p class="text-dark mb-3">Вы уверены что хотите удалить запись?</p>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Отмена
                              </button>
                              <button type="submit" class="btn btn-outline-danger ripple">Удалить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                <? endforeach; ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="i-tab-4" role="tabpanel" aria-labelledby="i-base-tab-4">
        <div class="row">
          <div class="col-12">
              <?= Html::beginForm(
                  ['news/index'],
                  'post',
                  ['enctype' => 'multipart/form-data']
              ) ?>
              <?= Html::hiddenInput('news', 'true'); ?>
              <?= Html::hiddenInput('files', 'true'); ?>
              <?= Html::hiddenInput('section', 'true'); ?>
            <h4 class="mt-4 mb-3">Текущие файлы:</h4>
            <div class="form-group row align-items-lg-end">
              <div class="col-12 col-lg-auto">
                <label>Приоритет бокового меню</label>
                <div class="input-group">
                  <span class="input-group-addon addon-primary">
                      <i class="la la-pencil"></i>
                  </span>
                  <input type="text" name="sections_files_priority"
                         value='<?= $section->priority_files ?: ''; ?>'
                         class="form-control" required/>
                </div>
              </div>
              <div class="col-12 col-lg-auto">
                <label>Признак публикации</label>
                <div class="styled-checkbox">
                  <input type="checkbox" name="sections_files_isActive"
                         id="sections-check-files-<?= $section->id ?>"
                      <?= $section->files_isActive ? 'checked' : ''; ?>>
                  <label for="sections-check-files-<?= $section->id ?>">Активировать</label>
                </div>
              </div>
              <div class="col-12 col-lg-auto">
                <button type="submit"
                        class="btn btn-outline-primary ripple">Изменить
                </button>
              </div>
            </div>
              <?= Html::endForm() ?>
          </div>
          <div class="col-12">
            <h4 class="mt-4 mb-3">Полезные файлы:</h4>
            <!--Добавить-->
            <a href="#" data-toggle="modal" data-target="#modal-files-create"
               class="btn btn-success ripple mr-1 mb-2">
              <i class="la la-plus edit"></i>Добавить
            </a>
            <div id="modal-files-create" class="modal fade">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <?= Html::beginForm(
                        ['news/index'],
                        'post',
                        ['enctype' => 'multipart/form-data']
                    ) ?>
                    <?= Html::hiddenInput('news', 'true'); ?>
                    <?= Html::hiddenInput('files', 'true'); ?>
                    <?= Html::hiddenInput('create', 'true'); ?>
                  <div class="modal-header">
                    <h3 class="modal-title">Создание записи</h3>
                    <button type="button" class="close" data-dismiss="modal">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">close</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-12">
                        <div class="form-group">
                          <label>Наименование</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                            <select class="selectpicker show-menu-arrow" name="files_idContent"
                                    required data-live-search="true" data-style="btn btn-square ripple">
                                <? foreach (Files::find()->all() as $m): ?>
                                  <option value="<?= $m->id ?>"><?= $m->name ?></option>
                                <? endforeach; ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Приоритет</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                            <input type="text" name="files_priority"
                                   value=''
                                   class="form-control" required/>
                          </div>
                        </div>
                        <div class="form-group mb-3">
                          <label>Признак публикации</label>
                          <div class="styled-checkbox">
                            <input type="checkbox" name="files_isActive"
                                   id="check-files">
                            <label for="check-files">Активировать</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary ripple"
                            data-dismiss="modal">Закрыть
                    </button>
                    <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                    </button>
                  </div>
                    <?= Html::endForm() ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="table-responsive">
              <table class="sorting-table table mb-0">
                <thead>
                <tr>
                  <th>Наименование</th>
                  <th>Приоритет</th>
                  <th>Признак публикации</th>
                  <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                <? foreach ($files as $cFile): ?>
                  <tr>
                    <td>
                      <span class="text-primary">
                        <? $cFileContent = Files::findOne($cFile->id_files) ?>
                        <?= $cFileContent->name ?>
                      </span>
                    </td>
                    <td><?= $cFile->priority ?></td>
                    <td>
                      <span style="width:100px;">
                          <? $status = $cFile->isActive ?>
                          <? if ($status): ?>
                            <span class="badge-text badge-text-small success">Активно</span>
                          <? else: ?>
                            <span class="badge-text badge-text-small danger">Отключено</span>
                          <? endif; ?>
                      </span>
                    </td>
                    <td class="td-actions">
                      <!--Редактировать-->
                      <a href="#" data-toggle="modal" data-target="#modal-files-<?= $cFile->id ?>">
                        <i class="la la-edit edit"></i>
                      </a>
                      <div id="modal-files-<?= $cFile->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['news/index'],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('news', 'true'); ?>
                              <?= Html::hiddenInput('files', 'true'); ?>
                              <?= Html::hiddenInput('files_id', $cFile->id); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Редактирование записи: <?= $cFileContent->name ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <div class="form-group">
                                    <label>Наименование</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                          <i class="la la-pencil"></i>
                                      </span>
                                      <select class="selectpicker show-menu-arrow" name="files_idContent"
                                              required data-live-search="true" data-style="btn btn-square ripple">
                                          <? foreach (Files::find()->all() as $m): ?>
                                              <? if ($m->id == $cFileContent->id): ?>
                                              <option value="<?= $m->id ?>" selected><?= $m->name ?></option>
                                              <? else: ?>
                                              <option value="<?= $m->id ?>"><?= $m->name ?></option>
                                              <? endif; ?>
                                          <? endforeach; ?>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label>Приоритет</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                          <i class="la la-pencil"></i>
                                      </span>
                                      <input type="text" name="files_priority"
                                             value='<?= $cFile->priority ?: ''; ?>'
                                             class="form-control" required/>
                                    </div>
                                  </div>
                                  <div class="form-group mb-3">
                                    <label>Признак публикации</label>
                                    <div class="styled-checkbox">
                                      <input type="checkbox" name="files_isActive"
                                             id="check-files-<?= $cFile->id ?>"
                                          <?= $cFile->isActive ? 'checked' : ''; ?>>
                                      <label for="check-files-<?= $cFile->id ?>">Активировать</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Закрыть
                              </button>
                              <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                      <!--Удалить-->
                      <a href="#" data-toggle="modal" data-target="#modal-files-delete-<?= $cFile->id ?>">
                        <i class="la la-close delete"></i>
                      </a>
                      <div id="modal-files-delete-<?= $cFile->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered modal-sm">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['news/index'],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('news', 'true'); ?>
                              <?= Html::hiddenInput('files', 'true'); ?>
                              <?= Html::hiddenInput('files_id', $cFile->id); ?>
                              <?= Html::hiddenInput('delete', 'true'); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Удаление записи: <?= $cFileContent->name ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <p class="text-dark mb-3">Вы уверены что хотите удалить запись?</p>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Отмена
                              </button>
                              <button type="submit" class="btn btn-outline-danger ripple">Удалить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                <? endforeach; ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="i-tab-5" role="tabpanel" aria-labelledby="i-base-tab-5">
        <div class="row">
          <div class="col-12">
              <?= Html::beginForm(
                  ['news/index'],
                  'post',
                  ['enctype' => 'multipart/form-data']
              ) ?>
              <?= Html::hiddenInput('news', 'true'); ?>
              <?= Html::hiddenInput('info', 'true'); ?>
              <?= Html::hiddenInput('section', 'true'); ?>
            <h4 class="mt-4 mb-3">Текущая дополнительная информация:</h4>
            <div class="form-group row align-items-lg-end">
              <div class="col-12 col-lg-auto">
                <label>Приоритет бокового меню</label>
                <div class="input-group">
                  <span class="input-group-addon addon-primary">
                      <i class="la la-pencil"></i>
                  </span>
                  <input type="text" name="sections_info_priority"
                         value='<?= $section->priority_info ?: ''; ?>'
                         class="form-control" required/>
                </div>
              </div>
              <div class="col-12 col-lg-auto">
                <label>Признак публикации</label>
                <div class="styled-checkbox">
                  <input type="checkbox" name="sections_info_isActive"
                         id="sections-check-info-<?= $section->id ?>"
                      <?= $section->info_isActive ? 'checked' : ''; ?>>
                  <label for="sections-check-info-<?= $section->id ?>">Активировать</label>
                </div>
              </div>
              <div class="col-12 col-lg-auto">
                <button type="submit"
                        class="btn btn-outline-primary ripple">Изменить
                </button>
              </div>
            </div>
              <?= Html::endForm() ?>
          </div>
          <div class="col-12">
            <h4 class="mt-4 mb-3">Дополнительная информация:</h4>
            <!--Добавить-->
            <a href="#" data-toggle="modal" data-target="#modal-info-create"
               class="btn btn-success ripple mr-1 mb-2">
              <i class="la la-plus edit"></i>Добавить
            </a>
            <div id="modal-info-create" class="modal fade">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <?= Html::beginForm(
                        ['news/index'],
                        'post',
                        ['enctype' => 'multipart/form-data']
                    ) ?>
                    <?= Html::hiddenInput('news', 'true'); ?>
                    <?= Html::hiddenInput('info', 'true'); ?>
                    <?= Html::hiddenInput('create', 'true'); ?>
                  <div class="modal-header">
                    <h3 class="modal-title">Создание записи</h3>
                    <button type="button" class="close" data-dismiss="modal">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">close</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-12">
                        <div class="form-group">
                          <label>Наименование</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                            <select class="selectpicker show-menu-arrow" name="info_idContent"
                                    required data-live-search="true" data-style="btn btn-square ripple">
                                <? foreach (Info::find()->all() as $m): ?>
                                  <option value="<?= $m->id ?>"><?= $m->name ?></option>
                                <? endforeach; ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Приоритет</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                            <input type="text" name="info_priority"
                                   value=''
                                   class="form-control" required/>
                          </div>
                        </div>
                        <div class="form-group mb-3">
                          <label>Признак публикации</label>
                          <div class="styled-checkbox">
                            <input type="checkbox" name="info_isActive"
                                   id="check-info">
                            <label for="check-info">Активировать</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary ripple"
                            data-dismiss="modal">Закрыть
                    </button>
                    <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                    </button>
                  </div>
                    <?= Html::endForm() ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="table-responsive">
              <table class="sorting-table table mb-0">
                <thead>
                <tr>
                  <th>Наименование</th>
                  <th>Приоритет</th>
                  <th>Признак публикации</th>
                  <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                <? foreach ($info as $cInfo): ?>
                  <tr>
                    <td>
                      <span class="text-primary">
                        <? $cInfoContent = Info::findOne($cInfo->id_info) ?>
                        <?= $cInfoContent->name ?>
                      </span>
                    </td>
                    <td><?= $cInfo->priority ?></td>
                    <td>
                      <span style="width:100px;">
                          <? $status = $cInfo->isActive ?>
                          <? if ($status): ?>
                            <span class="badge-text badge-text-small success">Активно</span>
                          <? else: ?>
                            <span class="badge-text badge-text-small danger">Отключено</span>
                          <? endif; ?>
                      </span>
                    </td>
                    <td class="td-actions">
                      <!--Редактировать-->
                      <a href="#" data-toggle="modal" data-target="#modal-info-<?= $cInfo->id ?>">
                        <i class="la la-edit edit"></i>
                      </a>
                      <div id="modal-info-<?= $cInfo->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['news/index'],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('news', 'true'); ?>
                              <?= Html::hiddenInput('info', 'true'); ?>
                              <?= Html::hiddenInput('info_id', $cInfo->id); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Редактирование записи: <?= $cInfoContent->name ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <div class="form-group">
                                    <label>Наименование</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                          <i class="la la-pencil"></i>
                                      </span>
                                      <select class="selectpicker show-menu-arrow" name="info_idContent"
                                              required data-live-search="true" data-style="btn btn-square ripple">
                                          <? foreach (Info::find()->all() as $m): ?>
                                              <? if ($m->id == $cInfoContent->id): ?>
                                              <option value="<?= $m->id ?>" selected><?= $m->name ?></option>
                                              <? else: ?>
                                              <option value="<?= $m->id ?>"><?= $m->name ?></option>
                                              <? endif; ?>
                                          <? endforeach; ?>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label>Приоритет</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                          <i class="la la-pencil"></i>
                                      </span>
                                      <input type="text" name="info_priority"
                                             value='<?= $cInfo->priority ?: ''; ?>'
                                             class="form-control" required/>
                                    </div>
                                  </div>
                                  <div class="form-group mb-3">
                                    <label>Признак публикации</label>
                                    <div class="styled-checkbox">
                                      <input type="checkbox" name="info_isActive"
                                             id="check-info-<?= $cInfo->id ?>"
                                          <?= $cInfo->isActive ? 'checked' : ''; ?>>
                                      <label for="check-info-<?= $cInfo->id ?>">Активировать</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Закрыть
                              </button>
                              <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                      <!--Удалить-->
                      <a href="#" data-toggle="modal" data-target="#modal-info-delete-<?= $cInfo->id ?>">
                        <i class="la la-close delete"></i>
                      </a>
                      <div id="modal-info-delete-<?= $cInfo->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered modal-sm">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['news/index'],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('news', 'true'); ?>
                              <?= Html::hiddenInput('info', 'true'); ?>
                              <?= Html::hiddenInput('info_id', $cInfo->id); ?>
                              <?= Html::hiddenInput('delete', 'true'); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Удаление записи: <?= $cInfoContent->name ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <p class="text-dark mb-3">Вы уверены что хотите удалить запись?</p>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Отмена
                              </button>
                              <button type="submit" class="btn btn-outline-danger ripple">Удалить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                <? endforeach; ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="i-tab-6" role="tabpanel" aria-labelledby="i-base-tab-6">
        <div class="row">
          <div class="col-12">
              <?= Html::beginForm(
                  ['news/index'],
                  'post',
                  ['enctype' => 'multipart/form-data']
              ) ?>
              <?= Html::hiddenInput('news', 'true'); ?>
              <?= Html::hiddenInput('descont', 'true'); ?>
              <?= Html::hiddenInput('section', 'true'); ?>
            <h4 class="mt-4 mb-3">Текущая акция:</h4>
            <div class="form-group row align-items-lg-end">
              <div class="col-12 col-lg-auto">
                <label>Приоритет бокового меню</label>
                <div class="input-group">
                  <span class="input-group-addon addon-primary">
                      <i class="la la-pencil"></i>
                  </span>
                  <input type="text" name="sections_descont_priority"
                         value='<?= $section->priority_descont ?: ''; ?>'
                         class="form-control" required/>
                </div>
              </div>
              <div class="col-12 col-lg-auto">
                <label>Признак публикации</label>
                <div class="styled-checkbox">
                  <input type="checkbox" name="sections_descont_isActive"
                         id="sections-check-descont-<?= $section->id ?>"
                      <?= $section->descont_isActive ? 'checked' : ''; ?>>
                  <label for="sections-check-descont-<?= $section->id ?>">Активировать</label>
                </div>
              </div>
              <div class="col-12 col-lg-auto">
                <button type="submit"
                        class="btn btn-outline-primary ripple">Изменить
                </button>
              </div>
            </div>
              <?= Html::endForm() ?>
          </div>
          <div class="col-12">
            <h4 class="mt-4 mb-3">Акции:</h4>
            <!--Добавить-->
            <a href="#" data-toggle="modal" data-target="#modal-descont-create"
               class="btn btn-success ripple mr-1 mb-2">
              <i class="la la-plus edit"></i>Добавить
            </a>
            <div id="modal-descont-create" class="modal fade">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <?= Html::beginForm(
                        ['news/index'],
                        'post',
                        ['enctype' => 'multipart/form-data']
                    ) ?>
                    <?= Html::hiddenInput('news', 'true'); ?>
                    <?= Html::hiddenInput('descont', 'true'); ?>
                    <?= Html::hiddenInput('create', 'true'); ?>
                  <div class="modal-header">
                    <h3 class="modal-title">Создание записи</h3>
                    <button type="button" class="close" data-dismiss="modal">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">close</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-12">
                        <div class="form-group">
                          <label>Наименование</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                            <select class="selectpicker show-menu-arrow" name="descont_idContent"
                                    required data-live-search="true" data-style="btn btn-square ripple">
                                <? foreach (Descont::find()->all() as $m): ?>
                                  <option value="<?= $m->id ?>"><?= $m->name ?></option>
                                <? endforeach; ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Приоритет</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                            <input type="text" name="descont_priority"
                                   value=''
                                   class="form-control" required/>
                          </div>
                        </div>
                        <div class="form-group mb-3">
                          <label>Признак публикации</label>
                          <div class="styled-checkbox">
                            <input type="checkbox" name="descont_isActive"
                                   id="check-descont">
                            <label for="check-descont">Активировать</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary ripple"
                            data-dismiss="modal">Закрыть
                    </button>
                    <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                    </button>
                  </div>
                    <?= Html::endForm() ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="table-responsive">
              <table class="sorting-table table mb-0">
                <thead>
                <tr>
                  <th>Наименование</th>
                  <th>Приоритет</th>
                  <th>Признак публикации</th>
                  <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                <? foreach ($descont as $cDescont): ?>
                  <tr>
                    <td>
                      <span class="text-primary">
                        <? $cDescontContent = Descont::findOne($cDescont->id_descont) ?>
                        <?= $cDescontContent->name ?>
                      </span>
                    </td>
                    <td><?= $cDescont->priority ?></td>
                    <td>
                      <span style="width:100px;">
                          <? $status = $cDescont->isActive ?>
                          <? if ($status): ?>
                            <span class="badge-text badge-text-small success">Активно</span>
                          <? else: ?>
                            <span class="badge-text badge-text-small danger">Отключено</span>
                          <? endif; ?>
                      </span>
                    </td>
                    <td class="td-actions">
                      <!--Редактировать-->
                      <a href="#" data-toggle="modal" data-target="#modal-descont-<?= $cDescont->id ?>">
                        <i class="la la-edit edit"></i>
                      </a>
                      <div id="modal-descont-<?= $cDescont->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['news/index'],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('news', 'true'); ?>
                              <?= Html::hiddenInput('descont', 'true'); ?>
                              <?= Html::hiddenInput('descont_id', $cDescont->id); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Редактирование записи: <?= $cDescontContent->name ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <div class="form-group">
                                    <label>Наименование</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                          <i class="la la-pencil"></i>
                                      </span>
                                      <select class="selectpicker show-menu-arrow" name="descont_idContent"
                                              required data-live-search="true" data-style="btn btn-square ripple">
                                          <? foreach (Descont::find()->all() as $m): ?>
                                              <? if ($m->id == $cDescontContent->id): ?>
                                              <option value="<?= $m->id ?>" selected><?= $m->name ?></option>
                                              <? else: ?>
                                              <option value="<?= $m->id ?>"><?= $m->name ?></option>
                                              <? endif; ?>
                                          <? endforeach; ?>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label>Приоритет</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                          <i class="la la-pencil"></i>
                                      </span>
                                      <input type="text" name="descont_priority"
                                             value='<?= $cDescont->priority ?: ''; ?>'
                                             class="form-control" required/>
                                    </div>
                                  </div>
                                  <div class="form-group mb-3">
                                    <label>Признак публикации</label>
                                    <div class="styled-checkbox">
                                      <input type="checkbox" name="descont_isActive"
                                             id="check-descont-<?= $cDescont->id ?>"
                                          <?= $cDescont->isActive ? 'checked' : ''; ?>>
                                      <label for="check-descont-<?= $cDescont->id ?>">Активировать</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Закрыть
                              </button>
                              <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                      <!--Удалить-->
                      <a href="#" data-toggle="modal" data-target="#modal-descont-delete-<?= $cDescont->id ?>">
                        <i class="la la-close delete"></i>
                      </a>
                      <div id="modal-descont-delete-<?= $cDescont->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered modal-sm">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['news/index'],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('news', 'true'); ?>
                              <?= Html::hiddenInput('descont', 'true'); ?>
                              <?= Html::hiddenInput('descont_id', $cDescont->id); ?>
                              <?= Html::hiddenInput('delete', 'true'); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Удаление записи: <?= $cDescontContent->name ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <p class="text-dark mb-3">Вы уверены что хотите удалить запись?</p>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Отмена
                              </button>
                              <button type="submit" class="btn btn-outline-danger ripple">Удалить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                <? endforeach; ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>