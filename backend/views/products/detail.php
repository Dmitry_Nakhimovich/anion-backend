<?php

use common\models\FiltersContent;
use common\models\NavMenu;
use common\models\ProductsAttr;
use common\models\Stock;
use common\models\StockContent;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use mihaildev\elfinder\InputFile;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="widget has-shadow">
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <h3>Редактирование продукта</h3>
  </div>
  <div class="widget-body">
    <ul class="nav nav-tabs" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="i-base-tab-1"
           data-toggle="tab" href="#i-tab-1" role="tab"
           aria-controls="i-tab-1" aria-selected="true">
          <i class="la la-align-left mr-2"></i>
          Основной контент
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="i-base-tab-2"
           data-toggle="tab" href="#i-tab-2" role="tab"
           aria-controls="i-tab-2" aria-selected="false">
          <i class="la la-image mr-2"></i>
          Слайдер
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="i-base-tab-3"
           data-toggle="tab" href="#i-tab-3" role="tab"
           aria-controls="i-tab-3" aria-selected="false">
          <i class="la la-list-ol mr-2"></i>
          Характеристики
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="i-base-tab-4"
           data-toggle="tab" href="#i-tab-4" role="tab"
           aria-controls="i-tab-4" aria-selected="false">
          <i class="la la-ruble mr-2"></i>
          Цена
        </a>
      </li>
    </ul>
    <div class="tab-content pt-3">
      <div class="tab-pane fade show active" id="i-tab-1" role="tabpanel" aria-labelledby="i-base-tab-1">
          <?= Html::beginForm(
              ['products/detail', 'id' => $model->id],
              'post',
              ['enctype' => 'multipart/form-data']
          ) ?>
          <?= Html::hiddenInput('detail', 'true'); ?>
          <?= Html::hiddenInput('id', $model->id); ?>
        <div class="row">
          <div class="col-lg-6 col-12">
            <div class="form-group pb-3 pt-3 m-0">
              <label>Заголовок</label>
              <div class="input-group">
                <span class="input-group-addon addon-primary">
                    <i class="la la-pencil"></i>
                </span>
                <textarea name="title" rows="4"
                          class="form-control" required
                ><?= $model->title ?: ''; ?></textarea>
              </div>
            </div>
            <div class="form-group pb-3 pt-3 m-0">
              <label>Категория</label>
              <div class="input-group">
                <span class="input-group-addon addon-primary">
                    <i class="la la-pencil"></i>
                </span>
                <select class="selectpicker show-menu-arrow selectfluid" name="nav_menu"
                        required data-live-search="true" data-style="btn btn-square ripple">
                    <? $list = NavMenu::find()->where(['menu_type' => 1])
                        ->orderBy('priority')
                        ->all() ?>
                    <? foreach ($list as $n): ?>
                        <? if ($model->nav_menu_id == $n->id): ?>
                        <option value="<?= $n->id ?>" selected><?= $n->name ?></option>
                        <? else: ?>
                        <option value="<?= $n->id ?>"><?= $n->name ?></option>
                        <? endif; ?>
                    <? endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group pb-3 pt-3 m-0">
              <label>Признак комплектующих</label>
              <div class="styled-checkbox">
                <input type="checkbox" name="isComplect"
                       id="check-isComplect-<?= $model->id ?>"
                    <?= $model->isComplect ? 'checked' : ''; ?>>
                <label for="check-isComplect-<?= $model->id ?>">Активировать</label>
              </div>
            </div>
            <div class="form-group pb-3 pt-3 m-0">
              <label>Артикл</label>
              <div class="input-group">
                <span class="input-group-addon addon-primary">
                    <i class="la la-pencil"></i>
                </span>
                <input type="text" name="article"
                       value='<?= $model->article ?>'
                       class="form-control" required/>
              </div>
            </div>
            <!--<div class="form-group pb-3 pt-3 m-0">
              <label>Цена</label>
              <div class="input-group">
                <span class="input-group-addon addon-primary">
                    <i class="la la-pencil"></i>
                </span>
                <input type="text" name="price"
                       value='<? /*= $model->price */ ?>'
                       class="form-control" required/>
              </div>
            </div>-->
            <div class="form-group pb-3 pt-3 m-0">
              <label>Ссылка</label>
              <div class="input-group">
                <span class="input-group-addon addon-primary">
                    <i class="la la-pencil"></i>
                </span>
                <input type="text" name="link"
                       value='<?= $model->link ?>'
                       class="form-control"/>
              </div>
            </div>
            <div class="form-group pb-3 pt-3 m-0">
              <label>Дата публикации</label>
              <div class="input-group">
                <span class="input-group-addon addon-primary">
                    <i class="la la-calendar"></i>
                </span>
                <input type="text" name="date" id="datesingle"
                       value='<?= Yii::$app->formatter->asDate($model->date) ?: ''; ?>'
                       class="form-control" required/>
              </div>
            </div>
            <div class="form-group pb-3 pt-3 m-0">
              <label>Признак публикации</label>
              <div class="styled-checkbox">
                <input type="checkbox" name="isActive"
                       id="check-<?= $model->id ?>"
                    <?= $model->isActive ? 'checked' : ''; ?>>
                <label for="check-<?= $model->id ?>">Активировать</label>
              </div>
            </div>
            <div class="form-group pb-3 pt-3 m-0">
              <label>Ссылка на изображение</label>
              <div class="input-group">
                <span class="input-group-addon addon-primary">
                  <i class="la la-file"></i>
                </span>
                  <?= InputFile::widget([
                      'language' => 'ru',
                      'filter' => ['image/jpeg', 'image/jpeg', 'image/png'],
                      'name' => 'img',
                      'options' => [
                          'id' => 'img-detail-src',
                          'class' => 'form-control',
                          //'required' => 'required',
                      ],
                      'buttonOptions' => [
                          'class' => 'btn btn-primary btn-square ripple'
                      ],
                      'buttonName' => 'Выбрать',
                      'value' => $model->img,
                  ]); ?>
              </div>
            </div>
            <script>
                $(function () {
                    $('#img-detail-src').on('change', function () {
                        $('#img-detail-prev').prop('src', $(this).val());
                    });
                });
            </script>
            <div class="form-group pb-3 pt-3 m-0">
              <div class="img-pic">
                <img id="img-detail-prev" src="<?= $model->img ?: '/admin/upload/no-pic.png' ?>" alt="">
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-12">
            <div class="form-group pb-3 pt-3 m-0">
              <label>Текст</label>
              <div class="cke-group">
                  <?= CKEditor::widget([
                      'name' => 'text',
                      'model' => $model,
                      'value' => $model->text,
                      'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                          'preset' => 'full',
                          'inline' => false,
                      ]),
                  ]); ?>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="widget-footer bordered">
              <a href="<?= Url::toRoute(['/products/detail', 'id' => $model->id]) ?>"
                 class="btn btn-outline-secondary ripple">Отменить
              </a>
              <button type="submit"
                      class="btn btn-outline-primary ripple">Сохранить
              </button>
            </div>
          </div>
        </div>
          <?= Html::endForm() ?>
      </div>
      <div class="tab-pane fade" id="i-tab-2" role="tabpanel" aria-labelledby="i-base-tab-2">
        <div class="row">
          <div class="col-12">
            <h4 class="mt-4 mb-3">Изображения в слайдере продукции</h4>
            <!--Добавить-->
            <a href="#" data-toggle="modal" data-target="#modal-slider-create"
               class="btn btn-success ripple mr-1 mb-2">
              <i class="la la-plus edit"></i>Добавить
            </a>
            <div id="modal-slider-create" class="modal fade">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <?= Html::beginForm(
                        ['products/detail', 'id' => $model->id],
                        'post',
                        ['enctype' => 'multipart/form-data']
                    ) ?>
                    <?= Html::hiddenInput('detail', 'true'); ?>
                    <?= Html::hiddenInput('id', $model->id); ?>
                    <?= Html::hiddenInput('slider', 'true'); ?>
                    <?= Html::hiddenInput('create', 'true'); ?>
                  <div class="modal-header">
                    <h3 class="modal-title">Создание записи</h3>
                    <button type="button" class="close" data-dismiss="modal">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">close</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-12">
                        <div class="form-group">
                          <label>Наименование</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                            <input type="text" name="slider_name"
                                   value=''
                                   class="form-control"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Ссылка на изображение</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                              <i class="la la-file"></i>
                            </span>
                              <?= InputFile::widget([
                                  'language' => 'ru',
                                  'filter' => ['image/jpeg', 'image/jpeg', 'image/png'],
                                  'name' => 'slider_img',
                                  'options' => [
                                      //'id' => 'img-detail-src',
                                      'class' => 'form-control',
                                      'required' => 'required',
                                  ],
                                  'buttonOptions' => [
                                      'class' => 'btn btn-primary btn-square ripple'
                                  ],
                                  'buttonName' => 'Выбрать',
                                  'value' => '',
                              ]); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Приоритет</label>
                          <div class="input-group">
                            <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                            </span>
                            <input type="text" name="slider_priority"
                                   value=''
                                   class="form-control" required/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary ripple"
                            data-dismiss="modal">Закрыть
                    </button>
                    <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                    </button>
                  </div>
                    <?= Html::endForm() ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12">
            <div class="table-responsive">
              <table id="sorting-table" class="table mb-0">
                <thead>
                <tr>
                  <th>Наименование</th>
                  <th>Изображение</th>
                  <th>Приоритет</th>
                  <th>Действия</th>
                </tr>
                </thead>
                <tbody>

                <? foreach ($slider as $item): ?>
                  <tr>
                    <td>
                      <span class="text-primary">
                          <?= $item->name ?>
                      </span>
                    </td>
                    <td>
                      <img src="<?= $item->img ?: '/admin/upload/no-pic.png' ?>" alt=""
                           style="max-width: 300px; max-height: 300px">
                    </td>
                    <td><?= $item->priority ?></td>
                    <td class="td-actions">
                      <!--Редактировать-->
                      <a href="#" data-toggle="modal" data-target="#modal-slider-<?= $item->id ?>">
                        <i class="la la-edit edit"></i>
                      </a>
                      <div id="modal-slider-<?= $item->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['products/detail', 'id' => $model->id],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('detail', 'true'); ?>
                              <?= Html::hiddenInput('id', $model->id); ?>
                              <?= Html::hiddenInput('slider', 'true'); ?>
                              <?= Html::hiddenInput('slider_id', $item->id); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Редактирование записи: <?= $item->name ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <div class="form-group">
                                    <label>Наименование</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                          <i class="la la-pencil"></i>
                                      </span>
                                      <input type="text" name="slider_name"
                                             value='<?= $item->name ?>'
                                             class="form-control"/>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label>Ссылка на изображение</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                        <i class="la la-file"></i>
                                      </span>
                                        <?= InputFile::widget([
                                            'language' => 'ru',
                                            'filter' => ['image/jpeg', 'image/jpeg', 'image/png'],
                                            'name' => 'slider_img',
                                            'options' => [
                                                //'id' => 'img-detail-src',
                                                'class' => 'form-control',
                                                'required' => 'required',
                                            ],
                                            'buttonOptions' => [
                                                'class' => 'btn btn-primary btn-square ripple'
                                            ],
                                            'buttonName' => 'Выбрать',
                                            'value' => $item->img,
                                        ]); ?>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label>Приоритет</label>
                                    <div class="input-group">
                                      <span class="input-group-addon addon-primary">
                                          <i class="la la-pencil"></i>
                                      </span>
                                      <input type="text" name="slider_priority"
                                             value='<?= $item->priority ?>'
                                             class="form-control" required/>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Закрыть
                              </button>
                              <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                      <!--Удалить-->
                      <a href="#" data-toggle="modal" data-target="#modal-slider-delete-<?= $item->id ?>">
                        <i class="la la-close delete"></i>
                      </a>
                      <div id="modal-slider-delete-<?= $item->id ?>" class="modal fade">
                        <div class="modal-dialog modal-dialog-centered modal-sm">
                          <div class="modal-content">
                              <?= Html::beginForm(
                                  ['products/detail', 'id' => $model->id],
                                  'post',
                                  ['enctype' => 'multipart/form-data']
                              ) ?>
                              <?= Html::hiddenInput('detail', 'true'); ?>
                              <?= Html::hiddenInput('id', $model->id); ?>
                              <?= Html::hiddenInput('slider', 'true'); ?>
                              <?= Html::hiddenInput('slider_id', $item->id); ?>
                              <?= Html::hiddenInput('delete', 'true'); ?>
                            <div class="modal-header">
                              <h3 class="modal-title">Удаление записи: <?= $item->name ?></h3>
                              <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">close</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-12">
                                  <p class="text-dark mb-3">Вы уверены что хотите удалить запись?</p>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary ripple"
                                      data-dismiss="modal">Отмена
                              </button>
                              <button type="submit" class="btn btn-outline-danger ripple">Удалить
                              </button>
                            </div>
                              <?= Html::endForm() ?>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                <? endforeach; ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="i-tab-3" role="tabpanel" aria-labelledby="i-base-tab-3">
          <?= Html::beginForm(
              ['products/detail', 'id' => $model->id],
              'post',
              ['enctype' => 'multipart/form-data']
          ) ?>
        <div class="row">
          <div class="col-12 col-lg-6">
              <?= Html::hiddenInput('detail', 'true'); ?>
              <?= Html::hiddenInput('id', $model->id); ?>
              <?= Html::hiddenInput('attr', 'true'); ?>
              <? foreach ($attr as $c): ?>
                  <? $cattr = ProductsAttr::findOne($c->attr_id) ?>
                <div class="form-group">
                  <label>Характеристика: <?= $cattr->name ?></label>
                    <? if ($cattr->type == 'text'): ?>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="text_<?= $c->id ?>"
                               value='<?= $c->text ?>'
                               class="form-control" required/>
                      </div>
                    <? elseif ($cattr->type == 'num'): ?>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="number" name="text_<?= $c->id ?>"
                               value='<?= $c->num ?>'
                               class="form-control" required/>
                      </div>
                    <? elseif ($cattr->type == 'select'): ?>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <select class="selectpicker show-menu-arrow selectfluid" name="text_<?= $c->id ?>"
                                required data-style="btn btn-square ripple">
                            <? $list = FiltersContent::find()->where(['attr_id' => $cattr->id])->all() ?>
                            <? foreach ($list as $n): ?>
                                <? if ($c->text == $n->text): ?>
                                <option value="<?= $n->text ?>" selected><?= $n->text ?></option>
                                <? else: ?>
                                <option value="<?= $n->text ?>"><?= $n->text ?></option>
                                <? endif; ?>
                            <? endforeach; ?>
                        </select>
                      </div>
                    <? endif; ?>
                </div>
              <? endforeach; ?>
          </div>
          <div class="col-12">
            <div class="widget-footer bordered">
              <a href="<?= Url::toRoute(['/products/detail', 'id' => $model->id]) ?>"
                 class="btn btn-outline-secondary ripple">Отменить
              </a>
              <button type="submit"
                      class="btn btn-outline-primary ripple">Сохранить
              </button>
            </div>
          </div>
        </div>
          <?= Html::endForm() ?>
      </div>
      <div class="tab-pane fade" id="i-tab-4" role="tabpanel" aria-labelledby="i-base-tab-4">
          <?= Html::beginForm(
              ['products/detail', 'id' => $model->id],
              'post',
              ['enctype' => 'multipart/form-data']
          ) ?>
        <div class="row">
          <div class="col-12 col-lg-6">
              <?= Html::hiddenInput('detail', 'true'); ?>
              <?= Html::hiddenInput('id', $model->id); ?>
              <?= Html::hiddenInput('stock', 'true'); ?>
              <? foreach ($stocks as $c): ?>
                  <? $cstock = Stock::findOne($c->stock_id) ?>
                <div class="form-group">
                  <label><?= $cstock->name ?></label>
                  <div class="input-group">
                    <span class="input-group-addon addon-primary">
                        <i class="la la-pencil"></i>
                    </span>
                    <input type="number" name="price_<?= $c->id ?>"
                           value='<?= $c->price ?>'
                           class="form-control" required/>
                  </div>
                </div>
              <? endforeach; ?>
          </div>
          <div class="col-12">
            <div class="widget-footer bordered">
              <a href="<?= Url::toRoute(['/products/detail', 'id' => $model->id]) ?>"
                 class="btn btn-outline-secondary ripple">Отменить
              </a>
              <button type="submit"
                      class="btn btn-outline-primary ripple">Сохранить
              </button>
            </div>
          </div>
        </div>
          <?= Html::endForm() ?>
      </div>
    </div>
  </div>
</div>

