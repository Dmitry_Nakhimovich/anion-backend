<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="widget has-shadow">
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <h3>Редактирование характеристики: <?= $model->name ?></h3>
  </div>
  <div class="widget-body">
    <div class="row">
      <div class="col-12 col-lg-6">
          <?= Html::beginForm(
              ['menu/detail-attr', 'id' => $model->id],
              'post',
              ['enctype' => 'multipart/form-data']
          ) ?>
          <?= Html::hiddenInput('filters', 'true'); ?>
          <?= Html::hiddenInput('create', 'true'); ?>
        <div class="form-group row align-items-lg-end">
          <div class="col-12 col-lg">
            <label>Новая опция фильтра</label>
            <div class="input-group">
              <span class="input-group-addon addon-primary">
                  <i class="la la-pencil"></i>
              </span>
              <input type="text" name="text_new"
                     value=''
                     class="form-control" required/>
            </div>
          </div>
          <div class="col-12 col-lg-auto">
            <button type="submit"
                    class="btn btn-outline-primary ripple">Добавить
            </button>
          </div>
        </div>
          <?= Html::endForm() ?>
      </div>
      <div class="col-lg-6"></div>
      <div class="col-12 col-lg-6">
          <?= Html::beginForm(
              ['menu/detail-attr', 'id' => $model->id],
              'post',
              ['enctype' => 'multipart/form-data', 'id' => 'filtersForm']
          ) ?>
          <?= Html::hiddenInput('filters', 'true'); ?>
          <?= Html::hiddenInput('update', 'true'); ?>
          <? foreach ($content as $c): ?>
            <div class="form-group row align-items-lg-end">
              <div class="col-12 col-lg">
                <label>Содержание опции фильтра</label>
                <div class="input-group">
                  <span class="input-group-addon addon-primary">
                      <i class="la la-pencil"></i>
                  </span>
                  <input type="text" name="text_<?= $c->id ?>"
                         value='<?= $c->text ?>'
                         class="form-control" required/>
                </div>
              </div>
              <div class="col-12 col-lg-auto">
                  <?= Html::a(
                      'Удалить',
                      ['menu/detail-attr', 'id' => $model->id],
                      [
                          'data' => [
                              'method' => 'post',
                              'params' => ['delete' => $c->id],
                          ],
                          'class' => 'btn btn-outline-danger ripple',
                      ]
                  ) ?>
              </div>
            </div>
          <? endforeach; ?>
          <?= Html::endForm() ?>
      </div>
    </div>
  </div>
  <div class="widget-footer bordered">
    <a href="<?= Url::toRoute(['menu/detail-products', 'id' => $model->sections_id]) ?>"
       class="btn btn-outline-secondary ripple">Отменить
    </a>
    <button type="submit" form="filtersForm"
            class="btn btn-outline-primary ripple">Сохранить
    </button>
  </div>
</div>