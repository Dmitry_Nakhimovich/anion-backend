<?php

use common\models\NavMenu;
use common\models\NavMenuImg;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use mihaildev\elfinder\InputFile;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="widget has-shadow">
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <div class="row">
      <div class="col-12">
        <h3 class="mt-3 mb-4">Меню категорий продукции</h3>
      </div>
      <div class="col-12">
        <!--Добавить-->
        <a href="#" data-toggle="modal" data-target="#modal-create"
           class="btn btn-success ripple mr-1 mb-2">
          <i class="la la-plus edit"></i>Добавить
        </a>
        <div id="modal-create" class="modal fade">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <?= Html::beginForm(
                    ['menu/products'],
                    'post',
                    ['enctype' => 'multipart/form-data']
                ) ?>
                <?= Html::hiddenInput('products', 'true'); ?>
                <?= Html::hiddenInput('create', 'true'); ?>
              <div class="modal-header">
                <h3 class="modal-title">Создание записи</h3>
                <button type="button" class="close" data-dismiss="modal">
                  <span aria-hidden="true">×</span>
                  <span class="sr-only">close</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-12 col-lg-6">
                    <div class="form-group">
                      <label>Наименование</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="name"
                               value=''
                               class="form-control" required/>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Ссылка</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="link"
                               value=''
                               class="form-control"/>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Родительский элемент</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <select class="selectpicker show-menu-arrow" name="parent_id"
                                required data-live-search="true" data-style="btn btn-square ripple">
                          <option value="0" selected>Корневой элемент</option>
                            <? foreach ($model as $n): ?>
                              <option value="<?= $n->id ?>"><?= $n->name ?></option>
                            <? endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group pb-3 pt-3 m-0">
                      <label>Ссылка на изображение</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                          <i class="la la-file"></i>
                        </span>
                          <?= InputFile::widget([
                              'language' => 'ru',
                              'filter' => ['image/jpeg', 'image/jpeg', 'image/png'],
                              'name' => 'img',
                              'options' => [
                                  'id' => 'img-create-src',
                                  'class' => 'form-control',
                                  //'required' => 'required',
                              ],
                              'buttonOptions' => [
                                  'class' => 'btn btn-primary btn-square ripple'
                              ],
                              'buttonName' => 'Выбрать',
                              'value' => '',
                          ]); ?>
                      </div>
                    </div>
                    <script>
                        $(function () {
                            $('#img-create-src').on('change', function () {
                                $('#img-create-prev').prop('src', $(this).val());
                            });
                        });
                    </script>
                    <div class="form-group pb-3 pt-3 m-0">
                      <div class="img-pic">
                        <img id="img-create-prev" src="" alt="">
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-lg-6">
                    <div class="form-group">
                      <label>Маркер</label>
                      <div class="input-group">
                          <span class="input-group-addon addon-primary">
                              <i class="la la-pencil"></i>
                          </span>
                        <input type="text" name="tag"
                               value=''
                               class="form-control"/>
                      </div>
                    </div>
                    <div class="form-group mb-3">
                      <label>Показать маркер</label>
                      <div class="styled-checkbox">
                        <input type="checkbox" name="tag_isActive"
                               id="tag_check">
                        <label for="tag_check">Активировать</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Порядок отображения</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="priority"
                               value=''
                               class="form-control" required/>
                      </div>
                    </div>
                    <div class="form-group mb-3">
                      <label>Признак публикации</label>
                      <div class="styled-checkbox">
                        <input type="checkbox" name="isActive"
                               id="check-create">
                        <label for="check-create">Активировать</label>
                      </div>
                    </div>
                    <div class="form-group mb-3">
                      <label>Комплектующие</label>
                      <div class="styled-checkbox">
                        <input type="checkbox" name="isComplect"
                               id="check-complect">
                        <label for="check-complect">Активировать</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary ripple"
                        data-dismiss="modal">Закрыть
                </button>
                <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                </button>
              </div>
                <?= Html::endForm() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="widget-body">
    <div class="table-responsive">
      <table id="sorting-table" class="table mb-0 products-table">
        <thead>
        <tr>
          <th>Наименование</th>
          <th>Уровень вложенности</th>
          <th>Родительский элемент</th>
          <th>Порядок отображения</th>
          <th>Ссылка</th>
          <th>Признак публикации</th>
          <th>Комплектующие</th>
          <th>Действия</th>
        </tr>
        </thead>
        <tbody>

        <? foreach ($model as $item): ?>
          <tr>
            <td>
              <span class="text-primary"><?= $item->name ?></span>
            </td>
            <td><?= $item->menu_level ?></td>
            <td><?= NavMenu::findOne($item->parent_id)->name ?: 'Корневой элемент' ?></td>
            <td><?= $item->priority ?></td>
            <td class="table-link">
                <?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/products/category/' . $item->link]) ?>
            </td>
            <td>
              <span style="width:100px;">
                  <? $status = $item->isActive ?>
                  <? if ($status): ?>
                    <span class="badge-text badge-text-small success">Активно</span>
                  <? else: ?>
                    <span class="badge-text badge-text-small danger">Отключено</span>
                  <? endif; ?>
              </span>
            </td>
            <td>
              <span style="width:100px;">
                  <? $status = $item->isComplect ?>
                  <? if ($status): ?>
                    <span class="badge-text badge-text-small success">Активно</span>
                  <? else: ?>
                    <span class="badge-text badge-text-small danger">Отключено</span>
                  <? endif; ?>
              </span>
            </td>
            <td class="td-actions">
              <!--Редактирование-->
              <a href="#" data-toggle="modal" data-target="#modal-<?= $item->id ?>">
                <i class="la la-edit edit"></i>
              </a>
              <div id="modal-<?= $item->id ?>" class="modal fade">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                      <?= Html::beginForm(
                          ['menu/products'],
                          'post',
                          ['enctype' => 'multipart/form-data']
                      ) ?>
                      <?= Html::hiddenInput('products', 'true'); ?>
                      <?= Html::hiddenInput('id', $item->id); ?>
                    <div class="modal-header">
                      <h3 class="modal-title">Редактирование записи: <?= $item->name ?></h3>
                      <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-12 col-lg-6">
                          <div class="form-group">
                            <label>Наименование</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="name"
                                     value='<?= $item->name ?: ''; ?>'
                                     class="form-control" required="required"/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Ссылка</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="link"
                                     value='<?= $item->link ?: ''; ?>'
                                     class="form-control"/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Родительский элемент</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <select class="selectpicker show-menu-arrow" name="parent_id"
                                      required data-live-search="true" data-style="btn btn-square ripple">
                                  <? if ($item->parent_id == 0): ?>
                                    <option value="0" selected>Корневой элемент</option>
                                  <? else: ?>
                                    <option value="0">Корневой элемент</option>
                                  <? endif; ?>
                                  <? foreach ($model as $n): ?>
                                      <? if ($n->id == $item->parent_id): ?>
                                      <option value="<?= $n->id ?>" selected><?= $n->name ?></option>
                                      <? elseif ($n->id != $item->id): ?>
                                      <option value="<?= $n->id ?>"><?= $n->name ?></option>
                                      <? endif; ?>
                                  <? endforeach; ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group pb-3 pt-3 m-0">
                            <label>Ссылка на изображение</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                <i class="la la-file"></i>
                              </span>
                                <?= InputFile::widget([
                                    'language' => 'ru',
                                    'filter' => ['image/jpeg', 'image/jpeg', 'image/png'],
                                    'name' => 'img',
                                    'options' => [
                                        'id' => 'img-edit-src-' . $item->id,
                                        'class' => 'form-control',
                                        //'required' => 'required',
                                    ],
                                    'buttonOptions' => [
                                        'class' => 'btn btn-primary btn-square ripple'
                                    ],
                                    'buttonName' => 'Выбрать',
                                    'value' => NavMenuImg::find()
                                        ->where(['product_id' => $item->id])->one()->img,
                                ]); ?>
                            </div>
                          </div>
                          <script>
                              $(function () {
                                  $('#img-edit-src-<?= $item->id ?>').on('change', function () {
                                      $('#img-edit-prev-<?= $item->id ?>').prop('src', $(this).val());
                                  });
                                  $('#img-edit-src-<?= $item->id ?>').trigger('change');
                              });
                          </script>
                          <div class="form-group pb-3 pt-3 m-0">
                            <div class="img-pic">
                              <img id="img-edit-prev-<?= $item->id ?>" src="" alt="">
                            </div>
                          </div>
                        </div>
                        <div class="col-12 col-lg-6">
                          <div class="form-group">
                            <label>Маркер</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="tag"
                                     value='<?= $item->tag ?: ''; ?>'
                                     class="form-control"/>
                            </div>
                          </div>
                          <div class="form-group mb-3">
                            <label>Показать маркер</label>
                            <div class="styled-checkbox">
                              <input type="checkbox" name="tag_isActive"
                                     id="tag_check-<?= $item->id ?>"
                                  <?= $item->tag_isActive ? 'checked' : ''; ?>>
                              <label for="tag_check-<?= $item->id ?>">Активировать</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Порядок отображения</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="priority"
                                     value='<?= $item->priority ?: ''; ?>'
                                     class="form-control" required="required"/>
                            </div>
                          </div>
                          <div class="form-group mb-3">
                            <label>Признак публикации</label>
                            <div class="styled-checkbox">
                              <input type="checkbox" name="isActive"
                                     id="check-<?= $item->id ?>"
                                  <?= $item->isActive ? 'checked' : ''; ?>>
                              <label for="check-<?= $item->id ?>">Активировать</label>
                            </div>
                          </div>
                          <div class="form-group mb-3">
                            <label>Комплектующие</label>
                            <div class="styled-checkbox">
                              <input type="checkbox" name="isComplect"
                                     id="check-complect-<?= $item->id ?>"
                                  <?= $item->isComplect ? 'checked' : ''; ?>>
                              <label for="check-complect-<?= $item->id ?>">Активировать</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-secondary ripple"
                              data-dismiss="modal">Закрыть
                      </button>
                      <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                      </button>
                    </div>
                      <?= Html::endForm() ?>
                  </div>
                </div>
              </div>
              <!--Детальная-->
              <a href="<?= Url::toRoute(['/menu/detail-products', 'id' => $item->external_id]) ?>">
                <i class="la la-outdent edit"></i>
              </a>
              <!--Удалить-->
              <a href="#" data-toggle="modal" data-target="#modal-delete-<?= $item->id ?>">
                <i class="la la-close delete"></i>
              </a>
              <div id="modal-delete-<?= $item->id ?>" class="modal fade">
                <div class="modal-dialog modal-dialog-centered modal-sm">
                  <div class="modal-content">
                      <?= Html::beginForm(
                          ['menu/products'],
                          'post',
                          ['enctype' => 'multipart/form-data']
                      ) ?>
                      <?= Html::hiddenInput('products', 'true'); ?>
                      <?= Html::hiddenInput('id', $item->id); ?>
                      <?= Html::hiddenInput('delete', 'true'); ?>
                    <div class="modal-header">
                      <h3 class="modal-title">Удаление записи: <?= $item->name ?></h3>
                      <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-12">
                          <p class="text-dark mb-3">Вы уверены что хотите удалить запись?</p>
                          <p class="text-dark mb-3">Все характеристики и подкатегории будут удалены!</p>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-secondary ripple"
                              data-dismiss="modal">Отмена
                      </button>
                      <button type="submit" class="btn btn-outline-danger ripple">Удалить
                      </button>
                    </div>
                      <?= Html::endForm() ?>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        <? endforeach; ?>

        </tbody>
      </table>
    </div>
  </div>
</div>