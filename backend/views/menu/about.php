<?php

use common\models\Materials;
use common\models\SectionsType;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="widget has-shadow">
    <?= Html::beginForm(
        ['menu/about'],
        'post',
        ['enctype' => 'multipart/form-data']
    ) ?>
    <?= Html::hiddenInput('menu', 'true'); ?>
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <h3>Материал страницы: <?= SectionsType::findOne(5)->name ?></h3>
  </div>
  <div class="widget-body">
    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="form-group pb-3 pt-3 m-0">
          <label>Материал</label>
          <div class="input-group">
            <span class="input-group-addon addon-primary">
                <i class="la la-pencil"></i>
            </span>
            <select class="selectpicker show-menu-arrow selectfluid" name="materials"
                    required data-live-search="true" data-style="btn btn-square ripple">
                <? foreach (Materials::find()->all() as $m): ?>
                    <? if ($m->id == $model->id_item): ?>
                    <option value="<?= $m->id ?>" selected><?= $m->title ?></option>
                    <? else: ?>
                    <option value="<?= $m->id ?>"><?= $m->title ?></option>
                    <? endif; ?>
                <? endforeach; ?>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="widget-footer bordered">
    <a href="<?= Url::toRoute(['/menu/about']) ?>"
       class="btn btn-outline-secondary ripple">Отменить
    </a>
    <button type="submit"
            class="btn btn-outline-primary ripple">Сохранить
    </button>
  </div>
    <?= Html::endForm() ?>
</div>