<?php

use common\models\Products;
use common\models\Settings;
use common\models\Stock;
use common\models\StockContent;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="widget has-shadow">
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <div class="row">
      <div class="col-12">
        <h3 class="mb-4">Детали заказа</h3>
      </div>
    </div>
  </div>
  <div class="widget-body">
    <div class="table-responsive">
      <table width="100%" border="0" cellpadding="0" cellspacing="0"
             style="border-collapse: collapse; border-spacing: 0;font-family:'Open Sans', sans-serif;">
        <tr>
          <td width="100%" align="left">
            <table border="0" cellpadding="0" cellspacing="0" width="600"
                   style="border-collapse: collapse; border-spacing: 0;">
              <!-- main start-->
              <tr>
                <td width="100%" style="background-color: #ffffff;padding-left: 10px; padding-right: 10px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%"
                         style="border-collapse: collapse; border-spacing: 0;">
                    <tr>
                      <td width="100%" height="50"></td>
                    </tr>
                    <tr>
                      <td>
                        <span style="color: #003068; font-weight: bold;">Позиции заказа:</span>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="35"></td>
                    </tr>
                    <tr>
                      <td width="100%">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0"
                               style="border-collapse: collapse; border-spacing: 0;">
                            <? $cart_res = 0; ?>
                            <? foreach ($model as $product): ?>
                                <? $cproduct = Products::findOne($product->id_product); ?>
                              <tr>
                                <td>
                                  <img src="<?= Url::to($cproduct->img, true) ?>" width="80" height="50">
                                </td>
                                <td>
                                  <span style="display: block;color: #003068; font-size: 14px;font-weight: 600;">
                                    <?= $cproduct->title ?>
                                  </span>
                                  <span style="display: block;color: #9e9e9e; font-size: 12px;"
                                  ><?= $product->count ?> шт.</span>
                                </td>
                                <td style="padding-bottom: 20px;">
                                  <span style="color: #003068;font-weight: 600;"
                                  ><?= $product->count * $product->price ?> ₽</span>
                                    <? $cart_res += $product->count * $product->price ?>
                                </td>
                              </tr>
                            <? endforeach; ?>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="45"></td>
                    </tr>
                    <tr>
                      <td width="100%">
                        <table>
                          <tr>
                            <td style="color: #949494;">
                              <span>Склад:</span>
                            </td>
                            <td style="text-decoration: underline;color: #003068;">
                                <? $stock = Stock::findOne($stock_id) ?>
                              <span style="padding-left: 15px"><?= $stock->name ?></span>
                            </td>
                            <td style="color: #949494;">
                              <span style="padding-left: 30px">Итого:</span>
                            </td>
                            <td style="color: #66c1ff;font-size: 18px;">
                              <span style="padding-left: 20px"><?= $cart_res ?: 0 ?> ₽</span>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="45"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <!-- main end-->
            </table>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>