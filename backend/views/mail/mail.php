<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="widget has-shadow">
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <div class="row">
      <div class="col-12">
        <h3 class="mb-4">Входящие сообщения</h3>
      </div>
    </div>
  </div>
  <div class="widget-body">
    <div class="table-responsive">
      <table id="sorting-table" class="table mb-0">
        <thead>
        <tr>
          <th>Имя</th>
          <th>Компания</th>
          <th>Телефон</th>
          <th>E-mail</th>
          <th>Текст</th>
          <th>Дата</th>
          <th>Действия</th>
        </tr>
        </thead>
        <tbody>

        <? foreach ($model as $item): ?>
          <tr>
            <td>
              <span class="text-primary">
                  <?= $item->name ?>
              </span>
            </td>
            <td><?= $item->company ?></td>
            <td><?= $item->tel ?></td>
            <td><?= $item->email ?></td>
            <td>
              <span class="text-primary">
                <?= $item->text ?>
              </span>
            </td>
            <td><?= Yii::$app->formatter->asDate($item->date) ?: ''; ?></td>
            <td class="td-actions">
              <!--Удалить-->
              <a href="#" data-toggle="modal" data-target="#modal-delete-<?= $item->id ?>">
                <i class="la la-close delete"></i>
              </a>
              <div id="modal-delete-<?= $item->id ?>" class="modal fade">
                <div class="modal-dialog modal-dialog-centered modal-sm">
                  <div class="modal-content">
                      <?= Html::beginForm(
                          ['mail/mail'],
                          'post',
                          ['enctype' => 'multipart/form-data']
                      ) ?>
                      <?= Html::hiddenInput('mail', 'true'); ?>
                      <?= Html::hiddenInput('delete', 'true'); ?>
                      <?= Html::hiddenInput('id', $item->id); ?>
                    <div class="modal-header">
                      <h3 class="modal-title">Удаление записи: <?= $item->name ?></h3>
                      <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-12">
                          <p class="text-dark mb-3">Вы уверены что хотите удалить запись?</p>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-secondary ripple"
                              data-dismiss="modal">Отмена
                      </button>
                      <button type="submit" class="btn btn-outline-danger ripple">Удалить
                      </button>
                    </div>
                      <?= Html::endForm() ?>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        <? endforeach; ?>

        </tbody>
      </table>
    </div>
  </div>
</div>