<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\MainAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

MainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>

  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
  <script>
      WebFont.load({
          google: {"families": ["Montserrat:300,400,500,600,700:cyrillic"]},
          active: function () {
              sessionStorage.fonts = true;
          }
      });
  </script>

  <!--<link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">-->
  <link rel="shortcut icon" type="image/x-icon"  sizes="32x32" href="<?= Yii::$app->homeUrl ?>assets/img/favicon.ico">
  <!--<link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">-->

    <?php $this->head() ?>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script src="<?= Yii::getAlias('@web') ?>/assets/vendors/js/base/jquery.min.js"></script>
</head>
<body id="page-top">
<?php $this->beginBody() ?>

<div id="preloader">
  <div class="canvas">
    <div class="spinner"></div>
  </div>
</div>

<div class="page">

  <header class="header">
    <nav class="navbar fixed-top">

      <div class="search-box">
        <button class="dismiss"><i class="ion-close-round"></i></button>
        <form id="searchForm" action="#" role="search">
          <input type="search" placeholder="Search something ..." class="form-control">
        </form>
      </div>

      <div class="navbar-holder d-flex align-items-center align-middle justify-content-between">

        <!--Левая часть хедера-->
        <div class="navbar-header">

          <a href="<?= Yii::$app->homeUrl ?>" class="navbar-brand">
            <div class="brand-image brand-big">
              <img src="/admin/assets/img/custom/logo.png" alt="logo" class="logo-big">
            </div>
            <div class="brand-image brand-small">
              <img src="/admin/assets/img/custom/logo-min.png" alt="logo" class="logo-small">
            </div>
          </a>

          <a id="toggle-btn" href="#" class="menu-btn active">
            <span></span>
            <span></span>
            <span></span>
          </a>

        </div>

        <!--Правая часть хедера-->
        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center pull-right">

          <li class="nav-item">
            <a id="search" href="#">
              <i class="la la-search"></i>
            </a>
          </li>

          <li class="nav-item">
              <?= Html::a(
                  '<i class="la la-sign-out"></i>',
                  ['site/logout'],
                  [
                      'data' => ['method' => 'post',],
                      'class' => 'user-logout',
                  ]
              ) ?>
          </li>

        </ul>

      </div>

    </nav>
  </header>

  <main class="page-content d-flex align-items-stretch">

    <!--Сайдбар-->
    <div class="default-sidebar blue">
      <nav class="side-navbar box-scroll sidebar-scroll">

          <?= $this->render('../common/_menulinks') ?>

      </nav>
    </div>

    <!--Основной контент-->
    <div class="content-inner">
      <!--Контейнер контента-->
      <div class="container-fluid">

        <!--Крошки навигации-->
        <div class="row">
          <div class="page-header">
            <div class="d-flex align-items-center">
              <!--Заголовок страницы-->
              <h2 class="page-header-title"><?= Html::encode($this->title) ?></h2>
              <div>
                <!--Хлебные крошки -->
                  <?= $this->render('../common/_breadcrumbs',
                      [
                          'links' => [
                              ['label' => 'Текущая страница']
                          ]
                      ]);
                  ?>
              </div>
            </div>
          </div>
        </div>

        <!--Контент страницы-->
        <div class="row flex-row">
          <div class="col-xl-12 col-12">

              <?= $content ?>

          </div>
        </div>

      </div>
      <!--Контейнер контента-->

      <!--Скролл вверх-->
      <a href="#" class="go-top"><i class="la la-arrow-up"></i></a>

    </div>

  </main>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
