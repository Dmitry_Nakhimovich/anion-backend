<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\SecondAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

SecondAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>

  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
  <script>
      WebFont.load({
          google: {"families": ["Montserrat:300,400,500,600,700:cyrillic"]},
          active: function () {
              sessionStorage.fonts = true;
          }
      });
  </script>

  <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">

    <?php $this->head() ?>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script src="<?= Yii::getAlias('@web') ?>/assets/vendors/js/base/jquery.min.js"></script>
</head>
<body id="page-top">
<?php $this->beginBody() ?>

<div id="preloader">
  <div class="canvas">
    <div class="spinner"></div>
  </div>
</div>

<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
