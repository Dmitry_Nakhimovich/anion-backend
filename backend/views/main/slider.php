<?php

use yii\helpers\Html;
use yii\helpers\Url;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;

?>

<div class="widget has-shadow">
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <div class="row">
      <div class="col-12">
        <h3 class="mb-4">Слайдер на главной</h3>
      </div>
      <div class="col-12">
        <!--Добавить-->
        <a href="#" data-toggle="modal" data-target="#modal-create"
           class="btn btn-success ripple mr-1 mb-2">
          <i class="la la-plus edit"></i>Добавить
        </a>
        <div id="modal-create" class="modal fade">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <?= Html::beginForm(
                    ['main/slider'],
                    'post',
                    ['enctype' => 'multipart/form-data']
                ) ?>
                <?= Html::hiddenInput('main', 'true'); ?>
                <?= Html::hiddenInput('type', 1); ?>
                <?= Html::hiddenInput('create', 'true'); ?>
              <div class="modal-header">
                <h3 class="modal-title">Создание записи</h3>
                <button type="button" class="close" data-dismiss="modal">
                  <span aria-hidden="true">×</span>
                  <span class="sr-only">close</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-12 col-lg-6">
                    <div class="form-group">
                      <label>Заголовок</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                          <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="title"
                               value=''
                               class="form-control"/>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Текст</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <textarea name="text" rows="3"
                                  class="form-control"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Приоритет</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="priority"
                               value='0'
                               class="form-control" required/>
                      </div>
                    </div>
                    <div class="form-group mb-3">
                      <label>Признак публикации</label>
                      <div class="styled-checkbox">
                        <input type="checkbox" name="isActive"
                               id="check">
                        <label for="check">Активировать</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-lg-6">
                    <div class="form-group">
                      <label>Ссылка</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="link"
                               value='' required
                               class="form-control"/>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Текст на кнопке</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="name"
                               value='Заказать' required
                               class="form-control"/>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Маркер</label>
                      <div class="input-group">
                          <span class="input-group-addon addon-primary">
                              <i class="la la-pencil"></i>
                          </span>
                        <input type="text" name="tag"
                               value=''
                               class="form-control"/>
                      </div>
                    </div>
                    <div class="form-group mb-3">
                      <label>Показать маркер</label>
                      <div class="styled-checkbox">
                        <input type="checkbox" name="tag_isActive"
                               id="tag_check">
                        <label for="tag_check">Активировать</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Длительность</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                            <i class="la la-pencil"></i>
                        </span>
                        <input type="text" name="delay"
                               value=''
                               class="form-control"/>
                      </div>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Ссылка на файл</label>
                      <div class="input-group">
                        <span class="input-group-addon addon-primary">
                          <i class="la la-file"></i>
                        </span>
                          <?= InputFile::widget([
                              'language' => 'ru',
                              'filter' => ['image/jpeg', 'image/jpeg', 'image/png'],
                              'name' => 'img',
                              'options' => [
                                  'class' => 'form-control',
                                  'required' => 'required',
                              ],
                              'buttonOptions' => [
                                  'class' => 'btn btn-primary btn-square ripple'
                              ],
                              'buttonName' => 'Выбрать',
                              'value' => '',
                          ]); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary ripple"
                        data-dismiss="modal">Закрыть
                </button>
                <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                </button>
              </div>
                <?= Html::endForm() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="widget-body">
    <div class="table-responsive">
      <table id="sorting-table" class="table mb-0">
        <thead>
        <tr>
          <th>Заголовок</th>
          <th>Изображение</th>
          <th>Приоритет</th>
          <th>Признак публикации</th>
          <th>Действия</th>
        </tr>
        </thead>
        <tbody>

        <? foreach ($model as $item): ?>
          <tr>
            <td>
              <span class="text-primary">
                  <?= $item->title ?>
              </span>
            </td>
            <td>
              <img src="<?= $item->img ?: '/admin/upload/no-pic.png' ?>" alt=""
                   style="max-width: 300px; max-height: 200px">
            </td>
            <td width="120px"> <?= $item->priority ?></td>
            <td width="220px">
              <span style="width:100px;">
                  <? $status = $item->isActive ?>
                  <? if ($status): ?>
                    <span class="badge-text badge-text-small success">Активно</span>
                  <? else: ?>
                    <span class="badge-text badge-text-small danger">Отключено</span>
                  <? endif; ?>
              </span>
            </td>
            <td class="td-actions">
              <!--Редактировать-->
              <a href="#" data-toggle="modal" data-target="#modal-<?= $item->id ?>">
                <i class="la la-edit edit"></i>
              </a>
              <div id="modal-<?= $item->id ?>" class="modal fade">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                      <?= Html::beginForm(
                          ['main/slider'],
                          'post',
                          ['enctype' => 'multipart/form-data']
                      ) ?>
                      <?= Html::hiddenInput('main', 'true'); ?>
                      <?= Html::hiddenInput('id', $item->id); ?>
                    <div class="modal-header">
                      <h3 class="modal-title">Редактирование записи: <?= $item->name ?></h3>
                      <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-12 col-lg-6">
                          <div class="form-group">
                            <label>Заголовок</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="title"
                                     value='<?= $item->title ?: '' ?>'
                                     class="form-control"/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Текст</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <textarea name="text" rows="3"
                                        class="form-control"
                              ><?= $item->text ?: '' ?></textarea>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Приоритет</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="priority"
                                     value='<?= $item->priority ?>'
                                     class="form-control" required/>
                            </div>
                          </div>
                          <div class="form-group mb-3">
                            <label>Признак публикации</label>
                            <div class="styled-checkbox">
                              <input type="checkbox" name="isActive"
                                     id="check-<?= $item->id ?>"
                                  <?= $item->isActive ? 'checked' : ''; ?>>
                              <label for="check-<?= $item->id ?>">Активировать</label>
                            </div>
                          </div>
                        </div>
                        <div class="col-12 col-lg-6">
                          <div class="form-group">
                            <label>Ссылка</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="link"
                                     value='<?= $item->link ?>' required
                                     class="form-control"/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Текст на кнопке</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="name"
                                     value='<?= $item->name ?>' required
                                     class="form-control"/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Маркер</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="tag"
                                     value='<?= $item->tag ?: '' ?>'
                                     class="form-control"/>
                            </div>
                          </div>
                          <div class="form-group mb-3">
                            <label>Показать маркер</label>
                            <div class="styled-checkbox">
                              <input type="checkbox" name="tag_isActive"
                                     id="tag_check-<?= $item->id ?>"
                                  <?= $item->tag_isActive ? 'checked' : ''; ?>>
                              <label for="tag_check-<?= $item->id ?>">Активировать</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label>Длительность</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                  <i class="la la-pencil"></i>
                              </span>
                              <input type="text" name="delay"
                                     value='<?= $item->delay ?>'
                                     class="form-control"/>
                            </div>
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="form-group">
                            <label>Ссылка на файл</label>
                            <div class="input-group">
                              <span class="input-group-addon addon-primary">
                                <i class="la la-file"></i>
                              </span>
                                <?= InputFile::widget([
                                    'language' => 'ru',
                                    'filter' => ['image/jpeg', 'image/jpeg', 'image/png'],
                                    'name' => 'img',
                                    'options' => [
                                        'class' => 'form-control',
                                        'required' => 'required',
                                    ],
                                    'buttonOptions' => [
                                        'class' => 'btn btn-primary btn-square ripple'
                                    ],
                                    'buttonName' => 'Выбрать',
                                    'value' => $item->img,
                                ]); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-secondary ripple"
                              data-dismiss="modal">Закрыть
                      </button>
                      <button type="submit" class="btn btn-outline-primary ripple">Сохранить
                      </button>
                    </div>
                      <?= Html::endForm() ?>
                  </div>
                </div>
              </div>
              <!--Удалить-->
              <a href="#" data-toggle="modal" data-target="#modal-delete-<?= $item->id ?>">
                <i class="la la-close delete"></i>
              </a>
              <div id="modal-delete-<?= $item->id ?>" class="modal fade">
                <div class="modal-dialog modal-dialog-centered modal-sm">
                  <div class="modal-content">
                      <?= Html::beginForm(
                          ['main/slider'],
                          'post',
                          ['enctype' => 'multipart/form-data']
                      ) ?>
                      <?= Html::hiddenInput('main', 'true'); ?>
                      <?= Html::hiddenInput('delete', 'true'); ?>
                      <?= Html::hiddenInput('id', $item->id); ?>
                    <div class="modal-header">
                      <h3 class="modal-title">Удаление записи: <?= $item->name ?></h3>
                      <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-12">
                          <p class="text-dark mb-3">Вы уверены что хотите удалить запись?</p>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline-secondary ripple"
                              data-dismiss="modal">Отмена
                      </button>
                      <button type="submit" class="btn btn-outline-danger ripple">Удалить
                      </button>
                    </div>
                      <?= Html::endForm() ?>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        <? endforeach; ?>

        </tbody>
      </table>
    </div>
  </div>
</div>