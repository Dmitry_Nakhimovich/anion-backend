<?php

use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="widget has-shadow">
    <?= Html::beginForm(
        ['main/about'],
        'post',
        ['enctype' => 'multipart/form-data']
    ) ?>
    <?= Html::hiddenInput('main', 'true'); ?>
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <h3>О компании на главной</h3>
  </div>
  <div class="widget-body">
    <div class="row">
      <div class="col-12">
        <div class="form-group pb-3 pt-3 m-0">
          <label>Основной текст</label>
          <div class="cke-group">
              <?= CKEditor::widget([
                  'name' => 'text_main',
                  'model' => $text_main,
                  'value' => $text_main->text,
                  'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                      'preset' => 'full',
                      'inline' => false,
                  ]),
              ]); ?>
          </div>
        </div>
        <div class="form-group pb-3 pt-3 m-0">
          <label>Вспомогательный текст</label>
          <div class="cke-group">
              <?= CKEditor::widget([
                  'name' => 'text_sub',
                  'model' => $text_sub,
                  'value' => $text_sub->text,
                  'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                      'preset' => 'full',
                      'inline' => false,
                  ]),
              ]); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="widget-footer bordered">
    <a href="<?= Url::toRoute(['/main/about']) ?>"
       class="btn btn-outline-secondary ripple">Отменить
    </a>
    <button type="submit"
            class="btn btn-outline-primary ripple">Сохранить
    </button>
  </div>
    <?= Html::endForm() ?>
</div>