<?

use yii\helpers\Url;

?>

<ul class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="<?= Yii::$app->homeUrl ?>">
      <i class="ti ti-home"></i>
    </a>
  </li>
    <? foreach ($links as $link): ?>
        <? if ($link['url']): ?>
        <li class="breadcrumb-item">
          <a href="<?= Url::toRoute($link['url']); ?>"><?= $link['label'] ?></a>
        </li>
        <? else: ?>
        <li class="breadcrumb-item active"><?= $link['label'] ?></li>
        <? endif; ?>
    <? endforeach; ?>
</ul>