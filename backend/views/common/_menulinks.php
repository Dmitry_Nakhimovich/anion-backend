<?

use common\models\SectionsType;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<ul class="list-unstyled">
  <li>
      <? $item = SectionsType::findOne(1); ?>
    <a href="#dropdown-main" aria-expanded="false" data-toggle="collapse">
      <i class="la la-file-text"></i>
      <span><?= $item->name ?></span>
    </a>
    <ul id="dropdown-main" class="collapse list-unstyled pt-0">
      <li><a href="<?= Url::toRoute(['/main/slider']) ?>">Слайдер</a></li>
      <li><a href="<?= Url::toRoute(['/main/descont']) ?>">Акции</a></li>
      <li><a href="<?= Url::toRoute(['/main/products']) ?>">Продукция</a></li>
      <li><a href="<?= Url::toRoute(['/main/about']) ?>">О компании</a></li>
      <li><a href="<?= Url::toRoute(['/main/news']) ?>">Новости</a></li>
    </ul>
  </li>
  <li>
      <? $item = SectionsType::findOne(2); ?>
    <a href="<?= Url::toRoute(['/products']) ?>">
      <i class=" la la-shopping-cart"></i>
      <span><?= $item->name ?></span>
    </a>
  </li>
  <li>
      <? $item = SectionsType::findOne(3); ?>
    <a href="<?= Url::toRoute(['/materials']) ?>">
      <i class=" la la-files-o"></i>
      <span><?= $item->name ?></span>
    </a>
  </li>
  <li>
      <? $item = SectionsType::findOne(4); ?>
    <a href="<?= Url::toRoute(['/news']) ?>">
      <i class="la la-folder-open"></i>
      <span><?= $item->name ?></span>
    </a>
  </li>
</ul>

<span class="heading">Меню</span>
<ul class="list-unstyled">
  <li>
    <a href="<?= Url::toRoute(['/menu/main']) ?>">
      <i class="la la-list"></i>
      <span>Главное меню</span>
    </a>
  </li>
  <li>
    <a href="#dropdown-nav" aria-expanded="false" data-toggle="collapse">
      <i class="la la-list"></i>
      <span>Навигация</span>
    </a>
    <ul id="dropdown-nav" class="collapse list-unstyled pt-0">
      <li>
          <? $item = SectionsType::findOne(2); ?>
        <a href="<?= Url::toRoute(['/menu/products']) ?>"><?= $item->name ?></a>
      </li>
      <li>
          <? $item = SectionsType::findOne(3); ?>
        <a href="<?= Url::toRoute(['/menu/materials']) ?>"><?= $item->name ?></a>
      </li>
      <li>
          <? $item = SectionsType::findOne(5); ?>
        <a href="<?= Url::toRoute(['/menu/about']) ?>"><?= $item->name ?></a>
      </li>
      <li>
          <? $item = SectionsType::findOne(6); ?>
        <a href="<?= Url::toRoute(['/menu/contacts']) ?>"><?= $item->name ?></a>
      </li>
    </ul>
  </li>
</ul>

<span class="heading">Справочники</span>
<ul class="list-unstyled">
  <li>
    <a href="#dropdown-sidebar" aria-expanded="false" data-toggle="collapse">
      <i class="la la-list-ol"></i>
      <span>Сайдбар</span>
    </a>
    <ul id="dropdown-sidebar" class="collapse list-unstyled pt-0">
      <li><a href="<?= Url::toRoute(['/sidebar/menu']) ?>">Доп. меню</a></li>
      <li><a href="<?= Url::toRoute(['/sidebar/links']) ?>">Ссылки</a></li>
      <li><a href="<?= Url::toRoute(['/sidebar/files']) ?>">Файлы</a></li>
      <li><a href="<?= Url::toRoute(['/sidebar/info']) ?>">Текст</a></li>
      <li><a href="<?= Url::toRoute(['/sidebar/descont']) ?>">Акции</a></li>
    </ul>
  </li>
  <li>
    <a href="<?= Url::toRoute(['/stock']) ?>">
      <i class="la la-truck"></i>
      <span>Склады</span>
    </a>
  </li>
  <li>
    <a href="<?= Url::toRoute(['/settings']) ?>">
      <i class="la la-gears"></i>
      <span>Настройки</span>
    </a>
  </li>
</ul>

<span class="heading">Обратная связь</span>
<ul class="list-unstyled">
  <li>
    <a href="#dropdown-mail" aria-expanded="false" data-toggle="collapse">
      <i class="la la-envelope"></i>
      <span>Почта</span>
    </a>
    <ul id="dropdown-mail" class="collapse list-unstyled pt-0">
      <li><a href="<?= Url::toRoute(['/mail/mail']) ?>">Сообщения</a></li>
      <li><a href="<?= Url::toRoute(['/mail/cart']) ?>">Заявки</a></li>
    </ul>
  </li>
</ul>

<span class="heading">Выгрузка</span>
<ul class="list-unstyled">
  <li>
    <a href="<?= Url::toRoute(['/data']) ?>">
      <i class="la la-file-archive-o"></i>
      <span>Импорт/Экспорт</span>
    </a>
  </li>
</ul>

<ul class="list-unstyled">
  <li>
    <a href="<?= Yii::$app->urlManagerFrontend->createUrl('site/index') ?>">
      <i class="la la-home"></i>
      <span>Основной сайт</span>
    </a>
  </li>
</ul>