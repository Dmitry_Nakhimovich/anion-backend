<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="widget has-shadow">
  <div class="widget-header bordered no-actions d-flex align-items-center">
    <h3>Импорт/Экспорт продукции</h3>
  </div>
  <div class="widget-body">
    <div class="row">
      <div class="col-12 col-lg-6">
          <?= Html::beginForm(
              ['data/index'],
              'post',
              ['enctype' => 'multipart/form-data']
          ) ?>
          <?= Html::hiddenInput('export', 'true'); ?>
        <div class="form-group row align-items-lg-center">
          <div class="col-12 col-lg-auto">
            <div class="text-primary">Экспортировать продукцию:</div>
          </div>
          <div class="col-12 col-lg-auto">
            <button type="submit"
                    class="btn btn-outline-primary ripple">Экспорт
            </button>
          </div>
        </div>
          <?= Html::endForm() ?>
      </div>
      <div class="col-lg-6"></div>
      <div class="col-12 col-lg-6">
          <?= Html::beginForm(
              ['data/index'],
              'post',
              ['enctype' => 'multipart/form-data']
          ) ?>
          <?= Html::hiddenInput('import', 'true'); ?>
        <div class="form-group row align-items-lg-end">
          <div class="col-12 col-lg">
            <label>Импорт продукции (.xlsx формат файла)</label>
            <div class="input-group">
              <span class="input-group-addon addon-primary">
                  <i class="la la-file"></i>
              </span>
              <input type="file" name="spreadsheet"
                     class="form-control" required/>
            </div>
          </div>
          <div class="col-12 col-lg-auto">
            <button type="submit"
                    class="btn btn-outline-primary ripple">Импорт
            </button>
          </div>
        </div>
          <?= Html::endForm() ?>
      </div>
    </div>
  </div>
</div>