<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\console\Exception;
use yii\helpers\Console;
use common\models\AttrContent;
use common\models\NavMenu;
use common\models\Products;
use common\models\ProductsAttr;
use common\models\ProductsImg;
use common\models\Sections;
use common\models\Stock;
use common\models\StockContent;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Writer_Excel2007;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

class DataController extends Controller
{

    public function actionImport($file)
    {
        require_once(Yii::getAlias('@vendor/phpexcel/PHPExcel.php'));

        // load file
        $inputFile = $file;
        $inputFileType = PHPExcel_IOFactory::identify($inputFile);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFile);

        //Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        //Loop through each row of the worksheet in turn
        $offset_main = 7;
        $rowData = [];
        for ($row = 3; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
        }
        $offset_attr = $offset_main + count(Stock::find()->orderBy('priority')->all());
        $offset_all = $offset_attr + count(ProductsAttr::find()->orderBy('sections_id')->all());
        foreach ($rowData as $item) {
            $product = Products::findOne($item[0][0]);
            if (!empty($product)) {
                $section = Sections::find()->where(['id_type' => 1])
                    ->andWhere(['id_item' => $product->id])->one();
                $navMenu = NavMenu::findOne($product->nav_menu_id);
                $product->title = (string)$item[0][1];
                $product->article = (string)$item[0][4];
                $product->price = (float)$item[0][7] ?: 0;
                $product->img = (string)$item[0][5] ? "/admin/upload/" . (string)$item[0][5] : '';
                $arr_imgs = ProductsImg::find()->where(['product_id' => $product->id])->all();
                foreach ($arr_imgs as $pic) $pic->delete();
                $arr_imgs = explode(",", $item[0][6]);
                foreach ($arr_imgs as $j => $pic) {
                    $cimg = new ProductsImg();
                    $cimg->product_id = $product->id;
                    $cimg->img = "/admin/upload/" . (string)$pic;
                    $cimg->priority = $j;
                    $cimg->save();
                }
                for ($i = $offset_attr; $i < $offset_all; $i++) {
                    $attr = ProductsAttr::find()
                        ->where(['name' => $sheet->getCellByColumnAndRow($i, 2)])
                        ->andWhere(['sections_id' => $navMenu->external_id])->one();
                    $attrCnt = AttrContent::find()->where(['attr_id' => $attr->id])
                        ->andWhere(['sections_id' => $section->id])->one();
                    if (isset($attrCnt) && isset($attr)) {
                        if ($attr->type == 'num') {
                            $attrCnt->text = (string)$item[0][$i];
                            $attrCnt->num = (float)$item[0][$i];
                        } else {
                            $attrCnt->text = $item[0][$i];
                            $attrCnt->num = 0;
                        }
                        $attrCnt->save();
                    } else {
                        $attrCnt = new AttrContent();
                        $attrCnt->attr_id = $attr->id;
                        $attrCnt->sections_id = $section->id;
                        if ($attr->type == 'num') {
                            $attrCnt->text = (string)$item[0][$i];
                            $attrCnt->num = (float)$item[0][$i];
                        } else {
                            $attrCnt->text = $item[0][$i];
                            $attrCnt->num = 0;
                        }
                        $attrCnt->save();
                    }
                }
                for ($i = $offset_main; $i < $offset_attr; $i++) {
                    $attr = Stock::find()
                        ->where(['name' => $sheet->getCellByColumnAndRow($i, 2)])
                        ->one();
                    $attrCnt = StockContent::find()->where(['stock_id' => $attr->id])
                        ->andWhere(['product_id' => $product->id])->one();
                    if (isset($attrCnt) && isset($attr)) {
                        $attrCnt->price = (float)$item[0][$i];
                        $attrCnt->save();
                    } else {
                        $cattrCnt = new StockContent();
                        $cattrCnt->stock_id = $attr->id;
                        $cattrCnt->product_id = $product->id;
                        $cattrCnt->price = (float)$item[0][$i] ? (float)$item[0][$i] : 0;
                        $cattrCnt->save();
                    }
                }
                $product->save();
            } else {
                $navMenu = NavMenu::findOne($item[0][2]);
                $product = new Products();
                $product->nav_menu_id = $navMenu->id;
                $product->title = (string)$item[0][1];
                $product->article = (string)$item[0][4];
                $product->price = (float)$item[0][7] ?: 0;
                $product->img = (string)$item[0][5] ? "/admin/upload/" . (string)$item[0][5] : '';
                $product->date = date('Y-m-d');
                $product->isActive = 1;
                $product->isComplect = 0;
                $product->save();
                $arr_imgs = explode(",", $item[0][6]);
                foreach ($arr_imgs as $j => $pic) {
                    $cimg = new ProductsImg();
                    $cimg->product_id = $product->id;
                    $cimg->img = "/admin/upload/" . (string)$pic;
                    $cimg->priority = $j;
                    $cimg->save();
                }
                $section = new Sections();
                $section->id_type = 1;
                $section->id_item = $product->id;
                $section->isActive = 1;
                $section->save();
                for ($i = $offset_attr; $i < $offset_all; $i++) {
                    $attr = ProductsAttr::find()
                        ->where(['name' => $sheet->getCellByColumnAndRow($i, 2)])
                        ->andWhere(['sections_id' => $navMenu->external_id])->one();
                    $attrCnt = new AttrContent();
                    $attrCnt->attr_id = $attr->id;
                    $attrCnt->sections_id = $section->id;
                    if ($attr->type == 'num') {
                        $attrCnt->text = (string)$item[0][$i];
                        $attrCnt->num = (float)$item[0][$i];
                    } else {
                        $attrCnt->text = $item[0][$i];
                        $attrCnt->num = 0;
                    }
                    $attrCnt->save();
                }
                for ($i = $offset_main; $i < $offset_attr; $i++) {
                    $attr = Stock::find()
                        ->where(['name' => $sheet->getCellByColumnAndRow($i, 2)])
                        ->one();
                    $cattrCnt = new StockContent();
                    $cattrCnt->stock_id = $attr->id;
                    $cattrCnt->product_id = $product->id;
                    $cattrCnt->price = (float)$item[0][$i] ? (float)$item[0][$i] : 0;
                    $cattrCnt->save();
                }
            }
        }
        //var_dump('<pre>', $rowData);die;

        unlink($file);
    }

}