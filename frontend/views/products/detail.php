<?php

use common\models\AttrContent;
use common\models\FiltersContent;
use common\models\NavMenu;
use common\models\ProductsAttr;
use common\models\ProductsImg;
use common\models\Sections;
use common\models\Stock;
use common\models\StockContent;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<main class="page-product_card">
  <div class="container">
    <div class="row">
      <!--Крошки-->
      <div class="col-12">
          <?= $this->render('../common/_breadcrumbs',
              [
                  'links' => [
                      ['label' => 'Главная', 'url' => '/'],
                      ['label' => $navMenu->name, 'url' => '/products/category', 'id' => Sections::findOne($navMenu->external_id)->link],
                      ['label' => $product->title]
                  ]
              ]);
          ?>
      </div>
      <!--Сайдбар-->
      <div class="col-xl-3">
          <? $csidebar = Sections::findOne($navMenu->external_id) ?>
          <? $csidebar = ($csidebar->id_item == 0) ? $csidebar : $section; ?>
          <?= $this->render('../common/_sidebar', [
              'section' => $csidebar,
              'navMenu' => $navMenu,
          ]);
          ?>
      </div>
      <!--Контент-->
      <div class="col-xl-9">
        <section class="product-block">
          <div class="row">
              <? $sliderImgs = ProductsImg::find()->where(['product_id' => $product->id])->all() ?>
              <? if ($sliderImgs): ?>
                <div class="col-lg-5">
                  <div class="slider-wrap">
                    <div class="slider-top swiper-container gallery-top">
                      <div class="swiper-wrapper">
                          <? foreach ($sliderImgs as $sliderImg): ?>
                            <div class="swiper-slide">
                              <img class="swiper-img-full" alt=""
                                   src="<?= $sliderImg->img ?>">
                            </div>
                          <? endforeach; ?>
                      </div>
                    </div>
                    <div class="slider-thumbs swiper-container gallery-thumbs">
                      <div class="swiper-wrapper">
                          <? foreach ($sliderImgs as $sliderImg): ?>
                            <div class="swiper-slide">
                              <img class="swiper-img" alt=""
                                   src="<?= $sliderImg->img ?>">
                            </div>
                          <? endforeach; ?>
                      </div>
                    </div>
                  </div>
                </div>
              <? endif; ?>
            <div class="col-lg-7">
              <div class="desc-wrap">
                <div class="row">
                  <div class="col-12">
                    <div class="item-id"><?= $product->article ?></div>
                  </div>
                  <div class="col-12">
                    <div class="item-title"><?= $product->title ?></div>
                  </div>
                  <div class="col-lg-7">
                    <div class="item-info">
                      <div class="table-responsive">
                        <table class="table">
                          <tbody>
                          <? $productAttr = ProductsAttr::find()->where(['sections_id' => $navMenu->external_id])
                              ->andWhere(['isActive' => 1])->orderBy('priority')->all() ?>
                          <? foreach ($productAttr as $a): ?>
                              <? $productAttrContent = AttrContent::find()->where(['sections_id' => $section->id])
                                  ->andWhere(['attr_id' => $a->id])
                                  ->one() ?>
                            <tr>
                              <td><?= $a->name ?></td>
                              <td>
                                  <? if ($a->type == 'num')
                                      echo $productAttrContent->num ?: 0;
                                  else
                                      echo $productAttrContent->text ?: '-';
                                  ?>
                              </td>
                            </tr>
                          <? endforeach; ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-5">

                    <div class="item-cost">
                        <? $cstock = Stock::find()->where(['code' => $_SESSION['stock']])->one(); ?>
                        <? $cprice = StockContent::find()->where(['product_id' => $product->id])
                            ->andWhere(['stock_id' => $cstock->id])->one() ?>
                      <div class="price"><?= $cprice->price ?></div>
                      <div class="action">
                        <button class="btn orange" id="btn-cart-add"
                                data-product_id="<?= $product->id ?>">В КОРЗИНУ
                        </button>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <div class="col-12">
              <div class="product-text">
                  <?= $product->text ?>
              </div>
            </div>
          </div>
        </section>

          <? if (!empty($complects) && $navMenu->isComplect == 1): ?>
            <section class="other-block">
              <div class="row">
                <div class="col-12">
                  <div class="block-title">Комплектующие</div>
                </div>
                <div class="col-12">
                  <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <? foreach ($complects as $complect): ?>
                            <? $cprice = StockContent::find()->where(['product_id' => $complect->id])
                                ->andWhere(['stock_id' => $cstock->id])->one() ?>
                            <? if ($complect->img): ?>
                            <div class="swiper-slide">
                              <div class="card-wrap">
                                <a class="card-link"
                                   href="<?= Url::toRoute(['/products/detail', 'id' => $complect->id]) ?>"></a>
                                <div class="card-pic img-wrap">
                                  <img src="<?= $complect->img ?>">
                                </div>
                                <div class="card-text">
                                  <div class="card-id"><?= $complect->article ?></div>
                                  <div class="card-title dot-text"><?= $complect->title ?></div>
                                  <div class="card-price"><?= $cprice->price ?></div>
                                </div>
                              </div>
                            </div>
                            <? else: ?>
                            <div class="swiper-slide">
                              <div class="card-wrap no-pic">
                                <a class="card-link"
                                   href="<?= Url::toRoute(['/products/detail', 'id' => $complect->id]) ?>"></a>
                                <div class="card-text">
                                  <div class="card-id"><?= $complect->article ?></div>
                                  <div class="card-title dot-long"><?= $complect->title ?></div>
                                  <div class="card-price"><?= $cprice->price ?></div>
                                </div>
                              </div>
                            </div>
                            <? endif; ?>
                        <? endforeach; ?>
                    </div>
                  </div>
                  <div class="swiper-navigation">
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                  </div>
                </div>
              </div>
            </section>
          <? endif; ?>
      </div>

    </div>
  </div>
</main>