<?php

use common\models\FiltersContent;
use common\models\NavMenu;
use common\models\NavMenuImg;
use common\models\Stock;
use common\models\StockContent;
use common\models\Sections;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<main class="page-product_list">
  <div class="container">
    <div class="row">
      <!--Крошки-->
      <div class="col-12">
          <?
          $breadcrumbs = [];
          $breadcrumbs['links'] = [];
          $breadcrumbs['links'][] = ['label' => $navMenu->name];
          $breadNav = $navMenu;
          if ($navMenu->parent_id)
              while ($breadNav->parent_id != 0) {
                  $breadNav = NavMenu::findOne($breadNav->parent_id);
                  $breadcrumbs['links'][] = ['label' => $breadNav->name, 'url' => '/products/category', 'id' => Sections::findOne($breadNav->external_id)->link];
              }
          $breadcrumbs['links'][] = ['label' => 'Главная', 'url' => '/'];
          $breadcrumbs['links'] = array_reverse($breadcrumbs['links']);
          ?>
          <?= $this->render('../common/_breadcrumbs', $breadcrumbs);
          ?>
      </div>
      <!--Сайдбар-->
      <div class="col-xl-3">
          <?= $this->render('../common/_sidebar', [
              'section' => $section,
              'navMenu' => $navMenu,
          ]);
          ?>
      </div>

      <div class="col-xl">
        <!--Фильры продукции-->
        <section class="filters-block">
          <div class="row">
            <div class="col-12">
              <div class="block-title-wrap">
                <h1 class="block-title"><?= $navMenu->name ?></h1>
              </div>
            </div>
              <? if ($filters && !$isPodcat): ?>
                <div class="col-12">
                    <?= Html::beginForm(
                        ['products/category', 'url' => $section->link],
                        'get',
                        ['enctype' => 'multipart/form-data']
                    ) ?>
                    <?= Html::hiddenInput('filters', 'true'); ?>
                  <div class="filters-wrap">
                    <div class="row">
                        <? foreach ($filters as $filter): ?>
                            <? if ($filter->type == 'num'): ?>
                            <!-- От-До-->
                            <div class="col-lg-4">
                              <div class="row">
                                <div class="col-12">
                                  <div class="filter-title"><?= $filter->name ?></div>
                                </div>
                                <div class="col-md-6">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">От</div>
                                    </div>
                                    <input class="form-control" type="number" step="1"
                                           name="filter_<?= $filter->id ?>[from]">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">До</div>
                                    </div>
                                    <input class="form-control" type="number" step="1"
                                           name="filter_<?= $filter->id ?>[to]">
                                  </div>
                                </div>
                              </div>
                            </div>
                            <? elseif ($filter->type == 'select'): ?>
                            <!-- Селект-->
                            <div class="col-lg-4">
                              <div class="row">
                                <div class="col-12">
                                  <div class="filter-title"><?= $filter->name ?></div>
                                </div>
                                <div class="col-12">
                                  <div class="form-group">
                                    <select class="selectpicker show-tick form-control" title="" data-size="5"
                                            name="filter_<?= $filter->id ?>">
                                      <option selected disabled hidden value=""></option>
                                        <? $fitlerLsit = FiltersContent::find()->where(['attr_id' => $filter->id])->all() ?>
                                        <? foreach ($fitlerLsit as $f): ?>
                                            <? if ($f->text == 'Нет'): ?>
                                            <option value=""><?= $f->text ?></option>
                                            <? else: ?>
                                            <option value="<?= $f->text ?>"><?= $f->text ?></option>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <? elseif ($filter->type == 'text'): ?>
                            <!-- Текст-->
                            <div class="col-lg-4">
                              <div class="row">
                                <div class="col-12">
                                  <div class="filter-title"><?= $filter->name ?></div>
                                </div>
                                <div class="col-12">
                                  <div class="input-group">
                                    <input class="form-control" type="text"
                                           name="filter_<?= $filter->id ?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                            <? endif; ?>
                        <? endforeach; ?>
                    </div>
                  </div>
                  <div class="filters-submit">
                    <div class="hide-btn">Свернуть фильтры</div>
                    <button type="submit" class="btn blue submit-btn">Применить фильтры</button>
                  </div>
                    <?= Html::endForm() ?>
                </div>
              <? endif; ?>
          </div>
        </section>
        <!--Карточки товаров-->
          <? if (!$isPodcat): ?>
            <section class="cards-block">
              <div class="row">
                  <? $cstock = Stock::find()->where(['code' => $_SESSION['stock']])->one(); ?>
                  <? foreach ($products as $product): ?>
                      <? $cprice = StockContent::find()->where(['product_id' => $product->id])
                          ->andWhere(['stock_id' => $cstock->id])->one() ?>
                      <? if ($product->img): ?>
                      <div class="col-lg-4">
                        <div class="card-wrap">
                          <a class="card-link"
                             href="<?= Url::toRoute(['/products/detail', 'id' => $product->id]) ?>"></a>
                          <div class="card-pic img-wrap">
                            <img src="<?= $product->img ?>">
                          </div>
                          <div class="card-text">
                            <div class="card-id"><?= $product->article ?></div>
                            <div class="card-title dot-text">
                                <?= $product->title ?>
                            </div>
                            <div class="card-price"><?= $cprice->price ?></div>
                          </div>
                        </div>
                      </div>
                      <? else: ?>
                      <div class="col-lg-4">
                        <div class="card-wrap no-pic">
                          <a class="card-link"
                             href="<?= Url::toRoute(['/products/detail', 'id' => $product->id]) ?>"></a>
                          <div class="card-text">
                            <div class="card-id"><?= $product->article ?></div>
                            <div class="card-title dot-long"><?= $product->title ?></div>
                            <div class="card-price"><?= $cprice->price ?></div>
                          </div>
                        </div>
                      </div>
                      <? endif; ?>
                  <? endforeach; ?>
                <!--Пагинация-->
                <div class="col-12">
                    <?= $this->render('../common/_pagination', [
                        'pagination' => $pagination
                    ]); ?>
                </div>
              </div>
            </section>
          <? else: ?>
            <section class="cards-block">
              <div class="row">
                  <? foreach ($podcat as $product): ?>
                    <div class="col-lg-4">
                      <div class="card-wrap no-pic">
                        <a class="card-link"
                           href="<?= Url::toRoute([
                               '/products/category',
                               'url' => Sections::findOne($product->external_id)->link,
                           ]) ?>"></a>
                          <? $cimg = NavMenuImg::find()->where(['product_id' => $product->id])->one(); ?>
                          <? if ($cimg): ?>
                            <div class="card-pic img-wrap">
                              <img src="<?= $cimg->img ?>">
                            </div>
                          <? endif; ?>
                        <div class="card-text">
                          <div class="card-id"><?= $product->tag ?></div>
                          <div class="card-title dot-long"><?= $product->name ?></div>
                        </div>
                      </div>
                    </div>
                  <? endforeach; ?>
                <!--Пагинация-->
                <div class="col-12">
                    <?= $this->render('../common/_pagination', [
                        'pagination' => $pagination
                    ]); ?>
                </div>
              </div>
            </section>
          <? endif; ?>
      </div>
    </div>
  </div>
</main>