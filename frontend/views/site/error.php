<?php

use yii\helpers\Html;

$this->title = $name;
?>

<main class="page-error site-error">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="mt-3"><?= Html::encode($this->title) ?></h1>

        <div class="alert alert-danger d-inline-block">
            <?= nl2br(Html::encode($message)) ?>
        </div>

        <p>
          Пожалуйста, перезагрузите страницу.
        </p>
        <p>
          Если ошибка повторяется, сообщите администратору сайта.
        </p>
      </div>
    </div>
  </div>
</main>

