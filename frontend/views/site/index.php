<?php

use common\models\NavMenu;
use common\models\Products;
use common\models\Sections;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\News;

?>

<main class="page-index">

  <section class="slider-block">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="slider-wrap">
            <div class="swiper-container">
              <div class="swiper-wrapper">
                  <? foreach ($slider as $item): ?>
                    <div class="swiper-slide" data-swiper-autoplay="<?= $item->delay ?>">
                      <div class="slide-pic">
                        <div class="img-wrap">
                          <img class="img" src="<?= $item->img ?>">
                        </div>
                      </div>
                      <div class="slide-text">
                          <? if ($item->tag_isActive): ?>
                            <div class="slide-new">
                              <span><?= $item->tag ?></span>
                            </div>
                          <? endif; ?>
                        <div class="slide-title dot-text">
                            <?= $item->title ?>
                        </div>
                        <div class="slide-subtitle dot-long">
                            <?= $item->text ?>
                        </div>
                        <div class="slide-btn">
                          <a class="btn blue" href="<?= $item->link ?>"><?= $item->name ?></a>
                        </div>
                      </div>
                    </div>
                  <? endforeach; ?>
              </div>
              <div class="swiper-pagination"></div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="discont-wrap">
            <div class="swiper-container">
              <div class="swiper-wrapper">
                  <? foreach ($descont as $item): ?>
                    <div class="swiper-slide" data-swiper-autoplay="<?= $item->delay ?>">
                      <a class="disc-link" href="<?= $item->link ?>"></a>
                      <div class="disc-text">
                        <div class="disc-title">Акция</div>
                        <div class="disc-subtitle mb-2"><?= $item->title ?></div>
                        <div class="disc-text small p-0"><?= $item->text ?></div>
                      </div>
                      <div class="disc-pic img-wrap">
                        <img src="<?= $item->img ?>">
                      </div>
                    </div>
                  <? endforeach; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="product-block">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h1 class="block-title">Продукция</h1>
        </div>
        <div class="col-12">
          <div class="swiper-container">
            <div class="swiper-wrapper">
                <? foreach ($products as $item): ?>
                    <? $cproduct = NavMenu::findOne($item->external_id) ?>
                    <? $cprodSection = Sections::findOne($cproduct->external_id) ?>
                  <div class="swiper-slide">
                    <a class="product-link"
                       href="<?= Url::toRoute(['/products/category', 'url' => $cprodSection->link]) ?>"></a>
                    <div class="product-icon img-wrap">
                      <img src="<?= $item->img ?>">
                    </div>
                    <div class="product-text dot-text">
                        <?= $cproduct->name ?>
                    </div>
                    <div class="product-next">
                      <div class="link-icon"><i class="mdi mdi-arrow-right"></i>
                      </div>
                    </div>
                  </div>
                <? endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="about-block">
    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <div class="long-wrap">
            <div class="block-title">О компании</div>
            <div class="info-wrap">
              <div class="info-text">
                  <?= $about[1]->text ?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-8">
          <div class="about-text">
              <?= $about[0]->text ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="cards-block">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="block-title-wrap">
            <h1 class="block-title">Новости</h1>
            <a class="block-link nav-link"
               href="<?= Url::toRoute(['/news']) ?>">Все новости</a>
          </div>
        </div>
          <? foreach ($news as $item): ?>
              <? $cnews = News::findOne($item->external_id) ?>
              <? if ($cnews->img): ?>
              <div class="col-lg-4">
                <div class="card-wrap">
                  <a class="card-link"
                     href="<?= Url::toRoute([
                         '/news/detail',
                         'url' => Sections::find()->where(['id_type' => 3])
                             ->andWhere(['id_item' => $item->external_id])
                             ->one()->link,
                     ]) ?>"></a>
                  <div class="card-pic img-wrap">
                    <img src="<?= $cnews->img ?>">
                  </div>
                  <div class="card-text">
                    <div class="card-date">
                      <i class="mdi mdi-calendar-blank"></i>
                        <?= Yii::$app->formatter->asDate($cnews->date) ?>
                    </div>
                    <div class="card-desc dot-text">
                        <?= $cnews->title ?>
                    </div>
                  </div>
                </div>
              </div>
              <? else: ?>
              <div class="col-lg-4">
                <div class="card-wrap no-pic">
                  <a class="card-link"
                     href="<?= Url::toRoute([
                         '/news/detail',
                         'url' => Sections::find()->where(['id_type' => 3])
                             ->andWhere(['id_item' => $item->external_id])
                             ->one()->link,
                     ]) ?>"></a>
                  <div class="card-text">
                    <div class="card-date">
                      <i class="mdi mdi-calendar-blank"></i>
                        <?= Yii::$app->formatter->asDate($cnews->date) ?>
                    </div>
                    <div class="card-desc dot-long">
                        <?= $cnews->title ?>
                    </div>
                  </div>
                </div>
              </div>
              <? endif; ?>
          <? endforeach; ?>
      </div>
    </div>
  </section>

</main>