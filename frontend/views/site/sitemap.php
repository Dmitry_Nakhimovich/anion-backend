<?php

use common\models\NavMenu;
use common\models\Sections;
use common\models\SectionsType;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<main class="page-sitemap">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <section class="sitemap-block">
          <h1 class="mb-3">Карта сайта:</h1>
            <? $menu = SectionsType::find()->where(['isActive' => 1])->orderBy('priority')->all() ?>
            <? if ($menu): ?>
              <ul>
                <li>
                    <a href="<?= Url::toRoute(['/site/index']) ?>">Главная страница</a>
                </li>
                <? foreach ($menu as $cmenu): ?>
                    <? if ($cmenu->id == 2): ?>
                        <li>
                            <a><?= $cmenu->name ?></a>
                            <ul>
                                <? function viewMap($arr, $parent_id = 0)
                                {
                                    //Условия выхода из рекурсии
                                    if (empty($arr[$parent_id])) {
                                        return;
                                    }

                                    //перебираем в цикле массив и выводим на экран
                                    foreach ($arr[$parent_id] as $arr_item) {
                                        $podcat_item = NavMenu::find()->where(['parent_id' => $arr_item->id])
                                            ->orderBy('priority')
                                            ->all();
                                        if (empty($podcat_item)) {
                                            echo '<li><a href="'
                                                . Url::toRoute([
                                                    '/products/category',
                                                    'url' => Sections::findOne($arr_item->external_id)->link,
                                                ])
                                                . '">' . $arr_item->name . '</a></li>';
                                        } else {
                                            echo '<li>';
                                            echo '<a href="'
                                                . Url::toRoute([
                                                    '/products/category',
                                                    'url' => Sections::findOne($arr_item->external_id)->link,
                                                ])
                                                . '">' . $arr_item->name . '</a>';
                                            //рекурсия - проверяем нет ли дочерних категорий
                                            echo '<ul>';
                                            viewMap($arr, $arr_item->id);
                                            echo '</ul>';
                                            echo '</li>';
                                        }
                                    }
                                } ?>
                                <? viewMap($menuList); ?>
                            </ul>
                        </li>
                    <? elseif ($cmenu->id == 3): ?>
                        <li>
                            <a>Материалы</a>
                            <ul>
                                <? function viewMat($arr, $parent_id = 0)
                                {
                                    //Условия выхода из рекурсии
                                    if (empty($arr[$parent_id])) {
                                        return;
                                    }

                                    //перебираем в цикле массив и выводим на экран
                                    foreach ($arr[$parent_id] as $arr_item) {
                                        $podcat_item = NavMenu::find()->where(['parent_id' => $arr_item->id])
                                            ->orderBy('priority')
                                            ->all();
                                        if (empty($podcat_item)) {
                                            echo '<li><a href="'
                                                . Url::toRoute([
                                                    '/materials/detail',
                                                    'url' => Sections::find()->where(['id_type' => 2])
                                                        ->andWhere(['id_item' => $arr_item->external_id])
                                                        ->one()->link
                                                ])
                                                . '">' . $arr_item->name . '</a></li>';
                                        } else {
                                            echo '<li>';
                                            echo '<a href="'
                                                . Url::toRoute([
                                                    '/materials/detail',
                                                    'url' => Sections::find()->where(['id_type' => 2])
                                                        ->andWhere(['id_item' => $arr_item->external_id])
                                                        ->one()->link
                                                ])
                                                . '">' . $arr_item->name . '</a>';
                                            //рекурсия - проверяем нет ли дочерних категорий
                                            echo '<ul>';
                                            viewMat($arr, $arr_item->id);
                                            echo '</ul>';
                                            echo '</li>';
                                        }
                                    }
                                } ?>
                                <? viewMat($materialsList); ?>
                            </ul>
                        </li>
                    <? elseif ($cmenu->id == 4): ?>
                        <li>
                        <a href="<?= Url::toRoute(['/news']) ?>"><?= $cmenu->name ?></a>
                        </li>
                    <? elseif ($cmenu->id == 5): ?>
                        <li>
                            <? $item = Sections::findOne(5) ?>
                            <a href="<?= Url::toRoute([
                                '/materials/detail',
                                'url' => Sections::find()->where(['id_type' => 2])
                                    ->andWhere(['id_item' => $item->id_item])
                                    ->one()->link,
                            ]) ?>"><?= $cmenu->name ?></a>
                        </li>
                    <? elseif ($cmenu->id == 6): ?>
                        <li>
                            <? $item = Sections::findOne(6) ?>
                            <a href="<?= Url::toRoute([
                                '/materials/detail',
                                'url' => Sections::find()->where(['id_type' => 2])
                                    ->andWhere(['id_item' => $item->id_item])
                                    ->one()->link,
                            ]) ?>"><?= $cmenu->name ?></a>
                        </li>
                    <? endif; ?>
                <? endforeach; ?>
              </ul>
            <? endif; ?>
        </section>
      </div>
    </div>
  </div>
</main>