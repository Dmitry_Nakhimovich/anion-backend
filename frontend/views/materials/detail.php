<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<main class="page-news_detail">
  <div class="container">
    <div class="row">
      <div class="col-12">
          <?= $this->render('../common/_breadcrumbs',
              [
                  'links' => [
                      ['label' => 'Главная', 'url' => '/'],
                      ['label' => 'Материалы', 'url' => ''],
                      ['label' => $materials->title],
                  ]
              ]);
          ?>
      </div>

        <? if ($section->menu_isActive ||
            $section->links_isActive ||
            $section->files_isActive ||
            $section->info_isActive ||
            $section->descont_isActive): ?>
          <div class="col-xl-3">
              <?= $this->render('../common/_sidebar', [
                  'section' => $section,
              ]);
              ?>
          </div>
        <? endif; ?>

      <div class="col-xl">
        <section class="news-block">
          <div class="row">
            <div class="col-12">
              <div class="block-title-wrap">
                <h1 class="block-title"><?= $materials->title ?></h1>
              </div>
            </div>
            <div class="col-12">
              <div class="news-date">
                <i class="mdi mdi-calendar-blank"></i>
                  <?= Yii::$app->formatter->asDate($materials->date) ?>
              </div>
            </div>
            <div class="col-12">
              <div class="news-pic img-wrap">
                <img src="<?= $materials->img ?>">
              </div>
            </div>
            <div class="col-12">
              <div class="news-text">
                  <?= $materials->text ?>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
</main>
