<?php

use yii\widgets\LinkPager;

?>

<nav class="pagination-wrap">
    <?= LinkPager::widget([
        'pagination' => $pagination,
        'pageCssClass' => 'page-item',
        'linkOptions' => ['class' => 'page-link'],
        'firstPageLabel' => '<i class="mdi mdi-chevron-double-left"></i>',
        'firstPageCssClass' => 'page-item',
        'lastPageLabel' => '<i class="mdi mdi-chevron-double-right"></i>',
        'lastPageCssClass' => 'page-item',
        'nextPageLabel' => '<i class="mdi mdi-chevron-right"></i>',
        'nextPageCssClass' => 'page-item',
        'prevPageLabel' => '<i class="mdi mdi-chevron-left"></i>',
        'prevPageCssClass' => 'page-item',
    ]);
    ?>
</nav>
