<?php

use yii\helpers\Url;

?>

<nav class="breadcrumb-wrap">
  <ol class="breadcrumb">
      <? foreach ($links as $link): ?>
          <? if ($link['url'] && !$link['id']): ?>
          <li class="breadcrumb-item">
            <a class="breadcrumb-link"
               href="<?= Url::toRoute($link['url']); ?>"><?= $link['label'] ?></a>
          </li>
          <? elseif ($link['url'] && $link['id']): ?>
          <li class="breadcrumb-item">
            <a class="breadcrumb-link"
               href="<?= Url::toRoute([$link['url'], 'url' => $link['id']]); ?>"><?= $link['label'] ?></a>
          </li>
          <? else: ?>
          <li class="breadcrumb-item active"><?= $link['label'] ?></li>
          <? endif; ?>
      <? endforeach; ?>
  </ol>
</nav>