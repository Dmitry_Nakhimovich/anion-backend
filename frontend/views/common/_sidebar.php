<?php

use common\models\Descont;
use common\models\DopMenu;
use common\models\DopMenuContent;
use common\models\Files;
use common\models\Links;
use common\models\Info;
use common\models\NavMenu;
use common\models\Sections;
use common\models\SectionsDescont;
use common\models\SectionsFiles;
use common\models\SectionsInfo;
use common\models\SectionsLinks;
use common\models\SectionsMenu;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<aside class="sidebar">
  <div class="btn-sidebar"></div>
  <div class="sidebar-inner">
    <div class="row">
        <?
        $arr = [
            'links' => ($section->links_isActive) ? ($section->priority_links) : -1,
            'files' => ($section->files_isActive) ? ($section->priority_files) : -1,
            'info' => ($section->info_isActive) ? $section->priority_info : -1,
            'descont' => ($section->descont_isActive) ? $section->priority_descont : -1,
        ];
        asort($arr);
        ?>

        <? if ($navMenu): ?>
            <? if ($navMenu->parent_id > 0) {
                $navList = NavMenu::find()->where(['parent_id' => $navMenu->parent_id])
                    ->andWhere(['menu_type' => 1])
                    ->andWhere(['isActive' => 1])
                    ->orderBy('priority')
                    ->all();
                $navListHeader = NavMenu::findOne($navMenu->parent_id);
            } else {
                $navList = NavMenu::find()->andWhere(['menu_type' => 1])
                    ->andWhere(['isActive' => 1])
                    ->andWhere(['menu_level' => 1])
                    ->orderBy('priority')
                    ->all();
            }
            ?>
          <div class="col-12">
            <nav class="side-links-wrap">
              <ul class="side-links">
                <li class="side-item active">
                  <a class="side-link"
                     href="<?= $navListHeader->external_id ?
                         Url::toRoute(['/products/category', 'url' => Sections::findOne($navListHeader->external_id)->link])
                         : '#' ?>">
                      <? if ($navListHeader->tag_isActive): ?>
                        <span class="new-item"><?= $navListHeader->tag ?></span>
                      <? endif; ?>
                      <?= $navListHeader->name ?: 'Категории продукции' ?>
                  </a>
                </li>
                  <? foreach ($navList as $mc): ?>
                    <li class="side-item <?= $mc->id == $navMenu->id ? 'active' : '' ?>">
                      <a class="side-link" href="<?= /*$mc->link*/
                      Url::toRoute(['/products/category', 'url' => Sections::findOne($mc->external_id)->link]) ?>">
                          <? if ($mc->tag_isActive): ?>
                            <span class="new-item"><?= $mc->tag ?></span>
                          <? endif; ?>
                          <?= $mc->name ?>
                      </a>
                    </li>
                  <? endforeach; ?>
              </ul>
            </nav>
          </div>
        <? endif; ?>
        <? if ($section->menu_isActive && $section->id_menu > 0): ?>
            <? $menuSections = SectionsMenu::findOne($section->id_menu) ?>
            <? $menu = DopMenu::findOne($menuSections->id_menu) ?>
            <? $menuContent = DopMenuContent::find()->where(['id_dop_menu' => $menu->id])->andWhere(['isActive' => 1])->orderBy(['priority' => SORT_ASC])->all() ?>
          <div class="col-12">
            <nav class="side-links-wrap">
              <ul class="side-links">
                <li class="side-item active">
                  <a class="side-link" href="<?= $menu->link ?: '#' ?>"><?= $menu->name ?></a>
                </li>
                  <? foreach ($menuContent as $mc): ?>
                    <li class="side-item">
                      <a class="side-link" href="<?= $mc->link ?>">
                          <? if ($mc->tag_isActive): ?>
                            <span class="new-item"><?= $mc->tag ?></span>
                          <? endif; ?>
                          <?= $mc->name ?>
                      </a>
                    </li>
                  <? endforeach; ?>
              </ul>
            </nav>
          </div>
        <? endif; ?>
        <? foreach ($arr as $k => $l): ?>
            <? if ($k == 'links' && $l > 0): ?>
                <? $cSections = SectionsLinks::find()->where(['id_sections' => $section->id])->andWhere(['isActive' => 1])->orderBy(['priority' => SORT_ASC])->all() ?>
            <div class="col-12">
              <nav class="dop-links-wrap">
                <div class="dop-links-title">Полезные ссылки</div>
                  <? foreach ($cSections as $cItem): ?>
                      <? $cContent = Links::findOne($cItem->id_link) ?>
                    <div class="dop-link-item">
                      <a class="dop-link" href="<?= $cContent->link ?>">
                        <i class="mdi mdi-open-in-new"></i>
                          <?= $cContent->name ?>
                      </a>
                    </div>
                  <? endforeach; ?>
              </nav>
            </div>
            <? endif; ?>

            <? if ($k == 'files' && $l > 0): ?>
                <? $cSections = SectionsFiles::find()->where(['id_sections' => $section->id])->andWhere(['isActive' => 1])->orderBy(['priority' => SORT_ASC])->all() ?>
            <div class="col-12">
              <nav class="dop-links-wrap">
                <div class="dop-links-title">Порядок установки</div>
                  <? foreach ($cSections as $cItem): ?>
                      <? $cContent = Files::findOne($cItem->id_files) ?>
                    <div class="dop-link-item">
                      <a class="dop-link dop-download" href="<?= $cContent->file ?>">
                        <i class="mdi mdi-paperclip"></i>
                          <?= $cContent->name ?>
                      </a>
                    </div>
                  <? endforeach; ?>
              </nav>
            </div>
            <? endif; ?>

            <? if ($k == 'info' && $l > 0): ?>
                <? $cSections = SectionsInfo::find()->where(['id_sections' => $section->id])->andWhere(['isActive' => 1])->orderBy(['priority' => SORT_ASC])->all() ?>
            <div class="col-12">
              <div class="dop-info-wrap">
                <div class="dop-info-title">Полезная информация</div>
                  <? foreach ($cSections as $cItem): ?>
                      <? $cContent = Info::findOne($cItem->id_info) ?>
                    <div class="dop-info-text">
                      <div class="dop-info-desc">
                          <?= $cContent->text ?>
                      </div>
                      <div class="dop-info-date">
                          <?= Yii::$app->formatter->asDate($cContent->date) ?>
                      </div>
                    </div>
                  <? endforeach; ?>
              </div>
            </div>
            <? endif; ?>

            <? if ($k == 'descont' && $l > 0): ?>
                <? $cSections = SectionsDescont::find()->where(['id_sections' => $section->id])->andWhere(['isActive' => 1])->orderBy(['priority' => SORT_ASC])->all() ?>
            <div class="col-12">
                <? foreach ($cSections as $cItem): ?>
                    <? $cContent = Descont::findOne($cItem->id_descont) ?>
                  <div class="discont-wrap">
                    <a class="disc-link" href="<?= $cContent->link ?>"></a>
                    <div class="disc-text">
                      <div class="disc-title"><?= $cContent->title ?></div>
                      <div class="disc-subtitle"><?= $cContent->text ?></div>
                    </div>
                    <div class="disc-pic img-wrap">
                      <img src="<?= $cContent->img ?>">
                    </div>
                  </div>
                <? endforeach; ?>
            </div>
            <? endif; ?>

        <? endforeach; ?>

    </div>
  </div>
</aside>