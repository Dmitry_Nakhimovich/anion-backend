<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Sections;

?>

<main class="page-catalog">
  <div class="container">
    <div class="row">
      <div class="col-12">
          <?= $this->render('../common/_breadcrumbs',
              [
                  'links' => [
                      ['label' => 'Главная', 'url' => '/'],
                      ['label' => 'Новости']
                  ]
              ]);
          ?>
      </div>

        <? if ($section->menu_isActive ||
            $section->links_isActive ||
            $section->files_isActive ||
            $section->info_isActive ||
            $section->descont_isActive): ?>
          <div class="col-xl-3">
              <?= $this->render('../common/_sidebar', [
                  'section' => $section,
              ]);
              ?>
          </div>
        <? endif; ?>

      <div class="col-xl">
        <section class="cards-block">
          <div class="row">
            <!--Тайтл блока-->
            <div class="col-12">
              <div class="block-title-wrap">
                <h1 class="block-title">Новости</h1>
              </div>
            </div>
            <!--Содержание-->
              <? foreach ($news as $cNews): ?>
                  <? if ($cNews->img): ?>
                  <div class="col-lg-4">
                    <div class="card-wrap">
                      <a class="card-link"
                         href="<?= Url::toRoute([
                             '/news/detail',
                             'url' => Sections::find()->where(['id_type' => 3])
                                 ->andWhere(['id_item' => $cNews->id])->one()->link,
                         ]) ?>"></a>
                      <div class="card-pic img-wrap">
                        <img src="<?= $cNews->img ?>">
                      </div>
                      <div class="card-text">
                        <div class="card-date">
                          <i class="mdi mdi-calendar-blank"></i>
                            <?= Yii::$app->formatter->asDate($cNews->date) ?>
                        </div>
                        <div class="card-desc dot-text">
                            <?= $cNews->title ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <? else: ?>
                  <div class="col-lg-4">
                    <div class="card-wrap no-pic">
                      <a class="card-link"
                         href="<?= Url::toRoute([
                             '/news/detail',
                             'url' => Sections::find()->where(['id_type' => 3])
                                 ->andWhere(['id_item' => $cNews->id])->one()->link,
                         ]) ?>"></a>
                      <div class="card-text">
                        <div class="card-date">
                          <i class="mdi mdi-calendar-blank"></i>
                            <?= Yii::$app->formatter->asDate($cNews->date) ?>
                        </div>
                        <div class="card-desc dot-long">
                            <?= $cNews->title ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <? endif; ?>
              <? endforeach; ?>
            <!--Пагинация-->
            <div class="col-12">
                <?= $this->render('../common/_pagination', [
                    'pagination' => $pagination
                ]); ?>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</main>