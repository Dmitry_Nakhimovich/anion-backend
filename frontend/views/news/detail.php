<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Sections;

?>

<main class="page-news_detail">
  <div class="container">
    <div class="row">
      <div class="col-12">
          <?= $this->render('../common/_breadcrumbs',
              [
                  'links' => [
                      ['label' => 'Главная', 'url' => '/'],
                      ['label' => 'Новости', 'url' => '/news'],
                      ['label' => $news->title],
                  ]
              ]);
          ?>
      </div>

        <? if ($section->menu_isActive ||
            $section->links_isActive ||
            $section->files_isActive ||
            $section->info_isActive ||
            $section->descont_isActive): ?>
          <div class="col-xl-3">
              <?= $this->render('../common/_sidebar', [
                  'section' => $section,
              ]);
              ?>
          </div>
        <? endif; ?>

      <div class="col-xl">
        <section class="news-block">
          <div class="row">
            <div class="col-12">
              <div class="block-title-wrap">
                <h1 class="block-title"><?= $news->title ?></h1>
              </div>
            </div>
            <div class="col-12">
              <div class="news-date">
                <i class="mdi mdi-calendar-blank"></i>
                  <?= Yii::$app->formatter->asDate($news->date) ?>
              </div>
            </div>
            <div class="col-12">
              <div class="news-pic img-wrap">
                <img src="<?= $news->img ?>">
              </div>
            </div>
            <div class="col-12">
              <div class="news-text">
                  <?= $news->text ?>
              </div>
            </div>
          </div>
        </section>

          <? if ($newsOther): ?>
            <section class="other-block">
              <div class="row">
                <div class="col-12">
                  <div class="block-title">Вам будет интересно</div>
                </div>
                <div class="col-12">
                  <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <? foreach ($newsOther as $cOther): ?>
                            <? if ($cOther->img): ?>
                            <div class="swiper-slide">
                              <div class="card-wrap">
                                <a class="card-link"
                                   href="<?= Url::toRoute([
                                       '/news/detail',
                                       'url' => Sections::find()->where(['id_type' => 3])
                                           ->andWhere(['id_item' => $cOther->id])->one()->link,
                                   ]) ?>"></a>
                                <div class="card-pic img-wrap">
                                  <img src="<?= $cOther->img ?>">
                                </div>
                                <div class="card-text">
                                  <div class="card-date">
                                    <i class="mdi mdi-calendar-blank"></i>
                                      <?= Yii::$app->formatter->asDate($cOther->date) ?>
                                  </div>
                                  <div class="card-desc dot-text">
                                      <?= $cOther->title ?>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <? else: ?>
                            <div class="swiper-slide">
                              <div class="card-wrap no-pic">
                                <a class="card-link"
                                   href="<?= Url::toRoute([
                                       '/news/detail',
                                       'url' => Sections::find()->where(['id_type' => 3])
                                           ->andWhere(['id_item' => $cOther->id])->one()->link,
                                   ]) ?>"></a>
                                <div class="card-text">
                                  <div class="card-date">
                                    <i class="mdi mdi-calendar-blank"></i>
                                      <?= Yii::$app->formatter->asDate($cOther->date) ?>
                                  </div>
                                  <div class="card-desc dot-long">
                                      <?= $cOther->title ?>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <? endif; ?>
                        <? endforeach; ?>
                    </div>
                  </div>
                  <div class="swiper-navigation">
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                  </div>
                </div>
              </div>
            </section>
          <? endif; ?>

      </div>
    </div>
  </div>
</main>
