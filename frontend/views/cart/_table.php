<?php

use common\models\Stock;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="table-responsive-lg">
  <table>
    <tbody>
    <?= $this->render('_rows', [
        'products' => $products,
        'products_cart' => $products_cart,
    ]) ?>
    </tbody>
  </table>
</div>
<div class="table-bottom">
  <div class="result-wrap">
    <div class="res-city">
      <span class="city-title">Склад:</span>
        <? session_start(); ?>
        <? $stocks = Stock::find()->where(['isActive' => 1])->orderBy('priority')->all(); ?>
        <? $baseStock = Stock::find()->where(['isActive' => 1])->orderBy('priority')->one()->code; ?>
        <? if ($_SESSION['stock']) {
            $cstock = $_SESSION['stock'];
        } else {
            $_SESSION['stock'] = $baseStock;
            $cstock = $_SESSION['stock'];
        } ?>
      <i class="mdi mdi-map-marker-outline"></i>
      <select class="selectpicker" id="cart-location"
              data-header="Изменить наименование склада">
          <? foreach ($stocks as $stock): ?>
              <? if ($cstock == $stock->code): ?>
              <option value="<?= $stock->code ?>" selected><?= $stock->name ?></option>
              <? else: ?>
              <option value="<?= $stock->code ?>"><?= $stock->name ?></option>
              <? endif; ?>
          <? endforeach; ?>
      </select>
    </div>
    <div class="res-price">
      <span class="price-title">Итого:</span>
      <span class="price-val"><?= $_SESSION['cart_sum'] ?: 0 ?></span>
    </div>
    <div class="res-btn">
        <? if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])): ?>
          <a class="btn blue" href="#" data-toggle="modal" data-target="#orderModal">Заказать</a>
          <div class="modal fade" id="orderModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg">
              <div class="modal-content">
                  <?= Html::beginForm(
                      ['search/mail'],
                      'post',
                      ['enctype' => 'multipart/form-data']
                  ) ?>
                  <?= Html::hiddenInput('cart', 'true'); ?>
                <div class="modal-header">
                  <div class="col-12">
                    <div class="modal-title">Форма заказа</div>
                  </div>
                  <button class="close" type="button" data-dismiss="modal"><span>×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label for="fioInputOrderModal">Ваше имя*:</label>
                          <input class="form-control" id="fioInputOrderModal" type="text" name="fio"
                                 placeholder="" required>
                        </div>
                        <div class="form-group">
                          <label for="companyInputOrderModal">Компания / организация
                            (необязательно):</label>
                          <input class="form-control" id="companyInputOrderModal" type="text"
                                 name="company" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="telInputOrderModal">Телефон*:</label>
                          <input class="form-control" id="telInputOrderModal" type="tel" name="tel"
                                 placeholder="" required>
                        </div>
                        <div class="form-group">
                          <label for="emailInputOrderModal">Email*:</label>
                          <input class="form-control" id="emailInputOrderModal" type="email" name="email"
                                 placeholder="" required>
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="form-group">
                          <label for="commentInputOrderModal">Сообщение:</label>
                          <textarea class="form-control" id="commentInputOrderModal" rows="8"
                                    name="comment" placeholder=""></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button class="btn blue" id="orderModalBtn" type="submit">Отправить заявку</button>
                </div>
                  <?= Html::endForm() ?>
              </div>
            </div>
          </div>
        <? endif; ?>
    </div>
  </div>
</div>
