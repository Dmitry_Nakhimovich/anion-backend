<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<main class="page-cart">
  <div class="container">
    <div class="row">

      <div class="col-12">
          <?= $this->render('../common/_breadcrumbs',
              [
                  'links' => [
                      ['label' => 'Главная', 'url' => '/'],
                      ['label' => 'Корзина']
                  ]
              ]);
          ?>
      </div>

      <div class="col-12">
        <section class="cart-block">
          <div class="row">
            <div class="col-12">
              <div class="block-title-wrap">
                <h1 class="block-title">Корзина</h1>
              </div>
            </div>
            <div class="col-12">
              <div class="cart-wrap">
                  <?= $this->render('_table', [
                      'products' => $products,
                      'products_cart' => $products_cart,
                  ]) ?>
              </div>
            </div>
          </div>
        </section>
      </div>

    </div>
  </div>
</main>