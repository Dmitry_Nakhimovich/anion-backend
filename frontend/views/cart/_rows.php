<?php

use common\models\Stock;
use common\models\StockContent;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<? $_SESSION['cart_sum'] = 0 ?>
<? if (!isset($products_cart) || empty($products_cart)): ?>
  <tr id="hide-cart-btn">
    <td align="center">
      Корзина пуста. Сначала добавьте товары.
    </td>
  </tr>
<? else: ?>
    <? foreach ($products as $product): ?>
        <?
        $cstock = Stock::find()->where(['code' => $_SESSION['stock']])->one();
        $cprice = StockContent::find()->where(['product_id' => $product->id])
            ->andWhere(['stock_id' => $cstock->id])->one();
        foreach ($products_cart as $pc)
            if ($product->id == $pc['product_id'])
                $cproduct_session = $pc;
        ?>
    <tr>
      <td>
        <div class="item-pic-wrap">
          <img class="item-pic" src="<?= $product->img ?>">
        </div>
      </td>
      <td>
        <a class="item-title"
           href="<?= Url::toRoute(['/products/detail', 'id' => $product->id]) ?>"><?= $product->title ?></a>
        <br>
        <span class="item-subtitle"><?= $product->article ?></span>
      </td>
      <td>
        <input class="item-counter" type="text"
               data-product_id="<?= $cproduct_session['product_id'] ?>"
               value="<?= $cproduct_session['product_count'] ?: 1 ?>">
        <span class="item-unit">1 шт. = <?= $cprice->price ?></span>
      </td>
      <td align="center">
          <? $_SESSION['cart_sum'] += $cproduct_session['product_count'] * $cprice->price; ?>
        <span class="item-price"><?= $cproduct_session['product_count'] * $cprice->price ?></span>
      </td>
      <td align="center">
        <a class="item-del"
           data-product_id="<?= $product->id ?>"
           data-product_count="<?= $cproduct_session['product_count'] ?>"
           href="#">Удалить</a>
      </td>
    </tr>
    <? endforeach; ?>
<? endif; ?>
