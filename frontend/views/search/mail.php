<?php

use common\models\Settings;
use common\models\Stock;
use common\models\StockContent;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <title>Новая заявка на сайте Anion</title>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0"
       style="border-collapse: collapse; border-spacing: 0;font-family:'Open Sans', sans-serif;">
  <tr>
    <td width="100%" align="center">
      <table border="0" cellpadding="0" cellspacing="0" width="600"
             style="border-collapse: collapse; border-spacing: 0;">
        <!-- title start-->
        <tr>
          <td width="100%" style="background-color: #0097ff;padding-left: 50px; padding-right: 50px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; border-spacing: 0;">
              <tr>
                <td>
                  <a href="<?= Url::to('@web', true) ?>">
                    <img src="<?= Url::to('@web/img/email/anion_email_header_logo.png', true) ?>"
                         width="90" height="60">
                  </a>
                </td>
              </tr>
              <tr>
                <td align="center" width="100%">
                  <span style="vertical-align: middle; display:inline-block;">
                    <img src="<?= Url::to('@web/img/email/anion_email_pic_cart.png', true) ?>"
                         width="80" height="70">
                  </span>
                  <span style="vertical-align: middle; display:inline-block; font-size: 18px; color: #ffffff;">
                    Заявка на заказ отправлена
                  </span>
                </td>
              </tr>
              <tr>
                <td width="100%" height="60"></td>
              </tr>
            </table>
          </td>
        </tr>
        <!-- title end-->

        <!-- head start -->
        <tr>
          <td width="100%" style="border-bottom: 1px solid #c9c9c9;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; border-spacing: 0;">
              <tr>
                <td width="100%" style="background-color: #ffffff;padding-left: 50px; padding-right: 50px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%"
                         style="border-collapse: collapse; border-spacing: 0;">
                    <tr>
                      <td width="100%" height="25"></td>
                    </tr>
                    <tr>
                      <td>
                        <span style="color: #003068; font-weight: bold;font-size: 16px;"
                        >Здравствуйте, <?= $mail['fio'] ?>!</span>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="25"></td>
                    </tr>
                    <tr>
                      <td>
                        <span style="color: #003068;font-weight: 400;font-size: 16px;">
                          Мы приняли ваш заказ, скоро с Вами свяжется
                          <br>
                          наш менеджер для подтверждения деталей заказа
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="50"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <!-- head end-->

        <!-- main start-->
        <tr>
          <td width="100%" style="background-color: #ffffff;padding-left: 50px; padding-right: 50px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; border-spacing: 0;">
              <tr>
                <td width="100%" height="50"></td>
              </tr>
              <tr>
                <td>
                  <span style="color: #003068; font-weight: bold;">Позиции заказа:</span>
                </td>
              </tr>
              <tr>
                <td width="100%" height="35"></td>
              </tr>
              <tr>
                <td width="100%">
                  <table width="100%" border="0" cellpadding="0" cellspacing="0"
                         style="border-collapse: collapse; border-spacing: 0;">
                      <? foreach ($products as $product): ?>
                          <?
                          $cstock = Stock::find()->where(['code' => $_SESSION['stock']])->one();
                          $cprice = StockContent::find()->where(['product_id' => $product->id])
                              ->andWhere(['stock_id' => $cstock->id])->one();
                          foreach ($products_cart as $pc)
                              if ($product->id == $pc['product_id'])
                                  $cproduct_session = $pc;
                          ?>
                        <tr>
                          <td>
                            <img src="<?= Url::to('@web' . $product->img, true) ?>" width="80" height="50">
                          </td>
                          <td>
                            <span style="display: block;color: #003068; font-size: 14px;font-weight: 600;">
                              <a style="text-decoration: none; color: #162b62;"
                                 href="<?= Url::toRoute(['@web/products/detail', 'id' => $product->id], true) ?>">
                                <?= $product->title ?>
                              </a>
                            </span>
                            <span style="display: block;color: #9e9e9e; font-size: 12px;"
                            ><?= $cproduct_session['product_count'] ?> шт.</span>
                          </td>
                          <td style="padding-bottom: 20px;">
                            <span style="color: #003068;font-weight: 600;"
                            ><?= $cproduct_session['product_count'] * $cprice->price ?> ₽</span>
                          </td>
                        </tr>
                      <? endforeach; ?>
                  </table>
                </td>
              </tr>
              <tr>
                <td width="100%" height="45"></td>
              </tr>
              <tr>
                <td width="100%">
                  <table>
                    <tr>
                      <td style="color: #949494;">
                        <span>Склад:</span>
                      </td>
                      <td style="text-decoration: underline;color: #003068;">
                          <? $stock = Stock::find()->where(['code' => $_SESSION['stock']])->one() ?>
                        <span style="padding-left: 15px"><?= $stock->name ?></span>
                      </td>
                      <td style="color: #949494;">
                        <span style="padding-left: 30px">Итого:</span>
                      </td>
                      <td style="color: #66c1ff;font-size: 18px;">
                        <span style="padding-left: 20px"><?= $_SESSION['cart_sum'] ?: 0 ?> ₽</span>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td width="100%" height="45"></td>
              </tr>
            </table>
          </td>
        </tr>
        <!-- main end-->

        <!-- footer start -->
        <tr>
          <td width="100%" style="background-color: #f5f7fa;padding-left: 50px; padding-right: 50px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse: collapse; border-spacing: 0;">
              <tr>
                <td width="100%" height="30"></td>
              </tr>
              <tr>
                <td width="100%">
                  <table border="0" cellpadding="0" cellspacing="0"
                         style="width: 500px;border-collapse: collapse; border-spacing: 0;">
                    <tr>
                      <td>
                         <span style="display:block;">
                           <a href="<?= Url::to('@web', true) ?>" style="text-decoration: none;">
                            <img src="<?= Url::to('@web/img/email/anion_email_footer_logo.png', true) ?>"
                                 width="90" height="60">
                           </a>
                         </span>
                        <span style="font-size: 10px;display: block;color: #b2b5c0;">
                           © ООО «Анион», пластиковые емкости, промышленная
                          <br>
                           тара, листы, панели, стержни из пластика, дорожные
                          <br>
                           блоки, конусы, ограждения, садовая мебель. 1992—2017
                        </span>
                      </td>
                      <td>
                        <span style="display:block;height: 40px;">
                          <? $settings = Settings::find()->where(['name' => 'tel'])->one() ?>
                          <a href="tel:<?= $settings->text ?>" style="text-decoration: none;">
                            <span width="12" height="12" style="display:inline-block;">
                              <img src="<?= Url::to('@web/img/email/anion_email_1.png', true) ?>"
                                   width="12" height="12">
                            </span>
                            <span style="display:inline-block; font-size: 10px;font-weight: bold;color: #162b62;">
                               <?= $settings->text ?>
                            </span>
                          </a>
                        </span>
                        <span style="display:block;height: 40px;">
                          <? $settings = Settings::find()->where(['name' => 'adr'])->one() ?>
                          <a href="<?= $settings->link ?>" style="text-decoration: none;">
                            <span width="10" height="15" style="display:inline-block;">
                              <img src="<?= Url::to('@web/img/email/anion_email_2.png', true) ?>"
                                   width="10" height="15">
                            </span>
                            <span style="display:inline-block; font-size: 10px;font-weight: bold;color: #162b62;">
                              <?= $settings->text ?>
                            </span>
                          </a>
                        </span>
                        <span style="display:block;height: 40px;">
                          <? $settings = Settings::find()->where(['name' => 'work'])->one() ?>
                          <span width="12" height="12" style="display:inline-block;">
                            <img src="<?= Url::to('@web/img/email/anion_email_3.png', true) ?>"
                                 width="12" height="12">
                          </span>
                          <span style="display:inline-block; font-size: 10px;font-weight: bold;color: #162b62;">
                            <?= $settings->text ?>
                          </span>
                        </span>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td width="100%" height="25"></td>
              </tr>
            </table>
          </td>
        </tr>
        <!-- footer end -->
      </table>
    </td>
  </tr>
</table>
</body>
</html>