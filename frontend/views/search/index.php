<?php

use common\models\AttrContent;
use common\models\Sections;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<main class="page-search">
  <div class="container">
    <div class="row">
      <!--Крошки-->
      <div class="col-12">
          <?= $this->render('../common/_breadcrumbs',
              [
                  'links' => [
                      ['label' => 'Главная', 'url' => '/'],
                      ['label' => 'Результаты поиска']
                  ]
              ]);
          ?>
      </div>

      <div class="col-12">
        <section class="search-block">
          <div class="row">
            <div class="col-12">
              <div class="block-title-wrap">
                <h1 class="block-title">Результаты поиска</h1>
              </div>
            </div>
            <div class="col-12">
              <div class="block-results-search">
                  <? if (isset($search) && !empty($search)): ?>
                      <? foreach ($products as $s): ?>
                      <div class="result-search">
                        <div class="content-result-search">
                          <a class="content-title" style="color: #162b62 !important;"
                             href="<?= Url::toRoute(['/products/detail', 'id' => $s->id]) ?>"
                          ><?= $s->title ?></a>
                          <div class="content-text"
                          ><?= mb_strimwidth(strip_tags($s->text), 0, 50, "...") ?></div>
                        </div>
                      </div>
                      <? endforeach; ?>
                      <? foreach ($materials as $s): ?>
                      <div class="result-search">
                        <div class="content-result-search">
                          <a class="content-title" style="color: #162b62 !important;"
                             href="<?= Url::toRoute([
                                 '/materials/detail',
                                 'url' => Sections::find()->where(['id_type' => 2])
                                     ->andWhere(['id_item' => $s->id])
                                     ->one()->link,
                             ]) ?>"><?= $s->title ?></a>
                          <div class="content-text"
                          ><?= mb_strimwidth(strip_tags($s->text), 0, 50, "...") ?></div>
                        </div>
                      </div>
                      <? endforeach; ?>
                      <? foreach ($news as $s): ?>
                      <div class="result-search">
                        <div class="content-result-search">
                          <a class="content-title" style="color: #162b62 !important;"
                             href="<?= Url::toRoute([
                                 '/news/detail',
                                 'url' => Sections::find()->where(['id_type' => 3])
                                     ->andWhere(['id_item' => $s->id])
                                     ->one()->link,
                             ]) ?>"><?= $s->title ?></a>
                          <div class="content-text"
                          ><?= mb_strimwidth(strip_tags($s->text), 0, 50, "...") ?></div>
                        </div>
                      </div>
                      <? endforeach; ?>
                  <? else: ?>
                    <div class="result-search">
                      <div class="content-result-search">
                        <div class="content-title">Поиск по запросу не дал результатов...</div>
                      </div>
                    </div>
                  <? endif; ?>
              </div>
            </div>
              <? if (isset($search) && !empty($search)): ?>
                <!--Пагинация-->
                <div class="col-12">
                    <? /*= $this->render('../common/_pagination', [
                        'pagination' => $pagination
                    ]); */ ?>
                </div>
              <? endif; ?>
          </div>
        </section>
      </div>
    </div>
  </div>
</main>