<?php

use common\models\NavMenu;
use common\models\Sections;
use common\models\SectionsType;
use common\models\Settings;
use common\models\Stock;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\MainAsset;
use common\widgets\Alert;

MainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="keywords" content="">
    <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <link rel="shortcut icon" type="image/x-icon" sizes="32x32" href="<?= Yii::$app->homeUrl ?>img/icons/favicon.ico">
  <!--<link rel="apple-touch-icon" href="<? /*= Yii::$app->homeUrl */ ?>img/icons/favicon.ico">-->
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="blue" id="page-preloader">
  <div class="preload-wrap">
    <div class="obj obj-1"></div>
    <div class="obj obj-2"></div>
    <div class="obj obj-3"></div>
    <div class="obj obj-4"></div>
  </div>
</div>
<header>
  <nav class="navbar navbar-expand-lg">
    <div class="container">
      <a class="navbar-brand d-block d-lg-none" href="/">
        <img src="/img/icons/logo.png" alt="Anion logo">
      </a>
      <button class="navbar-toggler" data-toggle="collapse" data-target="#header-navbar"><span
          class="navbar-toggler-icon"></span>
      </button>
      <!--Хедер меню-->
      <div class="collapse navbar-collapse row" id="header-navbar">
        <!--Верхнее меню-->
        <div class="navbar-top col-12">
          <div class="row">
            <div class="col-xl-auto col-lg-12 d-none d-lg-block">
              <a class="navbar-brand" href="/">
                <img src="/img/icons/logo.png" alt="Anion logo">
              </a>
            </div>
            <div class="col">
              <ul class="navbar-nav">
                <li class="nav-social">
                    <? $settings = Settings::find()->where(['name' => 'tel'])->one() ?>
                  <a class="social-link" href="tel:<?= $settings->text ?>">
                    <i class="mdi mdi-phone-in-talk"></i><?= $settings->text ?>
                  </a>
                </li>
                <li class="nav-social">
                    <? $settings = Settings::find()->where(['name' => 'adr'])->one() ?>
                  <a class="social-link href="<?= $settings->link ?>">
                  <i class="mdi mdi-map-marker-outline"></i><?= $settings->text ?>
                  </a>
                </li>
                <li class="nav-social">
                    <? $settings = Settings::find()->where(['name' => 'work'])->one() ?>
                  <div class="social-link">
                    <i class="mdi mdi-clock"></i><?= $settings->text ?>
                  </div>
                </li>
              </ul>
            </div>
            <div class="col-auto">
              <a class="btn btn-lg blue-border" href="#" data-toggle="modal"
                 data-target="#feedBackModal">НАПИСАТЬ НАМ</a>
              <div class="modal fade" id="feedBackModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                  <div class="modal-content">
                      <?= Html::beginForm(
                          ['search/mail'],
                          'post',
                          ['enctype' => 'multipart/form-data']
                      ) ?>
                      <?= Html::hiddenInput('mail', 'true'); ?>
                    <div class="modal-header">
                      <div class="col-12">
                        <div class="modal-title">Написать нам</div>
                      </div>
                      <button class="close" type="button" data-dismiss="modal"><span>×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group">
                              <label for="fioInputFeedBackModal">Ваше имя*:</label>
                              <input class="form-control" id="fioInputFeedBackModal"
                                     type="text" name="fio" placeholder="" required>
                            </div>
                            <div class="form-group">
                              <label for="companyInputFeedBackModal">Компания / организация (необязательно):</label>
                              <input class="form-control" id="companyInputFeedBackModal"
                                     type="text" name="company" placeholder="">
                            </div>
                            <div class="form-group">
                              <label for="telInputFeedBackModal">Телефон*:</label>
                              <input class="form-control" id="telInputFeedBackModal"
                                     type="tel" name="tel" placeholder="" required>
                            </div>
                            <div class="form-group">
                              <label for="emailInputFeedBackModal">Email*:</label>
                              <input class="form-control" id="emailInputFeedBackModal"
                                     type="email" name="email" placeholder="" required>
                            </div>
                          </div>
                          <div class="col-md-7">
                            <div class="form-group">
                              <label for="commentInputFeedBackModal">Сообщение:</label>
                              <textarea class="form-control" id="commentInputFeedBackModal"
                                        rows="8" name="comment" placeholder=""></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button class="btn blue" id="feedBackModalBtn" type="submit">Отправить</button>
                    </div>
                      <?= Html::endForm() ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--Нижнее меню-->
        <div class="navbar-bottom col-12">
          <div class="row">
            <div class="col-auto search-block">
                <?= Html::beginForm(
                    ['search/index'],
                    'get',
                    [
                        'enctype' => 'multipart/form-data',
                        'class' => 'form-inline header-form-search',
                        'id' => 'header-search',
                    ]
                ) ?>
                <?= Html::hiddenInput('search', 'true'); ?>
              <input class="form-control" placeholder="Поиск"
                     type="text" name="header_search">
              <button class="lupa-search" type="submit"></button>
                <?= Html::endForm() ?>
            </div>
            <div class="col">
                <? $menu = SectionsType::find()->where(['isActive' => 1])->orderBy('priority')->all() ?>
                <? if ($menu): ?>
                  <ul class="navbar-nav navbar-ul">
                      <? foreach ($menu as $cmenu): ?>
                          <? if ($cmenu->id == 1): ?>
                          <li class="nav-item">
                            <a class="nav-link"
                               href="<?= Url::toRoute(['/site']) ?>"><?= $cmenu->name ?></a>
                          </li>
                          <? elseif ($cmenu->id == 2): ?>
                          <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                                <?= $cmenu->name ?>
                            </a>
                            <div class="dropdown-menu multi-level">
                                <?
                                $list = NavMenu::find()->where(['menu_type' => 1])
                                    ->andWhere(['isActive' => 1])
                                    ->andWhere(['<=', 'menu_level', 2])
                                    ->orderBy('priority')
                                    ->all();
                                $menuList = [];
                                foreach ($list as $ml) {
                                    if (empty($menuList[$ml->parent_id]) || !isset($menuList[$ml->parent_id]))
                                        $menuList[$ml->parent_id] = [];
                                    $menuList[$ml->parent_id][] = $ml;
                                }
                                ?>
                                <? function viewMenu($arr, $parent_id = 0)
                                {
                                    //Условия выхода из рекурсии
                                    if (empty($arr[$parent_id])) {
                                        return;
                                    }

                                    //перебираем в цикле массив и выводим на экран
                                    foreach ($arr[$parent_id] as $arr_item) {
                                        $podcat_item = NavMenu::find()->where(['parent_id' => $arr_item->id])
                                            ->andWhere(['isActive' => 1])
                                            ->andWhere(['<=', 'menu_level', 2])
                                            ->orderBy('priority')
                                            ->all();
                                        if (empty($podcat_item)) {
                                            echo '<a class="dropdown-item" href="'
                                                . Url::toRoute([
                                                    '/products/category',
                                                    'url' => Sections::findOne($arr_item->external_id)->link,
                                                ])
                                                . '">' . $arr_item->name . '</a>';
                                        } else {
                                            echo '<div class="dropdown-submenu">';
                                            echo '<a class="dropdown-item" href="'
                                                . Url::toRoute([
                                                    '/products/category',
                                                    'url' => Sections::findOne($arr_item->external_id)->link,
                                                ])
                                                . '">' . $arr_item->name . '</a>';
                                            //рекурсия - проверяем нет ли дочерних категорий
                                            echo '<div class="dropdown-menu">';
                                            viewMenu($arr, $arr_item->id);
                                            echo '</div>';
                                            echo '</div>';
                                        }
                                    }
                                } ?>
                                <? viewMenu($menuList); ?>
                            </div>
                          </li>
                          <? elseif ($cmenu->id == 3): ?>
                          <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                                <?= $cmenu->name ?>
                            </a>
                            <div class="dropdown-menu multi-level">
                                <? $list = NavMenu::find()->where(['menu_type' => 2])
                                    ->andWhere(['isActive' => 1])
                                    ->orderBy('priority')
                                    ->all(); ?>
                                <? $drops = [];
                                foreach ($list as $el)
                                    if ($el->parent_id > 0)
                                        $drops[] = $el->parent_id;
                                $drops = array_unique($drops);
                                ?>
                                <? foreach ($list as $item): ?>
                                    <? if (in_array($item->id, $drops)): ?>
                                    <div class="dropdown-submenu">
                                      <a class="dropdown-item"
                                         href="<?= Url::toRoute([
                                             '/materials/detail',
                                             'url' => Sections::find()->where(['id_type' => 2])
                                                 ->andWhere(['id_item' => $item->external_id])
                                                 ->one()->link,
                                         ]) ?>">
                                          <?= $item->name ?>
                                      </a>
                                      <div class="dropdown-menu">
                                          <? $sublist = NavMenu::find()->where(['parent_id' => $item->id])
                                              ->andWhere(['isActive' => 1])
                                              ->orderBy('priority')
                                              ->all(); ?>
                                          <? foreach ($sublist as $subitem): ?>
                                            <a class="dropdown-item"
                                               href="<?= Url::toRoute([
                                                   '/materials/detail',
                                                   'url' => Sections::find()->where(['id_type' => 2])
                                                       ->andWhere(['id_item' => $subitem->external_id])
                                                       ->one()->link,
                                               ]) ?>">
                                                <?= $subitem->name ?>
                                            </a>
                                          <? endforeach; ?>
                                      </div>
                                    </div>
                                    <? elseif ($item->menu_level == 1): ?>
                                    <a class="dropdown-item"
                                       href="<?= Url::toRoute([
                                           '/materials/detail',
                                           'url' => Sections::find()->where(['id_type' => 2])
                                               ->andWhere(['id_item' => $item->external_id])
                                               ->one()->link,
                                       ]) ?>">
                                        <?= $item->name ?>
                                    </a>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </div>
                          </li>
                          <? elseif ($cmenu->id == 4): ?>
                          <li class="nav-item">
                            <a class="nav-link"
                               href="<?= Url::toRoute(['/news']) ?>"><?= $cmenu->name ?></a>
                          </li>
                          <? elseif ($cmenu->id == 5): ?>
                          <li class="nav-item">
                              <? $item = Sections::findOne(5) ?>
                            <a class="nav-link"
                               href="<?= Url::toRoute([
                                   '/materials/detail',
                                   'url' => Sections::find()->where(['id_type' => 2])
                                       ->andWhere(['id_item' => $item->id_item])
                                       ->one()->link,
                               ]) ?>"><?= $cmenu->name ?></a>
                          </li>
                          <? elseif ($cmenu->id == 6): ?>
                          <li class="nav-item">
                              <? $item = Sections::findOne(6) ?>
                            <a class="nav-link"
                               href="<?= Url::toRoute([
                                   '/materials/detail',
                                   'url' => Sections::find()->where(['id_type' => 2])
                                       ->andWhere(['id_item' => $item->id_item])
                                       ->one()->link,
                               ]) ?>"><?= $cmenu->name ?></a>
                          </li>
                          <? endif; ?>
                      <? endforeach; ?>
                  </ul>
                <? endif; ?>
            </div>
            <div class="col-auto bucket">
              <a class="btn-shop nav-link" id="header-btn-shop"
                 href="<?= Url::toRoute(['/cart']) ?>">
                <i class="mdi mdi-cart"></i>Корзина<span
                  class="btn-shop-items">(<?= isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0 ?> товаров)</span>
              </a>
            </div>
          </div>
        </div>
        <div class="navbar-location">
          <div class="header-location">
            <div class="location">
                <? session_start(); ?>
                <? $stocks = Stock::find()->where(['isActive' => 1])->orderBy('priority')->all(); ?>
                <? $baseStock = Stock::find()->where(['isActive' => 1])->orderBy('priority')->one()->code; ?>
                <? if ($_SESSION['stock']) {
                    $cstock = $_SESSION['stock'];
                } else {
                    $_SESSION['stock'] = $baseStock;
                    $cstock = $_SESSION['stock'];
                } ?>
              <i class="mdi mdi-map-marker-outline"></i>
              <select class="selectpicker" id="header-location"
                      data-header="Изменить наименование склада">
                  <? foreach ($stocks as $stock): ?>
                      <? if ($cstock == $stock->code): ?>
                      <option value="<?= $stock->code ?>" selected><?= $stock->name ?></option>
                      <? else: ?>
                      <option value="<?= $stock->code ?>"><?= $stock->name ?></option>
                      <? endif; ?>
                  <? endforeach; ?>
              </select>
            </div>
            <div class="loc-text">Текущий склад доставки:</div>
          </div>
        </div>
      </div>
    </div>
  </nav>
</header>

<?= $content ?>

<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <div class="footer-left-wrap">
          <a class="footer-logo" href="/">
            <img src="/img/icons/logo.png" alt="Anion logo">
          </a>
          <div class="footer-about-text">© ООО «Анион», пластиковые емкости, промышленная тара, листы, панели, стержни
            <br>из пластика, дорожные блоки, конусы, ограждения, садовая мебель. 1992—2017
          </div>
          <a class="footer-nefabrika" href="http://nefabrika.ru">Сайт разработан в студии «Нефабрика»</a>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="row">
          <div class="col-lg-7">
            <div class="nav-social">
                <? $settings = Settings::find()->where(['name' => 'tel'])->one() ?>
              <a class="social-link" href="tel:<?= $settings->text ?>">
                <i class="mdi mdi-phone-in-talk"></i><?= $settings->text ?></a>
            </div>
            <div class="nav-social">
                <? $settings = Settings::find()->where(['name' => 'adr'])->one() ?>
              <a class="social-link href="<?= $settings->link ?>">
              <i class="mdi mdi-map-marker-outline"></i><?= $settings->text ?>
              </a>
            </div>
          </div>
          <div class="col-lg-5">
            <div class="nav-social">
                <? $settings = Settings::find()->where(['name' => 'work'])->one() ?>
              <div class="social-link">
                <i class="mdi mdi-clock"></i><?= $settings->text ?>
              </div>
            </div>
            <div class="nav-social">
                <? $settings = Settings::find()->where(['name' => 'mail'])->one() ?>
              <a class="social-link" href="mailto:<?= $settings->text ?>">
                <i class="mdi mdi-email-outline"></i><?= $settings->text ?></a>
            </div>
          </div>
          <div class="col-12">
              <? $menu = SectionsType::find()->where(['isActive' => 1])->orderBy('priority')->all() ?>
              <? if ($menu): ?>
                <ul class="navbar-nav">
                    <? foreach ($menu as $cmenu): ?>
                        <? if ($cmenu->id == 1): ?>
                        <li class="nav-item">
                          <a class="nav-link"
                             href="<?= Url::toRoute(['/site']) ?>"><?= $cmenu->name ?></a>
                        </li>
                        <? elseif ($cmenu->id == 2): ?>
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                              <?= $cmenu->name ?>
                          </a>
                          <div class="dropdown-menu multi-level">
                              <?
                              $list = NavMenu::find()->where(['menu_type' => 1])
                                  ->andWhere(['isActive' => 1])
                                  ->orderBy('priority')
                                  ->all();
                              $menuList = [];
                              foreach ($list as $ml) {
                                  if (empty($menuList[$ml->parent_id]) || !isset($menuList[$ml->parent_id]))
                                      $menuList[$ml->parent_id] = [];
                                  $menuList[$ml->parent_id][] = $ml;
                              }
                              ?>
                              <? viewMenu($menuList); ?>
                          </div>
                        </li>
                        <? elseif ($cmenu->id == 3): ?>
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                              <?= $cmenu->name ?>
                          </a>
                          <div class="dropdown-menu multi-level">
                              <? $list = NavMenu::find()->where(['menu_type' => 2])
                                  ->andWhere(['isActive' => 1])
                                  ->orderBy('priority')
                                  ->all(); ?>
                              <? $drops = [];
                              foreach ($list as $el)
                                  if ($el->parent_id > 0)
                                      $drops[] = $el->parent_id;
                              $drops = array_unique($drops);
                              ?>
                              <? foreach ($list as $item): ?>
                                  <? if (in_array($item->id, $drops)): ?>
                                  <div class="dropdown-submenu">
                                    <a class="dropdown-item"
                                       href="<?= Url::toRoute([
                                           '/materials/detail',
                                           'url' => Sections::find()->where(['id_type' => 2])
                                               ->andWhere(['id_item' => $item->external_id])
                                               ->one()->link,
                                       ]) ?>">
                                        <?= $item->name ?>
                                    </a>
                                    <div class="dropdown-menu">
                                        <? $sublist = NavMenu::find()->where(['parent_id' => $item->id])
                                            ->andWhere(['isActive' => 1])
                                            ->orderBy('priority')
                                            ->all(); ?>
                                        <? foreach ($sublist as $subitem): ?>
                                          <a class="dropdown-item"
                                             href="<?= Url::toRoute([
                                                 '/materials/detail',
                                                 'url' => Sections::find()->where(['id_type' => 2])
                                                     ->andWhere(['id_item' => $subitem->external_id])
                                                     ->one()->link,
                                             ]) ?>">
                                              <?= $subitem->name ?>
                                          </a>
                                        <? endforeach; ?>
                                    </div>
                                  </div>
                                  <? elseif ($item->menu_level == 1): ?>
                                  <a class="dropdown-item"
                                     href="<?= Url::toRoute([
                                         '/materials/detail',
                                         'url' => Sections::find()->where(['id_type' => 2])
                                             ->andWhere(['id_item' => $item->external_id])
                                             ->one()->link,
                                     ]) ?>">
                                      <?= $item->name ?>
                                  </a>
                                  <? endif; ?>
                              <? endforeach; ?>
                          </div>
                        </li>
                        <? elseif ($cmenu->id == 4): ?>
                        <li class="nav-item">
                          <a class="nav-link"
                             href="<?= Url::toRoute(['/news']) ?>"><?= $cmenu->name ?></a>
                        </li>
                        <? elseif ($cmenu->id == 5): ?>
                        <li class="nav-item">
                            <? $item = Sections::findOne(5) ?>
                          <a class="nav-link"
                             href="<?= Url::toRoute([
                                 '/materials/detail',
                                 'url' => Sections::find()->where(['id_type' => 2])
                                     ->andWhere(['id_item' => $item->id_item])
                                     ->one()->link,
                             ]) ?>"><?= $cmenu->name ?></a>
                        </li>
                        <? elseif ($cmenu->id == 6): ?>
                        <li class="nav-item">
                            <? $item = Sections::findOne(6) ?>
                          <a class="nav-link"
                             href="<?= Url::toRoute([
                                 '/materials/detail',
                                 'url' => Sections::find()->where(['id_type' => 2])
                                     ->andWhere(['id_item' => $item->id_item])
                                     ->one()->link,
                             ]) ?>"><?= $cmenu->name ?></a>
                        </li>
                        <? endif; ?>
                    <? endforeach; ?>
                </ul>
              <? endif; ?>
          </div>
          <div class="col-12">
            <div class="nav-other-wrap">
                <? $settings = Settings::find()->where(['name' => 'rules'])->one() ?>
              <a class="nav-other" target="_blank" href="<?= $settings->link ?>"><?= $settings->text ?></a>
              <br>
              <a class="nav-other" href="<?= Url::toRoute(['/sitemap']) ?>">Карта сайта</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
