<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'language' => 'ru-RU',
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'formatter' => [
            'dateFormat' => 'dd/MM/Y',
            'datetimeFormat' => 'dd/MM/Y H:i:s',
            'timeFormat' => 'H:i:s',
            'locale' => 'ru-RU',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                //'<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
                'sitemap' => 'site/sitemap',
                'materials/<url:.+>' => 'materials/detail',
                'news/<url:.+>' => 'news/detail',
                'products/category/<url:.+>' => 'products/category',
                'products/detail/<id:.+>' => 'products/detail',
            ],
        ],
    ],
    'params' => $params,
];
