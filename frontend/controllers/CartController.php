<?php

namespace frontend\controllers;

use common\models\AttrContent;
use common\models\NavMenu;
use common\models\Products;
use common\models\ProductsAttr;
use common\models\Sections;
use common\models\Stock;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class CartController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        $products_cart = $_SESSION['cart'];
        $products = [];
        if (isset($products_cart) || !empty($products_cart))
            foreach ($products_cart as $pc)
                $products[] = Products::find()->where(['isActive' => 1])
                    ->andWhere(['id' => $pc['product_id']])
                    ->one();

        return $this->render('index', [
            'products' => $products,
            'products_cart' => $products_cart,
        ]);
    }

    public function actionUpdate()
    {
        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->get('upd')) {
                session_start();
                $cart = $_SESSION['cart'] ?: [];
                $id = $_GET['product_id'];
                $count = $_GET['product_count'];
                foreach ($cart as &$c) {
                    if ($c['product_id'] == $id && $count > 0) {
                        $c['product_count'] = $count;
                    }
                }
                $_SESSION['cart'] = $cart;

                $products_cart = $_SESSION['cart'];
                $products = [];
                if (isset($products_cart) || !empty($products_cart))
                    foreach ($products_cart as $pc)
                        $products[] = Products::find()->where(['isActive' => 1])
                            ->andWhere(['id' => $pc['product_id']])
                            ->one();

                return $this->renderPartial('_rows', [
                    'products' => $products,
                    'products_cart' => $products_cart,
                ]);
            }
            if (Yii::$app->request->get('del')) {
                session_start();
                $cart = $_SESSION['cart'] ?: [];
                $id = $_GET['product_id'];
                $count = $_GET['product_count'];
                foreach ($cart as $key => $c) {
                    if ($c['product_id'] == $id) {
                        unset($cart[$key]);
                    }
                }
                $_SESSION['cart'] = $cart;

                $products_cart = $_SESSION['cart'];
                $products = [];
                if (isset($products_cart) || !empty($products_cart))
                    foreach ($products_cart as $pc)
                        $products[] = Products::find()->where(['isActive' => 1])
                            ->andWhere(['id' => $pc['product_id']])
                            ->one();

                return $this->renderPartial('_rows', [
                    'products' => $products,
                    'products_cart' => $products_cart,
                ]);
            }

        }

        return $this->redirect(['/cart']);
    }

    public function actionLocation()
    {
        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->get('location')) {
                session_start();
                if (isset($_GET['code']))
                    $_SESSION['stock'] = $_GET['code'];

                return true;
            }
        }

        return $this->redirect(['/cart']);
    }

}
