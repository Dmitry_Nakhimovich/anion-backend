<?php

namespace frontend\controllers;

use common\models\AttrContent;
use common\models\NavMenu;
use common\models\Products;
use common\models\ProductsAttr;
use common\models\Sections;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class ProductsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionCategory($url)
    {
        $section = Sections::find()->where(['id_type' => 1])
            ->andWhere(['link' => $url])
            ->andWhere(['<=', 'id_item', 0])
            ->one();
        if (empty($section))
            throw new HttpException(404, 'Страница не существует...');
        $id = $section->id;
        $navMenu = NavMenu::find()->where(['menu_type' => 1])
            ->andWhere(['external_id' => $id])
            ->one();
        //$section = Sections::findOne($id);
        $products = Products::find()->where(['isActive' => 1])
            ->andWhere(['isComplect' => 0])
            ->andWhere(['nav_menu_id' => $navMenu->id])
            ->orderBy(['date' => SORT_DESC]);
        $filters = ProductsAttr::find()->where(['sections_id' => $id])
            ->andWhere(['isActive' => 1])
            ->andWhere(['isFilter' => 1])
            ->orderBy('priority')
            ->all();
        $podcat = NavMenu::find()->where(['parent_id' => $navMenu->id])
            ->andWhere(['isActive' => 1])
            ->orderBy('priority')
            ->all();
        $isPodcat = empty($podcat) ? false : true;

        $count = $products->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 9]);
        $products = $products->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        // filters get form
        if (Yii::$app->request->get('filters')) {
            $cproducts = Products::find()->where(['isActive' => 1])
                ->andWhere(['isComplect' => 0])
                ->andWhere(['nav_menu_id' => $navMenu->id])
                ->orderBy(['date' => SORT_DESC])->all();
            $resFilters = [];
            foreach ($cproducts as $cproduct)
                $resFilters[] = $cproduct->id;
            $res = $resFilters;
            foreach ($filters as $cfilter) {
                if (!empty($_GET['filter_' . $cfilter->id])
                    || gettype($_GET['filter_' . $cfilter->id]) == "array") {
                    $cfilterContent = AttrContent::find()->where(['attr_id' => $cfilter->id]);
                    if ($cfilter->type == 'text' || $cfilter->type == 'select') {
                        $cGet = $_GET['filter_' . $cfilter->id];
                        if ($cGet)
                            $cfilterContent = $cfilterContent->andWhere(['like', 'text', '%' . $cGet . '%', false]);
                    } elseif ($cfilter->type == 'num') {
                        $cGet_from = (float)$_GET['filter_' . $cfilter->id]['from'] ?: '0';
                        $cGet_to = (float)$_GET['filter_' . $cfilter->id]['to'] ?: '9999999999';
                        if (isset($cGet_from) && isset($cGet_to)) {
                            $cfilterContent = $cfilterContent->andFilterWhere(['between', 'num', $cGet_from, $cGet_to]);
                        }
                    }
                    $cfilterContent = $cfilterContent->all();
                }
                $resCFilters = [];
                if (!empty($cfilterContent)) {
                    foreach ($cfilterContent as $cfc) {
                        $resCFilters[] = Sections::findOne($cfc->sections_id)->id_item;
                    }
                    $res = array_intersect($res, $resFilters, $resCFilters);
                }
            }
            if ($res) {
                $cproducts = Products::find();
                foreach ($res as $r)
                    $cproducts = $cproducts->orWhere(['id' => $r]);
                $cproducts = $cproducts->orderBy(['date' => SORT_DESC]);
                $count = $cproducts->count();
                $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 9]);
                $cproducts = $cproducts->offset($pagination->offset)
                    ->limit($pagination->limit)
                    ->all();
            } else {
                $cproducts = [];
            }

            return $this->render('category', [
                'navMenu' => $navMenu,
                'section' => $section,
                'products' => $cproducts,
                'pagination' => $pagination,
                'filters' => $filters,
            ]);
        }

        return $this->render('category', [
            'navMenu' => $navMenu,
            'section' => $section,
            'products' => $products,
            'pagination' => $pagination,
            'filters' => $filters,
            'podcat' => $podcat,
            'isPodcat' => $isPodcat,
        ]);
    }

    public function actionDetail($id)
    {
        $product = Products::findOne($id);
        $section = Sections::find()->where(['id_type' => 1])
            ->andWhere(['id_item' => $id])->one();
        $navMenu = NavMenu::findOne($product->nav_menu_id);
        $navMenuSection = Sections::find()->where(['id_type' => 1])
            ->andWhere(['id_item' => $navMenu->id])->one();
        $complects = Products::find()->where(['nav_menu_id' => $navMenu->id])
            ->andWhere(['isComplect' => 1])
            ->andWhere(['isActive' => 1])
            ->orderBy('date')
            ->all();

        return $this->render('detail', [
            'navMenu' => $navMenu,
            'navMenuSection' => $navMenuSection,
            'section' => $section,
            'product' => $product,
            'complects' => $complects,
        ]);
    }

    public function actionUpdate()
    {
        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->get('add')) {
                session_start();
                $cart = $_SESSION['cart'] ?: [];
                $id = $_GET['product_id'];
                $isExist = false;
                foreach ($cart as &$c) {
                    if ($c['product_id'] == $id) {
                        $c['product_count']++;
                        $isExist = true;
                    }
                }
                if (!$isExist) {
                    $newProduct["product_id"] = $id;
                    $newProduct["product_count"] = 1;
                    $cart[] = $newProduct;
                }
                $_SESSION['cart'] = $cart;

                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    "count" => count($_SESSION['cart']),
                ];
            }

        }

        return $this->redirect(['/cart']);
    }

}
