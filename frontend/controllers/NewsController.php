<?php

namespace frontend\controllers;

use common\models\News;
use common\models\Sections;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\HttpException;

/**
 * Site controller
 */
class NewsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        $news = News::find()->where(['isActive' => 1])->orderBy(['date' => SORT_DESC])->all();
        $query = Sections::find()->where(['id_type' => 3])->andWhere(['isActive' => 1])->andWhere(['>', 'id_item', 0]);
        $section = Sections::findOne(4);
        // 4 - News id in sections table

        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 9]);
        $sections = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'news' => $news,
            'sections' => $sections,
            'section' => $section,
            'pagination' => $pagination,
        ]);
    }

    public function actionDetail($url)
    {
        $section = Sections::find()->where(['id_type' => 3])->andWhere(['link' => $url])->one();
        if (empty($section))
            throw new HttpException(404, 'Страница не существует...');
        $id = $section->id_item;
        $news = News::findOne($id);
        //$section = Sections::find()->where(['id_type' => 3])->andWhere(['id_item' => $id])->one();
        $sectionMain = Sections::findOne(4);

        $newsOther = News::find()->where(['!=', 'id', $id])->orderBy(['date' => SORT_DESC])->limit(5)->all();

        return $this->render('detail', [
            'news' => $news,
            'section' => ($sectionMain->id_item == -1) ? $sectionMain : $section,
            'newsOther' => $newsOther,
        ]);
    }

}
