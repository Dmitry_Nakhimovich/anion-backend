<?php

namespace frontend\controllers;

use common\models\Main;
use common\models\Materials;
use common\models\NavMenu;
use common\models\Products;
use common\models\Sections;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        $slider = Main::find()->where(['type' => 1])
            ->andwhere(['isActive' => 1])
            ->orderBy('priority')
            ->all();
        $descont = Main::find()->where(['type' => 2])
            ->andwhere(['isActive' => 1])
            ->orderBy('priority')
            ->all();
        $products = Main::find()->where(['type' => 3])
            ->andwhere(['isActive' => 1])
            ->orderBy('priority')
            ->all();
        $about = Main::find()->where(['type' => 4])->all();
        $news = Main::find()->where(['type' => 5])
            ->andwhere(['isActive' => 1])
            ->orderBy('priority')
            ->limit(3)
            ->all();

        return $this->render('index', [
            'slider' => $slider,
            'descont' => $descont,
            'products' => $products,
            'about' => $about,
            'news' => $news,
        ]);
    }

    public function actionSitemap()
    {
        $products = Products::find()->all();
        $productsNav = NavMenu::find()->where(['menu_type' => 1])
            ->orderBy('priority')
            ->all();
        $productsSection = Sections::find()->where(['id_type' => 1])->all();
        $menuList = [];
        foreach ($productsNav as $ml) {
            if (empty($menuList[$ml->parent_id]) || !isset($menuList[$ml->parent_id]))
                $menuList[$ml->parent_id] = [];
            $menuList[$ml->parent_id][] = $ml;
        }

        $materials = Materials::find()->all();
        $materialsNav = NavMenu::find()->where(['menu_type' => 2])
            ->orderBy('priority')
            ->all();
        $materialsSection = Sections::find()->where(['id_type' => 2])->all();
        $materialsList = [];
        foreach ($materialsNav as $ml) {
            if (empty($materialsList[$ml->parent_id]) || !isset($materialsList[$ml->parent_id]))
                $materialsList[$ml->parent_id] = [];
            $materialsList[$ml->parent_id][] = $ml;
        }

        return $this->render('sitemap', [
            'menuList' => $menuList,
            'materialsList' => $materialsList,
        ]);
    }


}









