<?php

namespace frontend\controllers;

use common\models\Materials;
use common\models\Sections;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\HttpException;


/**
 * Site controller
 */
class MaterialsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionDetail($url)
    {
        $section = Sections::find()->where(['id_type' => 2])->andWhere(['link' => $url])->one();
        if (empty($section))
            throw new HttpException(404, 'Страница не существует...');
        $id = $section->id_item;
        $materials = Materials::findOne($id);
        //$section = Sections::find()->where(['id_type' => 2])->andWhere(['id_item' => $id])->one();
        $sectionMain = Sections::findOne(3);

        return $this->render('detail', [
            'materials' => $materials,
            'section' => ($sectionMain->id_item == -1) ? $sectionMain : $section,
        ]);
    }

}
