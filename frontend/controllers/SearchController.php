<?php

namespace frontend\controllers;

use common\models\Cart;
use common\models\CartDetail;
use common\models\Feedback;
use common\models\Materials;
use common\models\News;
use common\models\Products;
use common\models\Stock;
use common\models\StockContent;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SearchController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        if (Yii::$app->request->get('search')) {
            $text = $_GET['header_search'] ?: '';
            if (empty($text))
                return $this->render('index', []);

            $products = Products::find()->where(['isActive' => 1])
                ->andWhere(['like', 'title', '%' . $text . '%', false])
                ->orWhere(['like', 'text', '%' . $text . '%', false])
                ->orderBy('date')
                ->all();
            $news = News::find()->where(['isActive' => 1])
                ->andWhere(['like', 'title', '%' . $text . '%', false])
                ->orWhere(['like', 'text', '%' . $text . '%', false])
                ->orderBy('date')
                ->all();
            $materials = Materials::find()->where(['isActive' => 1])
                ->andWhere(['like', 'title', '%' . $text . '%', false])
                ->orWhere(['like', 'text', '%' . $text . '%', false])
                ->orderBy('date')
                ->all();

            $search = array_merge($products, $news, $materials);

            return $this->render('index', [
                'products' => $products,
                'materials' => $materials,
                'news' => $news,
                'search' => $search,
            ]);
        }

        return $this->render('index', []);
    }

    public function actionMail()
    {
        if (Yii::$app->request->post('cart')) {
            $products_cart = $_SESSION['cart'];
            $products = [];
            if (isset($products_cart) || !empty($products_cart))
                foreach ($products_cart as $pc)
                    $products[] = Products::find()->where(['isActive' => 1])
                        ->andWhere(['id' => $pc['product_id']])
                        ->one();

            // send letter
            Yii::$app->mailer->compose()
                ->setFrom('test@anionplast.ru')
                ->setTo($_POST['email'] ?: '')
                ->setSubject('Новая заявка на сайте Anion')
                ->setHtmlBody($this->renderPartial('mail', [
                    'mail' => $_POST,
                    'products' => $products,
                    'products_cart' => $products_cart,
                ]))
                ->send();
            //var_dump('<pre>', $_POST);die;

            // create issue
            $model = new Cart();
            $model->id_stock = Stock::find()->where(['code' => $_SESSION['stock']])->one()->id;
            $model->name = $_POST['fio'] ?: '';
            $model->company = $_POST['company'] ?: '';
            $model->tel = $_POST['tel'] ?: '';
            $model->email = $_POST['email'] ?: '';
            $model->text = $_POST['comment'] ?: '';
            $model->date = date('Y-m-d');
            $model->save();
            foreach ($products as $product) {
                $cstock = Stock::find()->where(['code' => $_SESSION['stock']])->one();
                $cprice = StockContent::find()->where(['product_id' => $product->id])
                    ->andWhere(['stock_id' => $cstock->id])->one();
                foreach ($products_cart as $pc)
                    if ($product->id == $pc['product_id'])
                        $cproduct_session = $pc;
                $detail = new CartDetail();
                $detail->id_cart = $model->id;
                $detail->id_product = $cproduct_session['product_id'];
                $detail->count = $cproduct_session['product_count'];
                $detail->price = $cprice->price;
                $detail->save();
            }

            return $this->redirect(['/cart']);
        }

        if (Yii::$app->request->post('mail')) {
            // create issue
            $model = new Feedback();
            $model->id_stock = Stock::find()->where(['code' => $_SESSION['stock']])->one()->id;
            $model->name = $_POST['fio'] ?: '';
            $model->company = $_POST['company'] ?: '';
            $model->tel = $_POST['tel'] ?: '';
            $model->email = $_POST['email'] ?: '';
            $model->text = $_POST['comment'] ?: '';
            $model->date = date('Y-m-d');
            $model->save();

            return $this->redirect(['/']);
        }

        return $this->redirect(['/']);
    }

}
