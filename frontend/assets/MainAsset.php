<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/preloader.css',
        'css/fonts.css',
        'css/main.css',
        'css/common.css',
        'css/plugins.css',
    ];
    public $js = [
        'js/jquery.min.js',
        'js/bootstrap.min.js',
        'js/common.js',
        'js/plugins.js',
        'js/main.js',
        'js/custom.js',
    ];
    public $depends = [
        /*'yii\web\YiiAsset',*/
        /*'yii\bootstrap\BootstrapAsset',*/
    ];
}
