$(document).ready(function () {
    // ajax cart requests
    $("#btn-cart-add").on('click', function () {
        var productId = $(this).data('product_id');
        var csrfName = $('meta[name="csrf-param"]').prop("content");
        var csrfValue = $('meta[name="csrf-token"]').prop("content");
        var siteUrl = window.location.href;

        $.ajax({
            async: false,
            type: "GET",
            url: "/products/update",
            dataType: "json",
            data: {
                cart: 'true',
                add: 'true',
                product_id: productId,
            },
            error: function (data) {
                $.notify('Ошибка при добавлении товара в корзину', 'error');
            },
            success: function (data) {
                $.notify('Товар добавлен в корзину', 'success');
                if (data.count > 0)
                    $('#header-btn-shop .btn-shop-items').text('(' + data.count + ' товаров)');
                else
                    $('#header-btn-shop .btn-shop-items').text('(0 товаров)');
            }
        });
    });
    $(".page-cart .cart-wrap").on('click', '.item-del', function () {
        var productId = $(this).data('product_id');
        var productCount = $(this).data('product_count');
        var siteUrl = window.location.href;

        $.ajax({
            async: false,
            type: "GET",
            url: "cart/update",
            dataType: "text",
            data: {
                cart: 'true',
                del: 'true',
                product_id: productId,
                product_count: productCount,
            },
            error: function (data) {
                $.notify('Ошибка при удалении товара из корзины', 'error');
            },
            success: function (data) {
                // перерисовка колонок таблицы
                $('.page-cart .cart-wrap').find('tbody').html(data);
                // уведомление
                $.notify('Товар удален из корзины', 'success');
                //пересчет количества товаров в корзине
                var count = $('.page-cart .cart-wrap').find('tr').length;
                if (count === 1 && $('#hide-cart-btn').length)
                    count = 0;
                if (count > 0)
                    $('#header-btn-shop .btn-shop-items').text('(' + count + ' товаров)');
                else
                    $('#header-btn-shop .btn-shop-items').text('(0 товаров)');
                // общая сумма заказа
                var res = 0;
                $('.page-cart .cart-wrap').find('tr').each(function (index, item) {
                    res += parseFloat($(item).find('.item-price').text());
                });
                $('.page-cart .res-price .price-val').text(res);
                // скрыть кнопку отправки заказа
                if ($('#hide-cart-btn').length) {
                    $('.page-cart .res-btn a').hide();
                    $('.page-cart .res-price .price-val').text(0);
                }
                // перезагрузка плагинов внутри таблицы
                $(".item-counter").TouchSpin({
                    min: 0,
                    max: 9999,
                    step: 1,
                    maxboostedstep: 10,
                });
                $('.selectpicker').selectpicker();
            }
        });
    });
    $(".page-cart .cart-wrap").on('change', '.item-counter', function () {
        var productId = $(this).data('product_id');
        var productCount = $(this).val();
        $.ajax({
            async: false,
            type: "GET",
            url: "cart/update",
            dataType: "text",
            data: {
                cart: 'true',
                upd: 'true',
                product_id: productId,
                product_count: productCount,
            },
            error: function (data) {
                $.notify('Ошибка при обновлении корзины', 'error');
            },
            success: function (data) {
                $('.page-cart .cart-wrap').find('tbody').html(data);

                var res = 0;
                $('.page-cart .cart-wrap').find('tr').each(function (index, item) {
                    res += parseFloat($(item).find('.item-price').text());
                });
                $('.page-cart .res-price .price-val').text(res);

                $(".item-counter").TouchSpin({
                    min: 0,
                    max: 9999,
                    step: 1,
                    maxboostedstep: 10,
                });
                $('.selectpicker').selectpicker();
            }
        });
    });
    //header location
    $("#header-location, #cart-location").on('change', function () {
        var code = $(this).val();
        $.ajax({
            async: false,
            type: "GET",
            url: "/cart/location",
            dataType: "text",
            data: {
                cart: 'true',
                location: 'true',
                code: code,
            },
            error: function (data) {
                $.notify('Ошибка при изменении локации', 'error');
            },
            success: function (data) {
                $.notify('Склад доставки успешно изменен', 'success');
                location.reload();
            }
        });
    });

});
